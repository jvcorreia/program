'''
Projeto Controle de gastos
'''

def ver():
    import mysql.connector

    cnx = mysql.connector.connect(user='root', host='127.0.0.1:3305', password="root", database='gastos')
    cursor = cnx.cursor()

    sql = ("SELECT * FROM tb_gastos;")

    cursor.execute(sql)

    print("Tabela de gastos")
    for (idt, nme_mes, gastos, din_pensao, din_estagio, din_conta_final_mes, din_poupanca_final_mes) in cursor:
        print(idt, nme_mes, gastos, din_pensao, din_estagio, din_conta_final_mes, din_poupanca_final_mes)

    cursor.close()
    cnx.close()

opcao = 0

while opcao != 5:
    print('''
    Bem vindo ao controle de gastos mensal
    --------------------------------------
     1 - Ver tabela de gastos:
     2 - Modificar tabela de gastos:
     3 - Criar tabela de gastos para próximo mês:
     4 - Ver tabela em um mês específico:
     5 - Sair.''')
    int(input("Selecione uma opção abaixo:"))

if opcao == 1:
    ver()



