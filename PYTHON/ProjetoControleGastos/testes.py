def ver():
    import mysql.connector

    cnx = mysql.connector.connect(user='root', host='localhost', password="root", database='gastos')
    cursor = cnx.cursor()

    sql = ("SELECT * FROM tb_gastos;")

    cursor.execute(sql)

    print("Tabela de gastos")
    for (idt, nme_mes, gastos, din_pensao, din_estagio, din_conta_final_mes, din_poupanca_final_mes) in cursor:
        print(idt, nme_mes, gastos, din_pensao, din_estagio, din_conta_final_mes, din_poupanca_final_mes)

    cursor.close()
    cnx.close()

ver()