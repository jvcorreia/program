from base import sql
from flask import Flask, render_template, request


app = Flask(__name__)

@app.route('/')
def menu():
   return render_template('menu.html')

@app.route('/consultar')
def consultar():
   # Recuperando modelos existentes na base de dados
   mysql = sql.SQL("root","root", "test")
   comando = "SELECT * FROM tb_marca ORDER BY nme_marca;"

   cs = mysql.consultar(comando, ())
   sel = "<SELECT NAME='marca' id='idt'>"
   sel += "<OPTION VALUE='0'>Todos</OPTION>"
   for [idt, nome] in cs:
       sel += "<OPTION VALUE='" + str(idt) + "'>" + nome + "</OPTION>"
   sel += "</SELECT>"
   cs.close()
   return render_template('consultar.html', marcas=sel)

@app.route('/modelos', methods=['POST'])
def modelos():
   # Pegando os dados de parâmetro vindos por ajax de consultar
   idtMarca = request.form['marca']

   # Recuperando softwares que satisfazem ao parâmetro de filtragem
   mysql = sql.SQL("root","root", "test")
   comando = "SELECT nme_veic, vlr_veic FROM tb_veic WHERE cod_marca = %s OR 0 = %s;"

   cs = mysql.consultar(comando, [idtMarca, idtMarca])
   veic = "<TABLE><TR><TH>Livro</TH><TH>Valor</TH></TR>"
   for [nome, valor] in cs:
       veic += "<TR>"
       veic += "<TD>" + nome + "</TD>"
       veic += "<TD>" + str(valor) + "</TD>"
       veic += "</TR>"
   cs.close()
   veic += "</TABLE>"

   return render_template('ajax.html', AJAX=veic)




@app.route('/combos')
def combos():
   # Recuperando Tipos de Software existentes na base de dados
   mysql = sql.SQL("root","root", "test")
   comando = "SELECT * FROM tb_marca ORDER BY nme_marca;"

   cs = mysql.consultar(comando, ())
   sel = "<SELECT NAME='marca' id='idtMarca' onclick='execMarcas()'>"
   sel += "<OPTION VALUE='0'>Escolha uma editora</OPTION>"
   for [idt, nome] in cs:
       sel += "<OPTION VALUE='" + str(idt) + "'>" + nome + "</OPTION>"
   sel += "</SELECT>"
   cs.close()
   return render_template('combos.html', marcas=sel)


@app.route('/car', methods = ['POST'])
def veic():
   # Recuperando Tipos de Software existentes na base de dados
   mysql = sql.SQL("root","root", "test")
   comando = "SELECT idt_veic, nme_veic FROM tb_veic WHERE cod_marca=%s ORDER BY nme_veic;"
   idtMarca = request.form['marca']
   cs = mysql.consultar(comando, [idtMarca])
   sel = "<SELECT NAME='veic' id='idtVeic' onclick='execVeics()'>"
   sel += "<OPTION VALUE='0'>Escolha um livro</OPTION>"
   for [idt, nome] in cs:
       sel += "<OPTION VALUE='" + str(idt) + "'>" + nome + "</OPTION>"
   sel += "</SELECT>"
   cs.close()
   return render_template('AJAX.html', AJAX=sel)



@app.route('/val', methods = ['POST'])
def ver():
   # Recuperando Tipos de Software existentes na base de dados
   mysql = sql.SQL("root","root", "test")
   comando = "SELECT vlr_veic FROM tb_veic WHERE idt_veic=%s;"
   idtVeic = request.form['veic']
   cs = mysql.consultar(comando, [idtVeic])
   dados = cs.fetchone()
   sel = dados[0]
   cs.close()
   return render_template('AJAX.html', AJAX=sel)




@app.route('/barras')
def barras():
   # Recuperando Tipos de Software existentes na base de dados
   mysql = sql.SQL("root","root", "test")
   comando = "SELECT nme_marca, COUNT(idt_veic) AS qtd FROM tb_marca JOIN tb_veic ON idt_marca=cod_marca GROUP BY nme_marca;"

   cs = mysql.consultar(comando, ())

   grf = ""
   i = 0
   for [nome, qtd] in cs:
       grf += ", ['" + nome + "', " + str(qtd) + ", '#9999FF']"
       i += 1
   cs.close()

   return render_template('barras.html', barras=grf)



