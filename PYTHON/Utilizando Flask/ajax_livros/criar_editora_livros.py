from banco import sql

mysql = sql.SQL("root","root", "test") 

comando = "CREATE TABLE tb_marca (idt_marca INT AUTO_INCREMENT PRIMARY KEY, " + \
         "nme_marca VARCHAR(50) NOT NULL);"

if mysql.executar(comando, ()):
   print ("Tabela de marcas de carros criada com sucesso!")

comando = "CREATE TABLE tb_veic (idt_veic INT AUTO_INCREMENT PRIMARY KEY, " + \
         "nme_veic VARCHAR(50) NOT NULL, " + \
         "vlr_veic INT NOT NULL, " + \
         "cod_marca INT NOT NULL, " + \
         "CONSTRAINT fk_marca_veic FOREIGN KEY (cod_marca) REFERENCES tb_marca(idt_marca));"

if mysql.executar(comando, ()):
   print ("Tabela de veiculos criada com sucesso!")


#INSERTS FEITOS DIRETAMENTE NO BANCO DE DADOS
