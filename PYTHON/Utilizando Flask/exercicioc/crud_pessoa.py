from base import sql
from flask import Flask, render_template, request


app = Flask(__name__)

@app.route('/')
def menu():
   return render_template('menu.html')

@app.route('/formincluir')
def formIncluir():
   return render_template('formIncluir.html')

@app.route('/incluir', methods=['POST'])
def incluir():
   # Recuperando dados do formulário de formIncluir()
   nome = request.form['nome']
   sobrenome = request.form['sobrenome']
   idade = float(request.form['idade'])
   nacionalidade = request.form['nacionalidade']
   expectativa = float(request.form['expectativa'])

   # Incluindo dados no SGBD
   mysql = sql.SQL("root", "root", "pessoa")
   comando = "INSERT INTO tb_pessoa(nome_pessoa, sobrenome_pessoa, idade_pessoa, nacionalidade_pessoa, expectativa_pessoa) VALUES (%s, %s, %s, %s, %s);"

   if mysql.executar(comando, [nome, sobrenome, idade, nacionalidade, expectativa]):
       msg="Pessoa com o nome " + nome + " cadastrada com sucesso!"
   else:
       msg="Falha na inclusão da pessoa."

   return render_template('incluir.html', msg=msg)

@app.route('/parconsultar')
def parConsultar():
  # Recuperando modelos existentes na base de dados
  mysql = sql.SQL("root", "root", "pessoa")
  comando = "SELECT DISTINCT nome_pessoa FROM tb_pessoa ORDER BY nome_pessoa;"

  cs = mysql.consultar(comando, ())
  sel = "<SELECT NAME='nome'>"
  sel += "<OPTION>Todos</OPTION>"
  for [nome] in cs:
      sel += "<OPTION>" + nome + "</OPTION>"
  sel += "</SELECT>"
  cs.close()

  # Recuperando menor e maior valor de curso
  comando="SELECT MIN(expectativa_pessoa) AS menor, MAX(expectativa_pessoa) AS maior FROM tb_pessoa;"
  cs = mysql.consultar(comando, ())
  dados = cs.fetchone()
  menor = dados[0]
  maior = dados[1]

  return render_template('parConsultar.html', nome=sel, menor=menor, maior=maior)

@app.route('/consultar', methods=['POST'])
def consultar():
  # Pegando os dados de parâmetro vindos do formulário parConsultar()
  nome = request.form['nome']
  menor = str(request.form['ini'])
  maior = str(request.form['fim'])

  # Testando se é para considerar todos os modelos
  nome = "" if nome=="Todos" else nome

  # Recuperando modelos que satisfazem aos parâmetros de filtragem
  mysql = sql.SQL("root", "root", "pessoa")
  comando = "SELECT * FROM tb_pessoa WHERE nome_pessoa LIKE CONCAT('%', %s, '%') AND expectativa_pessoa BETWEEN %s AND %s ORDER BY nome_pessoa;"

  #locale.setlocale(locale.LC_ALL, 'pt_BR.UTF8')

  cs = mysql.consultar(comando, [nome, menor, maior])
  nomes = ""
  for [idt, nome, sobrenome, idade, nacionalidade, expectativa] in cs:
      nomes += "<TR>"
      nomes += "<TD>" + nome + "</TD>"
      nomes += "<TD>" + sobrenome + "</TD>"
      nomes += "<TD>" + str(idade) + "</TD>"
      nomes += "<TD>" + str(nacionalidade) + "</TD>"
      nomes += "<TD>" + expectativa + "</TD>"
      nomes += "</TR>"
  cs.close()

  return render_template('consultar.html', nomes=nomes)


@app.route('/paralterar')
def parAlterar():
  return render_template('parAlterar.html')

@app.route('/formalterar', methods=['POST'])
def formAlterar():
   # Pegando os dados de parâmetro vindos do formulário parConsultar()
   nome = request.form['nome']

   # Recuperando modelos que satisfazem aos parâmetros de filtragem
   mysql = sql.SQL("root", "root", "pessoa")
   comando = "SELECT * FROM tb_pessoa WHERE nome_pessoa=%s;"

   cs = mysql.consultar(comando, [nome])
   dados = cs.fetchone()
   cs.close()

   if dados == None:
       return render_template('naoEncontrado.html')
   else:
       return render_template('formAlterar.html', nome=dados[1], sobrenome=dados[2], idade=dados[3],
                              nacionalidade=dados[4], expectativa=dados[5])

@app.route('/alterar', methods=['POST'])
def alterar():
  # Recuperando dados do formulário de formAlterar()
  idt = (request.form['idt'])
  nome = request.form['nome']
  sobrenome = request.form['sobrenome']
  idade = str(request.form['idade'])
  nacionalidade = request.form['nacionalidade']
  expectativa = str(request.form['expectativa'])

  # Alterando dados no SGBD
  mysql = sql.SQL("root", "root", "pessoa")
  comando = "UPDATE tb_pessoa SET nome_pessoa=%s, sobrenome_pessoa=%s, idade_pessoa=%s, nacionalidade_pessoa=%s, expectativa_pessoa=%s WHERE idt_pessoa=%s;"

  if mysql.executar(comando, [nome, sobrenome, idade, nacionalidade, expectativa, idt]):
      msg="Pessoa com o nome " + nome + " alterada com sucesso !"
  else:
      msg="Falha na alteração da pessoa selecionada."

  return render_template('alterar.html', msg=msg)


@app.route('/parexcluir')
def parExcluir():
   # Recuperando todos os cursos da base de dados
   mysql = sql.SQL("root", "root", "pessoa")
   comando = "SELECT idt_pessoa, nome_pessoa, sobrenome_pessoa, idade_pessoa FROM tb_pessoa ORDER BY nome_pessoa;"

   cs = mysql.consultar(comando, ())
   pessoas = ""
   for [idt, nome, sobrenome, idade] in cs:
      pessoas += "<TR>"
      pessoas += "<TD>" + nome + " (" + sobrenome + ")" + "</TD>"
      pessoas+= "<TD>" + str(idade) + "</TD>"
      pessoas += "<TD><BUTTON ONCLICK=\"jsExcluir('" + nome + " (" + sobrenome + ")" + "', " + str(idt) + ")\">Excluir" + "</BUTTON></TD>"
      pessoas += "</TR>"
   cs.close()

   return render_template('parExcluir.html', pessoas=pessoas)



@app.route('/excluir', methods=['POST'])
def excluir():
   # Recuperando dados do formulário de parExcluir()
   idt = int(request.form['idt'])

   # Alterando dados no SGBD
   mysql = sql.SQL("root", "root", "pessoa")
   comando = "DELETE FROM tb_pessoa WHERE idt_pessoa=%s;"

   if mysql.executar(comando, [idt]):
       msg="pessoa excluído com sucesso!"
   else:
       msg="Falha na exclusão da pessoa."

   return render_template('excluir.html', msg=msg)
