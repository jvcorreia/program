'''
Estruturas lógicas: and, or, not, is

Operadores unários: not
Operadores binários: and, or, is

Regras de funcionamento:

    Para and, ambos os valores precisam ser True
    Para or, um ou outro valor precisa ser True
    Para o not, o valor do booleano
    Para o is, o valor é comparado com um segundo
'''

ativo = True
logado = True

if ativo or logado:
    print("usuário logado no sistema")
else:
    print("você precisa ativar a sua conta")

# Se não estiver ativo, print "olá"
if not ativo:
    print("olá")

print(not True)

if ativo is True:
    print('yes')

# Ativo é falso?
print(ativo is False)

nome = 'joao'

print(nome.isupper())  # nome está todo em maúsculo??


