'''
Loop -> Estrutura de repetição
for  -> Uma dessas estruturas

C ou JAVA

    for(int = 0; i < 10; i++){
        Execução do loop
    }

Python

    for item em interavel:
        Execução do loop

Utilizamos loops para iterar sobre sequências ou sobre valores iteráreveis

Exemplos de iteráveis:
-String
    nome = 'joao'
-Lista
    lista = [1, 3, 5, 7]
-Range
    numeros = range(1, 10)



#Exemplo de for 1 (Iterando em uma string)
for letra in nome:
    print(letra)

#Exemplo de for 2 (Iterando sobre uma lista)
for numero in lista:
    print(numero)

#Exemplo de for 3 (Iterando sobre um range)

range(valorinicial, valorfinal + 1)
OBS: o valor final é não inclusive
1-
2-
3-
4-
5-
6-
7-
8-
9-
10-
11-Não
'''
for numero in range(1, 11):
    print(numero)

nome = 'joao'
lista = [1, 3, 5, 7]
numeros = range(1, 10)  # Temos que transformar em uma lista

'''
Enumerate
(0, j), (1, o), (2, a), (3, o)
'''
for indice, letra in enumerate(nome):
    print(letra)

for _, letra in enumerate(nome):
    print(letra)
# OBS: quando não precisamos de um valor, podemos descarta-lo.

qtd = int(input("quantas vezes o loop deve rodar?"))

for n in range(1, qtd+1):
    print(f'imprindo {n}')

for letra in nome:  # printar todo o nome na mesma linha
    print(letra, end = '')

print(nome*3)  # imprimir o nome 3 vezes
