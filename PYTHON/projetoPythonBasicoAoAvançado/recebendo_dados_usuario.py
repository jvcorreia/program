"""
Recebendo dados do usuário
"""

'''
Em python, é string tudo o que estiver entre:
Aspas simples
Aspas duplas
Aspas simples triplas
Aspas duplas triplas
'''
# Entrada de dados

# print('Qual o seu nome?')
# nome = input() #Input --> entrada

nome = input('Qual o seu nome?')

# Exemplo de print 'antigo'
print('Seja bem-vindo(a) %s' %nome)

# Exemplo de print 'moderno'
print('Seja bem-vindo(a) {0}'.format(nome))

# Exemplo mais atual
print(f'Seja bem-vindo(a) {nome}')

# print('Qual a sua idade?')
# idade = input()#dado a ser recebido será do tipo string

idade = int(input("Qual a sua idade?"))


# Processamento

# Saída de dados

# Exemplo antigo
print('%s tem %s anos' % (nome, idade))

# Exemplo 'moderno'
print('{0} tem {1} anos'.format(nome, idade))

# Exemplo mais atual
print(f'{nome} tem {idade} anos')

'''
cast é a conversão de um tipo de dado para outro
'''
print(f'{nome} nasceu em {2018-(idade)}')
