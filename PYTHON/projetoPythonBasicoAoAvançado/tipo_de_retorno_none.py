'''
Tipo None

O tipo de dado none em python representa o tipo sem tipo, ou poderia ser conhecido
também como tipo vazio, porém falar que é um tipo sem tipo é mais apropriado.

OBS: a primeira letra sempre maiúscula

Quando utilizamos?
-quando queremos criar uma variável e utiliza-la sem declarar o tipo, antes de receber o valor final.
'''


numeros = None


print(numeros)
print(type(numeros))
