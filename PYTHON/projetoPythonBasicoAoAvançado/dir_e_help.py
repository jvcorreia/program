"""

Utilitários python para utilizar na programação

dir-> Apresenta todos os atributos/propriedades, funções/métodos disponíveis
para determinado tipo de dado ou variável.

No terminal python:

dir(tipo de dado/variável)

help-> Apresenta a documentação de como utilizar os atributos/propriedades e funções/métodos
para determinado tipo de dado ou variável.

No terminal python:

help(tipo de dado/variável.propriedade)
help('joao'.upper)

Ele irá mostrar tudo sobre a função

"""