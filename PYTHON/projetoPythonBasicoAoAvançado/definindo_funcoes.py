'''
Definindo funções

- funções são pequenas partes de código que realizam tarefas específicas
- pode ou não receber uma entrada de dados e retornar uma saída de dados
- muito úteis para executar procedimentos similares por muitas vezes

OBS: se você escrever uma função que realiza várias tarefas dentro dela, é bom
 fazer uma verificação para que a função seja simplificada

Já utilizamos várias funções
- print()
- len()
- max()
'''

# Exemplo de utilização de função

listacores = ['vermelho', 'azul', 'verde']

# Utilizando a função integrada (built-in) do Python (print)
print(listacores)

listacores.append('roxo')
print(listacores)

# Note que o append não pode ser utilizado em qualquer lugar, pois ele está integrado
# a outra variável


'''
Em python, a forma de definir uma função é:

    def nome_da_funcao(parametros_de_entrada):
        bloco da função
        
Onde: *nome da função sempre com letras minúsculas e se compostas separadas por underline
      *parametros_de_entrada são opcionais, e se tiver mais de um o mesmo é separado por vírgula, podendo
        ser opcionais ou não      
      *bloco da função é onde o processamento acontece, pode apresentar retorno ou não  
'''

# Definindo a primeira função


def diz_oi():
    print('oi')

'''
OBS: veja que dentro das nossas funções podemos utilizar outras funções
OBS: veja que nossa função executa uma tarefa, ou seja, a única coisa que ela faz é dizer oi
OBS: veja que esta função não recebe nenhum parâmetro de entrada
OBS: veja que esta função não retorna nada
'''

# Utilizando funções

# Chamada de execução
diz_oi()

# diz_oi -> gera erro
# diz_oi () -> Errado


def cantar_parabens():
    print('parabens pra vc')
    print("nesta data querida")


cantar_parabens()

for n in range(5): #0 a 4
    cantar_parabens()

# Em python, podemos inclusive criar variáveis do tipo de uma função e executar esta função
#  através da variável.

cantar = cantar_parabens

cantar()
'''
ATENÇÃO - nunca esqueça de utilizar os parênteses ao chamar uma função
'''