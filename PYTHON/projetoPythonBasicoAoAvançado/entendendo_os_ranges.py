'''
Ranges

-Precisamos conhecer o loop for para usar os ranges
-Precisamos conhcer o range para trabalhar melhor com loops for

Ranges são utilizados para gerar sequências númericas de forma especificada

#forma 01
range(valor_de_parada)

OBS: valor_de_parada não é inclusive - (inicia em 0, e o passo é de 1 em 1)

#forma 02
range(valor_de_inicio, valor_de_parada)

OBS: valor_de_parada não é inclusive - (inicio é especificado pelo usuário)

#forma 03
range(valor_de_inicio, valor_de_parada, passo)
OBS: valor_de_parada não é inclusive - (passo é especificado pelo usuário)

#forma 04 (forma 3 inversa)
range(valor_de_inicio, valor_de_parada, passo)
OBS: valor_de_parada não é inclusive - (passo é especificado pelo usuário)
'''

# Forma 01 - ex: printar de 1 a 10, 3 vezes
for _ in range(3):
    for x in range(11):
        print(x)

# Forma 02 - printar de 1 a 3, 2 vezes
for _ in range (1, 3):
    for x in range(1, 4):
        print(x)

# Forma 03 - contar de 1 a 49, de 2 em 2
for _ in range (1, 50, 2):
    print(_)

# Forma 03 - contar de 50 a 2, de 2 em 2
for _ in range (50, 1, -2):
    print(_)

