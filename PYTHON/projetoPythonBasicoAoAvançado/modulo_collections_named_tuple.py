'''
Módulo collections  - Named tuple

tupla = (2, 3, 5, 6, 7)

print(tupla[2])

Named tuple -> são tuplas diferenciadas, onde especificamos um nome para a mesma e também parâmetros
'''

from collections import namedtuple

#Precisamos definir nome e parâmetros

# Forma 1 - Declaração named tuple

cachorros = namedtuple('cachorro', 'idade raca nome')

# Forma 2 - Declaração named tuple

cachorros = namedtuple('cachorro', 'idade, raca, nome')

# Forma 3 - Declaração named tuple

cachorros = namedtuple('cachorro', ['idade', 'raca', 'nome'])

# Usando

ray = cachorros(idade = 2, raca = 'poddle', nome = 'julio')
print(ray)

print(ray.idade)

print(ray.index('julio'))
print(ray.count('julio'))
