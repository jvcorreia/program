'''
Tipo float

Tipo real

Tipo decimal

O separador de casas decimais na programação é o ponto e não a vírgula
'''

# Errado do ponto de vista do float, mas gera uma dupla
valor = 1, 44
print(valor)
print(type(valor))


# Certo do ponto de vista do float
valor2 = 1.44
print(valor2)
print(type(valor2))

# É possível fazer dupla atribuição
valor1, valor2 = 1, 44
print(valor1)
print(type(valor1))
print(valor2)
print(type(valor2))

# Podemos converter float para int
"""
Ao converter valores float para inteiro, perdemos precisão
"""
res = int(valor1)
print(valor1)
print(type(valor1))

# Podemos trabalhar com números complexos
variavel = 5j
print(type(variavel))