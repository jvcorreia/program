'''
Módulo collections - Deque

Podemos dizer que deque é uma lista de alta performance


'''
from collections import deque

deque = deque('geek')
print(deque)

deque.append('y')  # Adiciona no final

print(deque)

deque.appendleft('y')  # Adiciona no começo

print(deque)
print(deque.pop())  # Remove e retorna ultimo elemento
print(deque.popleft())  # Remove e retorna o primeiro elemento
