'''
Lista

Listas em python funcionam como vetores/matrizes em outras linguagens, com a diferença
de serem DINÂMICOS e podermos colocar ***QUALQUER*** tipo de dado.

Linguagens C/JAVA
    -Possuem tamanho e tipo de dado fixo
    Ou seja, nestas linguagens se você criar um array do tipo int e que guardar 5 dados,
    o array só poderá abrigar elementos int e no máximo 5.

PYTHON
-DINÂMICO:Não possue tamanho fixo, ou seja, podemos criar a lista e simplesmente ir adicionando
elementos;
-QUALQUER TIPO DE DADO:Não possuem tipo de dado fixo, ou seja, podemos colocar qualquer tipo de dado

As listas em python são representadas por colchetes [];
'''

type([])  # lista

lista1 = [1, 22, 23, 34, 99, 45, 46]

lista2 = ['j', 'o', 'a', 'o']

lista3 = []

lista4 = list(range(11))  # Lista do 0 ao 10

print(lista4)

lista5 = list('joao')

print(lista5)

# Podemos facilmente checar se determinado valor está contido na lista
if 18 in lista4:
    print('encontrei o número 8')
else:
    print('não encontrei o número 8')

if 'j' in lista5:
    print('sim')
else:
    print('não')

# Podemos facilmente ordenar uma lista
lista1.sort()
print(lista1)

lista5.sort()
print(lista5)

# Podemos facilmente contar o número de ocorrências de um valor em uma lista
print(lista1.count(1))  # return:1

print(lista5.count('o'))  # return:2

# Adicionar elementos em listas
'''
Para adicionar elementos em listas, utilizamos a função append

OBS: com append, só conseguimos adicionar um elemento por vez******
'''
print(lista1)
lista1.append(42)
print(lista1)

lista1.append([1, 4, 77777])  # adicionando uma lista dentro de uma lista,
print(lista1)

lista1.extend([123, 44, 67])  # Modo de adicionar elementos (mais de um) dentro de outra lista

'''
Existe uma diferença entre o 1 e o 2 método, o 1 coloca a lista como um elemtento único (sublista),
já o segundo coloca cada elemtno da lista como um valor adicional á lista
O append adiciona apenas 1 elemento por vez
'''

lista7 = list(range(7))

# Podemos inserir um novo elemento na lista informando a posição do índice
lista7.insert(2, 5)  # Adicionando o número 5 na posição 2
print(lista7)

# Podemos facilmento juntar duas listas
lista8 = lista2 + lista7
print(lista8)

# Imprimindo uma lista ao contrário
lista8.reverse()
print(lista8)
# Ou
print(lista8[::-1])

# Copiar uma lista

lista6 = lista2.copy()

# Para saber o tamanho de uma lista (contar elementos)
print(len(lista8))

# Podemos facilmente remover o último elemento de uma lista
# OBS: o pop não somente remove o último elemento, mas também o retorna
# Se não houver elemento para remover, teremos o IndexError
lista8.pop()

# Removendo elemento pelo indice
lista8.pop(2)  # Removendo o elemento da posição 2

# Remover todos os elementos
print(lista8)
print((lista8.clear()))

# Repetir elementos em uma lista
lista12 = [1, 10, 12]
lista12 = lista12 * 3
print(lista12)

# Converter uma string para uma lista
#OBS: por padrão o split separa por palavras
curso = 'joao victor'
curso = curso.split()
print(curso)

nome = 'joao,victor,correia,de,oliveira'
nome = nome.split(',')  # Definimos a vírgula como separador padrão
print(nome)

# Convertendo uma lista em uma string
frutas = ['maça', 'banana', 'uva']

frutasstring = ''.join((frutas)) # Pega a lista frutas, coloca um espaço entre cada elemento e
# transforma em string
print(frutas)

# Podemos realmente colocar qualquer tipo de dado em uma lista, inclusive misturando esses dados
listatudo = [1, 3.22, True, 'joao']

# Iterando sobre uma lista

# Exemplo 1 (FOR)
for elemento in lista12:
    print(elemento)
# Exemplo 2 (While)

carrinho = []
produto = ''

while produto != 'sair':
    print('Adicione um produto no carrinho ou digite sair para sair:')
    produto = input()
    if produto != 'sair':
        carrinho.append(produto)

for produto in carrinho:
    print(produto)

# Utilizando variáveis em listas
numeros = 1, 3, 5, 7

num1 = 1
num2 = 3
num3 = 5

numeros = [num1, num2, num3]

# Fazemos acesso aos elementos de forma indexada

cores = ['verde', 'amarelo', 'azul', 'branco']
print(cores[0])  # verde
print(cores[1])  # amarelo

# Fazer acesso aos elementos de forma invertida
print(cores[-1])  # branco -3, -2, -1, 0, 1, 2, 3 --> se tentar printar qualquer outro elemento, dará erro

for cor in cores:
    print(cor)

indice = 0
while indice < len(cores):
    print(indice)
    indice += 1

# Gerar indice em um for
for indice, cor in enumerate(cores):
    print(indice, cor)

# Métodos não tão importantes mas também úteis

    # Encontrar o índice de um elemento na lista
    numeros = [5, 6, 7, 8]
    print(numeros.index(6))#em qual indice está o valor 6
    # OBS: caso não tenha o elemento, dará erro.
    # Se tiver mais de um 6, retornara apenas a posição do primeiro elemento

    # Buscando a partir de um indice
    print(numeros.index(6, 1))  # buscando o 6 a partir o elemento 1

# Revisão de slicing
# lista(inicio:fim:passo) #similar ao range
print(numeros[1:3:1])

print(lista1[:3])  # começa no 0, vai até o elemento 2
print(lista1[::2])  # começa no 0, vai até o final, de 2 em 2

nome = ['joao', 'correia']
print(nome.reverse())

# Soma*, valor máximo*, valor mínimo*, Tamanho

# * Se os valores forem todos inteiros ou reias.

print(sum(lista12))  # Soma
print(max(lista12))  # Valor máximo
print(min(lista12))  # Valor mínimo
print(len(lista12))  # Tamanho da lista

# Transformar lista em tupla

lista = [1, 2, 3, 4, 5]
tupla = tuple(lista)

# Desempacotamento de lista
listanum = [1, 2, 3]
num1, num2, num3 = listanum

# OBS: se for colocado um número diferente de elementos para desempacotar, teremos ValueError

# Copiando uma lista para outra ********************* (Shallow copy e Deep copy)
lista = [1, 2, 3]

nova = lista.copy()  # <--------------------------------
# muito cobrado em entrevistas de emprego
# Deep copy

# São listas diferentes, modificações em uma não afetam em nada a outra

lista = nova  # nesse modo, as alterações valerão para ambas as listas
# Shallow copy
