'''
Tipo String

Em python, um dado é considerado do tipo String sempre que:
Estiver em aspas simples -> 'joao', '125', '4548454848', 'jonnys'
Estiver entre aspas duplas -> "joao", "125", "jdsufsfu"
Estiver entre aspas simples triplas ->
Estiver entre aspas duplas triplas

'''

nome = 'joao correia'
print(nome)
print(type(nome))

nome = 'Anjelina ' \
       ' Jolie'
print(nome)

print(nome.upper())  # Tudo maiúsculo
print(nome.lower())  # Tudo minúsculo
print(nome.split())  # Transforma em uma lista de strings
print(nome[0:3])  # Imprime do elemento 0 ao elemento 2 - Slice de string

'''
[::-1] -> comece do primeiro elemento, vá até o último elemento e inverta
'''
print(nome[::-1])

# print(nome.replace(('e', 'i')))

