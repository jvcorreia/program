'''
Módulo Collections - Counter (Contador)

Collections -> High-performance Container Datetypes

Counter -> Recebe um iterável como parâmetro e cria um objeto do tipo Collections Counter
que é parecido com um dicionário, contendo como chave o elemento da lista passada como parâmetrp
e como valor  a quantidade de ocorrências desse elemento.

'''

#Utilizando o Counter

from collections import Counter

#Podemps utilizar qualquer iterável, aqui estamos usando lista
lista = [1, 1, 1, 2, 2, 3, 3, 3, 3, 3, 4, 5, 6, 6, 6, 6, 6]

res = Counter(lista)

print(type(res))#<class 'collections.Counter'>
print(res)#Counter({3: 5, 6: 5, 1: 3, 2: 2, 4: 1, 5: 1})

#Para cada elemento da lista, o counter criou uma chave e colocou o valor a quantidade de ocorrências

print(Counter('joao victor correia'))#Counter({'o': 4, 'r': 3, 'a': 2, ' ': 2, 'i': 2, 'c': 2, 'j': 1, 'v': 1, 't': 1, 'e': 1})

hino_nacional = ''' 
Ouviram do Ipiranga as margens plácidas
De um povo heróico o brado retumbante,
E o sol da Liberdade, em raios fúlgidos,
Brilhou no céu da Pátria nesse instante.
Se o penhor dessa igualdade
Conseguimos conquistar com braço forte,
Em teu seio, ó Liberdade,
Desafia o nosso peito a própria morte!
Ó Pátria amada, Idolatrada, Salve! Salve!
Brasil, um sonho intenso, um raio vívido
De amor e de esperança à terra desce,
Se em teu formoso céu, risonho e límpido,
A imagem do Cruzeiro resplandece.
Gigante pela própria natureza,
És belo, és forte, impávido colosso,
E o teu futuro espelha essa grandeza
Terra adorada, Entre outras mil,
És tu, Brasil, Ó Pátria amada!
Dos filhos deste solo és mãe gentil,
Pátria amada, Brasil!
Deitado eternamente em berço esplêndido,
Ao som do mar e à luz do céu profundo,
Fulguras, ó Brasil, florão da América,
Iluminado ao sol do Novo Mundo!
Do que a terra mais garrida
Teus risonhos, lindos campos têm mais flores;
"Nossos bosques têm mais vida",
"Nossa vida" no teu seio "mais amores".
Ó Pátria amada, Idolatrada, Salve! Salve!
Brasil, de amor eterno seja símbolo
O lábaro que ostentas estrelado,
E diga o verde-louro desta flâmula
Paz no futuro e glória no passado.
Mas, se ergues da justiça a clava forte,
Verás que um filho teu não foge à luta,
Nem teme, quem te adora, a própria morte!
Terra adorada Entre outras mil,
És tu, Brasil, Ó Pátria amada!
Dos filhos deste solo és mãe gentil,
Pátria amada, Brasil!
'''

print(Counter(hino_nacional))

palavras = hino_nacional.split()  # Separando os elementos por palavras
print(Counter(palavras))

res = Counter(palavras)

print(res.most_common(5))  # 5 palavras mais comuns no hino nacional
