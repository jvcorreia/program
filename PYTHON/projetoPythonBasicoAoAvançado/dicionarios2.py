'''
(PARTE 2)
Mapas -> Conhecidos em python como dicionários
Representados por {}
'''

receita = {'janeiro': 1400, 'feveiro': 100}

for chave in receita:
    print(chave)

for chave in receita:
    print(receita[chave])

print(receita.keys())

# Acessando os valores
print(receita.values())

# Desempacotamento de dicionário
for chave, valor in receita.items():
    print(chave, valor)

# Soma, valor max, valor min, tamanho
# somente se forem inteiros ou reias
print(sum(receita.values()))
print(max(receita.values()))
print(len(receita.values()))
