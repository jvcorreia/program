'''
Módulo Collections - Default Dict

#Recap dicionários

print(dict)

print(dict['curso'])

print(dict['outro']) #KeyError

Default Dict -> ao criar o dicionário utlizando-a, nós informamos um valor default,
podendo utilizar um lambda par isso. Esse valor será utilizado sempre que não houver
um valor definido. Caso tentemos acessar uma chave que não existe, essa chave será criada,
e o valor default será atribuido.

OBS: lambdas são funções sem nome, que podem ou não receber valores de entrada e retornar valores
'''
from collections import defaultdict
dicionario = defaultdict(lambda: 0)

dicionario['aluno'] = 'joao'

print(dicionario['uhu'])  # Return = 0



