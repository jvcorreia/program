import mysql.connector
from mysql.connector import Error
try:
    connection = mysql.connector.connect(host='localhost',
                             database='escola_curso',
                             user='root',
                             password='root')
    if connection.is_connected():
       db_Info = connection.get_server_info()
       print("Connected to MySQL database... MySQL Server version on ", db_Info)
       cursor = connection.cursor()
       cursor.execute("select database();")
       record = cursor.fetchone()
       print ("Your connected to - ", record)
except Error as e:
    print("Error while connecting to MySQL", e)

c = connection.cursor()


# Função select - ver dados
def select(fields, tables, where=None):

    global c

    query = "SELECT " + fields + " From " + tables
    if where:
        query = query + " WHERE " + where

    c.execute(query)
    return c.fetchall()


print(select("nome", "alunos", "id_aluno = 1"))


# Função Insert - inserir dados
def insert(values, table, fields=None):

    global c, connection

    query = "INSERT INTO " + table
    if fields:
        query = query + "(" + fields + ")"
    query = query + " VALUES " + ",".join(["(" + v + ")" for v in values])

    c.execute(query)
    connection.commit()


values = [
    "DEFAULT, 'JOaaAO', '2000-01-01', '2 AV BLOCO 600', 'BSB','DF'"
         ]

insert(values, "alunos")
print(select("*", 'alunos'))

# Função update - atualizar dados
'''
UPDATE table
SET field = value, field = value
WHERE
'''


def update(sets, table, where=None):

    global c, connection

    query = "UPDATE " +table
    query = query + " SET " + ",".join([field + " = '" + value + "'" for field, value in sets.items()])
    if where:
        query = query + " WHERE " + where

    c.execute(query)
    connection.commit()


update({"nome": "joao", "estado": "xp"}, "alunos" , "id_aluno = 1")
print(select("*", 'alunos'))

# Função delete - deletar dados
'''
DELETE FROM table
WHERE where
'''


def delete(table, where):

    global c, connection

    query = "DELETE FROM " + table + " WHERE " + where

    c.execute(query)
    connection.commit()
    print(delete("alunos", "id_aluno = 1"))
