'''
Saindo de loops com break

Funciona da mesma forma que C ou JAVA

Utilizamos o break para sair de loops de maneira projetada
'''

for numero in range (1,10):
    if numero == 6:
        break
    else:
        print(numero)

while True:
    comando = input("digite sair para sair")
    if comando == 'sair':
        break
