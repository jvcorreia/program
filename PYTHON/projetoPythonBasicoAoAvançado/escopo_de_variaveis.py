'''
Escopo de variáveis

Dois casos de escopo:

1- Variáveis globais;
    -Variáveis globais são reconhecidas, ou seja, seu escopo compreende todo o programa

2- Variáveis locais;
    -Variáveis locais são reconhecidas apenas no bloco onde foram declaradas, ou seja
    seu escopo está limitado ao bloco onde foi declarada.

Para declarar variáveis em python fazemos:

nome_da_variavel = valor_da_variavel

Python é uma linguagem de tipagem dinâmica -> ao declarar uma variável,
não atribuimos o tipo de dado, que já é inferido

Exemplo em JAVA:
    int numero = 42;
'''

numero = 42
print(numero)
print(type(numero))

numero = 'geek'
print(numero)
print(type(numero))

numero = 42
novo = 0
if numero > 10:  # Só vai entrar se o número for maior, por isso o novo deve ser declarado antes
    novo = numero + 10
    print(novo)

print(novo)

