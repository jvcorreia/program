'''
Loop while

while expressão booleana:
    execução do loop

O bloco do while será repetido enquanto a expressão booleana for verdadeira

Expressão booleana é toda expressão onde o resultado é verdadeiro ou falso

num = 5

num < 5 -> return false
'''

# exemplo 1
num = 1
while num < 10:
    print(num)
    num += 1

# Em um loop while, é importante que cuidemos do critério de parada

# exemplo 2
resposta = ''
while resposta != 'sim':
    resposta = input('você está cansado?')

