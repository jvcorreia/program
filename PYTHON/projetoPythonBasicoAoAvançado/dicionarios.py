'''
Dicionários

OBS: Em algumas linguagens de programação, os dicionários python são conhecidos como
mapas.

Dicionários são coleções do tipo chave/valor

Dicionários são representados por chaves {}

OBS: tanto a chave quanto os valores podem ser de qualquer tipo,ou seja ,
podemos misturar tipos de dados. São separados por ponto e vírgula
respectivamente
'''

print(type({'br': 'Brasil', 'eua': 'Estados Unidos'}))#Return: dict

# Forma 1 (mais comum)
paises = {'br': 'Brasil', 'eua': 'Estados Unidos'}
print(paises)
print(type(paises))

# Forma 2 (menos comum)
paises = dict(br='brasil', eua='estados unidos')
print(type(paises))

paises = {'br': 'Brasil', 'eua': 'Estados Unidos'}
# Acessando elementos
# Forma 1: acessando via chave, mesma forma que lista e tupla
print(paises['br'])

# Caso tentamos fazer um acesso utilizando uma chave que não exista, teremos erro (KeyError)

# Forma 2: Acessando via get (recomendado) <-
print(paises.get('br'))
# Nesse caso não temos o KeyError, pois quando é chamado uma chave que não exista, ele retorna None.

paises = {'br': 'Brasil', 'eua': 'Estados Unidos'}


# Podemos definir um padrão para caso não encontremos o objeto com a chave informada
russia = print(paises.get('ru', 'não encontrado'))  # Usando assim, um if não é necessário


if russia:
    print("encontrei")
else:
    print(f'não encontrei {russia}')

paises = {'br': 'Brasil', 'eua': 'Estados Unidos'}

# Verificando se os elementos estão no dicionário. Ele não busca por valor***********
print('eua' in paises)
print('Estados Unidos' in paises)

if 'ru' in paises:
    print('sim')


# Mostrando que se pode utilizar qualquer tipo de dado em dicionários.
# Tuplas por exemplo são bastante interessantes de serem utilizadas como chave de dicionários, pois
# as mesmas são imutáveis
localidades = {
    (3232.232, 3232.344): 'escritorio em tokyo',
    (3272.232, 3275.344): 'escritorio em dubai',
}

# Adicionar elementos em dicionários

receita = {'abril': 1250, 'maio':100, 'junho':100}

# Forma 1 (mais comum)

receita['julho'] = 3000
print(receita)

# Forma 2
novo_dado = {'agosto': 600}

receita.update(novo_dado)  # receita.update({agosto: 600})
print(receita)

# Atualizando dados em um dicionário

receita = {'maio': 150, }

# Forma 2
receita.update({'abril': 100})
print(receita)

# OBS: a forma de adicionar e atualizar elementos em uma lista é a mesma
# OBS: Em dicionários, NÃO podemos ter chaves repetidas.

# Remover dados de um dicionário
receita = {'abril': 1250, 'maio':100, 'junho':100}

# Forma 1
receita.pop('abril')

rem = print(receita.pop('maio'))
# OBS: aqui precisamos sempre informar a chave a chave, caso não encontre um KeyError é retornado
# OBS: ao removermos um objeto, o valor dele é sempre retornado

# Forma
receita = {'abril': 1250, 'maio':100, 'junho':100}
del receita['abril'] #Não retorna valor
# Também gera erro caso a chave não exista

# Imagine que você tem um comércio eletrônico, onde temos um carrinho de compras na qual adicionamos
# produtos

'''
Carrinho de comprar:
    Produto 1:
        -nome
        -quantidade
        -preço
        
    Produto 2:
        -nome
        -quantidade
        -preço
        
'''
# 1 - Poderiamos utilizar uma lista para isso? sim

carrinho = []

produto1 = ['ps4', 1, 230.00]
produto2 = ['celular', 1, 1987.50]

carrinho.append(produto1)
carrinho.append(produto2)

print(carrinho)

# Teriamos que saber qual o indice de cada informação no produto.

# 2 - Poderiamos utilizar uma tupla para isso? sim
produto1 = ('ps4', 1, 3000)
produto2 = ('tablet', 1, 2700)

carrinho = (produto1, produto2)

print(carrinho)

# 3 - poderiamos utilizar um dicionário para isso? sim
carrinho = []

produto1 = {'nome': 'ps4', 'preço': 2300, 'quantidade':1}
produto2 = {'nome': 'rx7400', 'preço': 23033, 'quantidade':1}

carrinho.append(produto1)
carrinho.append(produto2)

print(carrinho)
# Forma bem mais detalhada


# Métodos de dicionários

d = dict(a= 1, b= 2, c= 3)

# Limpar o dicionario
d.clear()

d = dict(a= 1, b= 2, c= 3)

# Copiando dicionário #Deep copy

novo = d.copy()

# Forma 2 #Shallow copy

novo = d

# Forma não usual de criação de dicionários

outro = {}.fromkeys('a', 'b')  # chave, valor
outro = {}.fromkeys(['nome', 'pontos', 'email', 'profile'], 'desconhecido')  # atribuindo um valor a varias chaves
print(outro)


dictcores = {'blue':'azul'}
for key in dictcores:
    print(key + " " + dictcores[key])  # Imprime chave e valor
