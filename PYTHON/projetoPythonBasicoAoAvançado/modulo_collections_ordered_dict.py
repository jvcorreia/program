'''
Módulo collections: Orderes Dict
'''

# Em um dicionário, a ordem dos elementos não é garantida
dict = {'a': 1, 'b': 4, 'c': 0}

for chave, valor in dict.items():
    print(f'chave = {chave}, valor {valor}')

from collections import OrderedDict

dicionario = OrderedDict({'a': 1, 'b': 2, 'c': 3})

# Orderer dict é um dicionário que nos garante a ordem de inserção dos elementos
