'''
Conjuntos

- Conjuntos em qualquer linguagem de programação, estamos fazendo referência á teoria dos
conjuntos da matemática.

- Aqui no python, os conjuntos são chamados de Sets.

Dito isto, da mesma forma que na matemática:
- Sets (conjuntos) não possuem valores duplicados;
- Sets (conjuntos) não possuem valores ordenados;
- Elementos não são acessados via índice, ou seja, conjuntos não são indexados

Conjuntos são bons para se utilizar quando precisamos armazenar elementos
mas não nos importamos com a ordenação deles. Quando não precisamos se preocupar
com chaves, valores e intens duplicados

Os conjuntos (Sets) são referenciados em python com chaves {}

Diferença entre conjuntos e mapas em python:
    - Dicionário tem chave e valor, conjunto apenas valor

'''

# Definindo conjuntos em python

# Forma 1
s = set({1, 2, 3, 4, 5, 6, 2, 3, 4, 4})  # Repare que temos valores repetidos
print(s)
print(type(set))

# OBS: ao criar um conjunto, ao adicionar um valor existente, ele será ignorado porém não irá gerar erros

# Forma 2 (mais comum)
s = {1, 1, 2, 3, 4, 4, 3, 6}
print(s)


# Verificando a existencia de elemento no conjunto
if 3 in s:
    print('sim')
else:
    print('nada')

# Importante lembrar que, além de não termos valores duplicados, não temos ordem
tupla = (2, 2, 1, 20, 5, 3, 4, 55)  # Aceitam valores duplicados
lista = [2, 2, 1, 20, 5, 3, 4, 55]  # Aceitam valores duplicados
s = {2, 2, 1, 20, 5, 3, 4, 55}  # Não são duplicados
dict = {}.fromkeys([2, 2, 5, 60, 7], 'dict')  # Não são duplicados
print(tupla)
print(lista)
print(s)
print(dict)

# Assim como todo outro conjunto python podemos colocar tipos de dados misturados em sets

S = {2, 'b', 1.56, 24242525242, 'lucas', True}
print(s)

# Podemos iterar em um set normalmente
for valor in s:
    print(valor)

cidades = ['bh', 'sp', 'bsb', 'cuiaba']
print(len(set(cidades)))  # Printando o número de componente, pode ser usado para tirar duplicações

# Adicionando elementos em um oonjunto
num = {2, 3, 4, 6}
num.add(5)
print(num)  # Duplicidade não gera erro

# Adicionando elementos em um oonjunto
num = {2, 3, 4, 5}

# Forma 1
num.remove(5)  # Não é índice. Se o valor não existir, será gerado o erro KeyError

# Forma 2
num.discard(2)

print(num)  # Duplicidade não gera erro

# Copiando elementos
novo = num.copy()  # Deep copy
novo = num  # Shallow copy


# Removendo todos os itens de um elemento
num.clear()

# Operações com conjuntos
curso_python = {'marcos', 'joao', 'lucas', 'jones'}
curso_java = {'joao', 'diana', 'superman'}

# Veja que alguns fazem os dois cursos

# Precisamos gerar um conjunto com nomes de estudantes únicos
# Forma 1 - utilizando union
unico = curso_java.union(curso_python)

# Forma 2 - utilizando o caractere pipe |
unico2 = curso_python | curso_python

print(unico)
print(unico2)

ambos = curso_python & curso_java  # Estudantes dos dois cursos

umououtro = curso_java.difference(curso_python)  # Estudantes java - estudantes python

num = {2, 3, 4, 6}
print(max(num))  # Valor max
print(min(num))  # Valor min
print(sum(num))  # Soma
print(len(num))  # Número de elementos
