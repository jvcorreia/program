'''
Tipo booleano

Algebra booleana, criada por George Boole

2 constantes, verdadeiro ou false

OBS: sempre com a inicial maiúscula

Errado -> true, false

Certo -> True, False
'''

ativo = True

print(ativo)

'''
Operações básicas:
'''

# Negação (not):
'''
Fazendo a negação, se o valor for verdadeiro o resultado será false
se o valor for falso o resultado será verdadeiro, ou seja, sempre o contrario
'''

print(not(ativo))

logado = False

# Ou (or):
'''
É uma operação binária, ou seja, depende de dois valores
Um ou outro deve ser verdadeiro

True or True = True
True or False = True
False or True = True
False or False = False
'''
print(ativo or logado)

# E (and):
'''
Também é uma operação binária, ou seja, depende de dois valores
Ambos devem ser verdadeiros

True and True = True
True and False = False
False and True = False
False and False = False
'''
print(ativo and logado)
