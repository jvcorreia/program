'''
Tuplas

Tuplas são bastante parecidas com as listas,
existem 2 diferenças básicas:
    1- As tuplas são representadas por parênteses ()
    2- As tuplas são imutáveis, ou seja, ao se criar uma tupla, não é possível altera-la.
'''

# Cuidado, As tuplas são representadas por (), mas veja:

tupla1 = (1, 2, 3, 4, 5, 6)
print(tupla1)
print(type(tupla1))

tupla2 = 1, 2, 3, 4, 5, 6
print(tupla2)
print(type(tupla2))

# Cuidado 2, tuplas com 1 elemento:
tupla3 = (4)  # O python considera como int**
print(tupla3)
print(type(tupla3))

tupla4 = (5,)  # Isso é uma tupla
tupla5 = 5,  # Isso é uma tupla
print(type(tupla4))
print(type(tupla5))

# Conclusão: podemos concluir que as tuplas são definidas pela vírgula, e não pelo uso
# dos parênteses

# Podemos gerar uma tupla dinamicamente com range
tupla = tuple(range(11))
print(tupla)

# Desempacotamento de tupla
tupla = ('joao', 'victor')

primeiro_nome, segundo_nome = tupla  # Atribuindo variáveis aos componentes da tupla
print(primeiro_nome)
print(segundo_nome)

# Gera ValueError se colocarmos um número diferente de elementos para desempacotar

# Métodos para adição e remoção de elementos não existem nas tuplas

print(sum(tupla2))  # Somar todos os números de uma tupla
print(max(tupla2))  # Valor máximo
print(min(tupla2))  # Valor mínimo
print(len(tupla2))  # Número de elementos

# Concatenação de elementos
tupla1 = (1, 2, 3)
tupla2 = (4, 5, 6)

print(tupla1 + tupla2)  # Observe que a junção só serve para esse print.
print(tupla1)
print(tupla2)

tupla3 = (tupla1 + tupla2)  # Já aqui ela fica alterada dentro da variável,
# tuplas são imutáveis, mas podemos sobrescrever seus valores

print(33 in tupla2)# Return = false


# Iterando sobre uma tupla:
for n in tupla2:
    print(n)

for indice, valor in enumerate(tupla2):  # Relembrando: o "enumerate" imprime o valor juntamento com o índice
    print(indice, valor)



tupla1 = (1, 2, 3, 4)
tupla2 = (3, 7, 8)
tupla3 = tupla1 + tupla2
print(tupla3)

# contando os elementos de uma lista
lista1 = ['joao', 'victor', 'joao']
print(lista1.count('joao'))  # Return: 2

# Dicas na utilização de tuplas

# Devemos utilizar tuplas sempre que não formos modificar os dados contidos

# Exemplo 1
meses = ('janeiro', 'fevereiro', 'março', 'abril', 'maio', 'junho', 'julho')

# O jeito de acessar a lista é parecido com o da lista

escola = tuple('joao victor')
print(escola)
print(escola.count('o'))

# Dicas na utilização de tuplas
print(meses.index('maio'))  # Vendo qual o índice do elemento, caso o elemento não exista será gerado ERRO

# Slicing (inicio:final:passo)
print(meses[2:4])

# Porquê utilizar tuplas

# Tuplas são mais rápidas que listas
# Tuplas são mais seguras

