'''
Entendendo o *args

-> é um parâmetro qualquer, e você poderá chama-lo de qualquer coisa, desde que esteja com asterisco.

exemplo:
*xis

mas por convenção, é adotado o *args

o parâmetro *args utilizado em uma função coloca os valores armazenados em uma tupla.
'''

#Exemplos

def soma (*args):
    total = 0
    for numero in args:
        total = total + numero
    print(total)

soma(1)
soma(1, 4)
soma(1, 6, 8)

'''
é basicamente uma função que pode receber n parâmetros que não foram especificados
'''