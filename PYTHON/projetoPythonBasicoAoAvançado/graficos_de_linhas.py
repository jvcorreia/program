'''
Gráficos de linhas
Vizualização de dados em python
'''

import matplotlib.pyplot as plt

x = [1, 2, 5, 6]
y = [2, 3, 7, 0]
z = [200, 25, 400, 3300, 100]


plt.plot(x, y)  # Cria o gráfico
plt.show()  # Mostra o gráfico
plt.bar(x, y)  # Cria o gráfico de barras*
plt.show()  # Mostra o gráfico



# Inserindo legendas no gráfico
plt.title("Primeiro gráfico com python")  # Título
plt.xlabel("eixo x")
plt.ylabel("eixo y")

plt.plot(x, y, color="#000000", linestyle="--")  # Coloca linhas entre os pontos
plt.scatter(x, y, label="Meus pontos", color="k", marker=".", s=z)  # Pontilhado
plt.legend()
plt.show()
plt.savefig()  # salvar a foto
