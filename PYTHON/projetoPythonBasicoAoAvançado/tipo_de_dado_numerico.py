'''
Tipo numérico
'''

num = 1_000_000
print(num)

'''
5/2 = 2.5

5//2 = 2

5%2 = 1

5**2 = 25

CTRL + L = limpa o console

num = 43
num += 1 --> num = num + 1
num -= 1 --> 42
num *=2 --> 86
num /=2 --> 21

type(num)
return = int
'''