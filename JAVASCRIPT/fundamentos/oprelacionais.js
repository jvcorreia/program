console.log('1' == 1); //Retorna true, pois em um primeiro momento ele não considera o tipo da variável,
                        // apenas o dado em si

console.log('1' === 1); //Já nessa ele retorna false, pois faz uma comparação mais 'profunda'

console.log('3' !== 3);