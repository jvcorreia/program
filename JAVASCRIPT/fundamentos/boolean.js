let isAtivo = false;
console.log(isAtivo);

isAtivo = 1;
console.log(!!isAtivo);//Verificando se um valor é verdadeiro ou falso

console.log('os verdadeiros');
console.log(!!3);
console.log(!!-3);
console.log(!!' ');
console.log(!![]);

console.log('os falsos');
console.log(!!0);
console.log(!!"");
console.log(!!null);
console.log(!!undefined);

let nome = '';//Se tiver nome ele imprime o nome, se não tiver ele joga 'indefinido'
console.log(nome||'indefinido');