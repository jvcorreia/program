const peso1 = 1.0;
const peso2 = Number('2.0'); //Uma forma de declarar números usando String
console.log(Number.isInteger(peso1));
/**
 * Em outras linguagems, o numéro de peso1 seria considerado um número do tipo float,
 * porém aqui os floats terminados em 0 são considerados inteiros'
 */

 const avaliacao1 = 9.653;
 const avaliacao2 = 6.3254;

 const total = avaliacao1 * peso1 + avaliacao2 + peso2;
 console.log(total);

 const media = total/ peso1 + peso2;
 console.log(media.toFixed(2));// Passar o número de casas decimais para o console imprimir
 
 console.log(media.toString());//Converte em String

 console.log(media.toString(2));//Converte em binário

 /**
  * Alguns cuidados
  * 
  * em caso de 7/0 -> o console retorna "infinity"
  * 
  * o JS também aceita "10"/2
  */

  console.log(0.1 + 0.7); //Return: 0.7999999.....