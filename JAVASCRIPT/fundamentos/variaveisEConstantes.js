var a = 3; 
let b = 4; //Recomendado criar variáveis usando let.

var a = 30;
 b = 40;

console.log(a, b);

a = 300;
b = 400;

console.log(a, b);


const c = 5;//Como o próprio nome já diz, deve ser usada para valores que não serão modificados(números e letras).

console.log(c);

/**
 * Usando var, pode se declarar a mesma variável duas vezes.
 */