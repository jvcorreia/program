//Função sem retorno
function imprimeSoma(valor1, valor2){
    console.log(valor1 + valor2);
}

imprimeSoma(1, 3);
// É possível passar apenas um valor, mas o outro será definido como undefined.

//Função com retorno
let a = 1;
let b = 2;
function retornaSoma(){
    return a + b;
}
console.log(retornaSoma());