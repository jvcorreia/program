const escola = "dom pedro ii";

console.log(escola.charAt(2)); //Imprimindo a letra do índice 3
console.log(escola.charAt(88));//-> Retorna um campo vazio, não apontando "erro"
console.log(escola.charCodeAt(2));//Valor dentro da tabela asc
console.log(escola.indexOf('d'));//Retorna o índice da letra
console.log(escola.substring(2,7));//Retorna uma parte específica da string
console.log("brazil zil zil".concat(escola.concat(", isso aí!")));//Inserir mais strings entre a string já presente
//Forma 2:
        console.log("brazil zil zil" + " escola " + "dom pedro ii");
        
console.log(escola.replace("d", "2"));//Trocar uma letra por outra
console.log(escola.replace(/\o/, "2"));//Trocar todos os elementos com essa letra por outra letra
console.log("maria, joao, lucas".split(","));//separar os elementos, passando dentro o elemento que está separando( no caso a virgula)

