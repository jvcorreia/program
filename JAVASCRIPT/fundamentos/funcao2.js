//Armazenando uma função em uma variável
const soma = function(a, b){
    console.log(a + b);
}
soma(4, 5);

//Armazenando função arrow em uma variável
const somar = (a, b) => {
    return a + b;
}
console.log(soma(2, 9));

//Jeito mais reduzido
const subtracao = (a, b) => a-b;
console.log(subtracao(10,2));