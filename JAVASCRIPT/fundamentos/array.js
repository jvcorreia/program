const array = [7.6, 8.9, 4.4];
console.log(array[0]);

console.log(array[3]);//Acessando um valor que não existe --> return: undefined
array[4] = 5;

console.log(array[4]);

console.log(array);

console.log(array.lastIndexOf());

// Apesar de ser permitido, não é tão recomendado misturar dados dentro de arrays.
delete array[0];//Deletando
console.log(array);

console.log(typeof array);//Em JS as listas são do tipo object

