function verifica(nota){
        if(nota >= 7){
            console.log("aprovado com " + nota);
        }   
}
verifica(8);
verifica(9);

const imprimir = function(valor){
    if(valor > 50){
        console.log("velho");
    }
    else{
        console.log('novo');
    }
}

imprimir(49);
imprimir(51);
imprimir('epa!');//Não irá gerar erro, cuidado. Retornará false

Number.prototype.entre = function (inicio, fim) { //Criando um método que verifica se um número está entre dois números
        return this >= inicio && this <= fim;
  }

const imprimivalor = function(valor){
    if(valor.entre(7, 10)){
        console.log("está entre 7 e 10");
    }
    else if (valor.entre(10, 20)){
        console.log("está entre 10 e 20");
    }
    else{
        console.log("não se encaixa");
    }

}

imprimivalor(8);
imprimivalor(12);
