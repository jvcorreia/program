let qualquer = 'legal';
console.log(typeof qualquer); // Comando para receber o tipo de variável

qualquer = 3.1516;
console.log(typeof qualquer); // Comando para receber o tipo de variável

/**
 * O Javascript é uma linguagem de tipagem fraca, ou seja, diferentemente de linguagens como java
 * se tem maior flexibilidade com o manejo de estrutura de dados, variáveis e etc. A mesma variável
 * pode ter multiplos valores.
 * 
 * Evitar nome genéricos e siglas para variáveis
 */

