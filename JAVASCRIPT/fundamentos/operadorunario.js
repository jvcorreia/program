let a = 2;
a++;
console.log(a);
a--;
--a;
console.log(a);//O dois acima servem para diminuir em uma unidade, porém fazem diferença ao comparar

let b = 3;
let c = 2;

console.log(c === --b);
console.log(c === b--);