/*
função são objetos de primeira classe dentro do js

1 - função de escrita literal

function funcao(){ }  o bloco {} é obrigatório

2 - colocando uma função em uma variável

var xx = function(){}

3 - armazenar dentro de um array

const array = [function (a, b){ return a + b;}, funcao, xx]

4 - uma função pode retornar outra função

function retorna (a, b){
    return function outra(c){
        console.log(a + b + c);
    }
}

retorna(1, 4)(5); output: 10
*/
