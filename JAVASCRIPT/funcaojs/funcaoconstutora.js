function Carro (velocidademax = 200, delta = 5) { //Funciona como classe
     //Atributo privado
     let velocidadeatual = 0;

     //Método público
     this.acelerar = function () {
         if(velocidadeatual + delta <= velocidademax){
             velocidadeatual += delta;
         }
         else{
             velocidadeatual = velocidademax;
         }
       }

    //Métoso público
       this.getVelocidadeAtual = function () {
           return velocidadeatual;
         }
  }

  const celta = new Carro();//Usa os parâmetros padrões
  celta.acelerar();
  console.log(celta.getVelocidadeAtual());