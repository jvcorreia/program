//Abaixo faremos uma função que não determina o número de parâmetros que poderão ser passados
function soma() {
    let somar = 0;
    for (const i in arguments) { 
         somar += arguments[i]; //Pega cada um dos argumentos e soma
    }
    console.log(somar);
}

soma();
soma(2, 3);
soma(2, 3, 4.548748);

