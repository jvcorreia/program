//Função factory é uma função que retorna um obj

function factory(nome, idade){
    return {nome,
        idade
    }
}

const joao = factory('joao victor', 19);
console.log(joao);
