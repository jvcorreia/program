const marcas = ['adidas', 'nike', 'lacoste', 'oakley'];

function imprimir(nome, indice) {
    console.log(nome);
    console.log(indice + 1);
}

marcas.forEach(imprimir);

marcas.forEach(function(a){
    console.log(a);
})

let notas = [3.5, 8.7, 5.6, 9.2, 8.2, 2.1, 4.9];

let notasbaixas = [];

//Sem callback
for (const i in notas) {
    if(notas[i] <= 5){
        notasbaixas.push(notas[i]);
    }
}
console.log(notasbaixas);

//Com callback
notasaltas = notas.filter(function(i){ //O filter server para filtrar uma lista, se determinada condição retornar true,
    return i > 5;                       // ele inclui na lista.
});

console.log(notasaltas);

let notamedia = (nota) => nota = 5; 