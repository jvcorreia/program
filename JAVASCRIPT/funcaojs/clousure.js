//Clousure é um escopo criado quando a função é declarada, que permite acessar valores fora da função

const x = 'global';

function fora(){
    const x = 'local';
    function dentro(){
        return x;
    }
    return dentro();
}

const retornafora = fora();
console.log(retornafora);