class Pessoa {

    constructor (nome){
        this.nome = nome;
    }

    falar(){
        console.log("meu nome é " + this.nome );
    }
}

//Função construtora
function Pessoa (nome){
        this.nome = nome;
    
     this.falar = function(){
        console.log("meu nome é " + this.nome );
    }
}

const p1 = new Pessoa ('joao');
p1.falar();

const pessoa = nome => {
    return {
        falar: () => console.log( 'meu nome é ' + nome)
    }
}
const p2 = pessoa ('jubs');
p2.falar();