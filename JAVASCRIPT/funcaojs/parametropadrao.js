//Estratégia para gerar um valor padrão
function multiplica(a, b, c){
    a = a || 1;
    b = b || 2;
    c = c || 3;
    console.log(a * b * c);
}

multiplica(4, 4, 4);
multiplica();

//Outra estratégia (mais usada)
function soma(a, b ) {
    a = isNaN(a)? 1 : a; //Se a não for um número, passe o valor 1. Se a for um número, passe a.
    console.log(a + b);
}

soma("sim", 2);
