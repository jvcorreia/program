//IIFE -> IMMEDIATELLY INVOKED FUNCTION EXPRESS

(function () {  
    console.log("será executado na hora...");
    console.log("foge do escopo abrangente");
})() //Repara que após finalizar eu imediatamente chamei a função
//o IIFE serve para executar uma parte de código de modo que fique isolado do escopo global