const fs = require('fs');//File system, módulo que já vem no node

const caminho = __dirname + '/arquivo.json';

//Sincrono
const conteudo = fs.readFileSync(caminho, 'utf-8');
console.log(conteudo);

//Assincrona -> considerada a melhor forma para ler um json
fs.readFile(caminho, 'utf-8', (err, conteudo) =>{
    const config = JSON.parse(conteudo);
    console.log(config.db.host);
});

const config = require('./arquivo.json');//Se a extensão não for JS, é obrigatório digita-la
console.log(config.db);

fs.readdir(__dirname,(err, arquivos) =>{ //Lendo uma pasta -> JSON
    console.log('a árvore de arquivos:')
    console.log(arquivos);
});

