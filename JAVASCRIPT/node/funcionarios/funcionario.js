const url = 'http://files.cod3r.com.br/curso-js/funcionarios.json'; //Pegando dados de um JSON
const axios = require('axios');

axios.get(url).then(response =>{ 
    const funcionarios = response.data; //Alocando o JSON na variável

//Filtrando dentro do JSON a mulher chinesa com o menor salário.
    function apenaschineses(obj){ //Função para encontrar apenas pessoas que moram na china
       if( obj.pais == 'China'){
            return true;
       }
    }
    
    var filtrochina = funcionarios.filter(apenaschineses);//Fazendo o filtro para encontrar apenas pessoas que moram na china
    //console.log(filtrochina);

    function apenasmulheres(obj){
        if(obj.genero == 'F'){
            return true;
        }
    }

    var filtromulheres = filtrochina.filter(apenasmulheres);
    //console.log(filtromulheres);

    function menorsalario(func, funcatual){
        return func.salario < funcatual.salario? func:funcatual;
            
    }

    var chinesafiltrada = filtromulheres.reduce(menorsalario);
    console.log(chinesafiltrada);


})
