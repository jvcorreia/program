CREATE DATABASE db_facul;

USE db_facul;

CREATE TABLE tb_dis(
	idt_dis INT AUTO_INCREMENT PRIMARY KEY,
    nme_dis VARCHAR(50) NOT NULL,
    num_ch_dis INT NOT NULL);
    
CREATE TABLE tb_alu(
	idt_alu INT AUTO_INCREMENT PRIMARY KEY,
    nme_alu VARCHAR(70) NOT NULL,
    dta_nasc_alu DATE NOT NULL);
    

CREATE TABLE ta_mat(
	idt_mat INT AUTO_INCREMENT PRIMARY KEY,
    cod_dis INT NOT NULL,
    cod_alu INT NOT NULL,
    vlr_nota_mat FLOAT NOT NULL,
    num_falta_mat INT (2) NOT NULL,
    CONSTRAINT fk_dis_mat FOREIGN KEY (cod_dis) REFERENCES tb_dis(idt_dis),
    CONSTRAINT fk_alu_mat FOREIGN KEY (cod_alu) REFERENCES tb_alu(idt_alu));
    
INSERT INTO tb_dis(nme_dis, num_ch_dis)
	VALUES('LTP-II', 75);
    
INSERT INTO tb_alu(nme_alu, dta_nasc_alu)
	VALUES('João Victor', '2000-04-23');

INSERT INTO ta_mat(cod_dis, cod_alu, vlr_nota_mat, num_falta_mat)
	VALUES(1, 1, 9.5, 4);
    
#SELECT DENTRO DE UM SELECT
SELECT (SELECT nme_dis FROM tb_dis WHERE idt_dis = cod_dis) AS disc,
	   (SELECT nme_alu FROM tb_alu WHERE idt_alu = cod_alu) AS alu,
       cod_dis, cod_alu, vlr_nota_mat, num_falta_mat FROM ta_mat;
       
SELECT nme_dis, nme_alu, vlr_nota_mat, num_falta_mat FROM ta_mat, tb_dis, tb_alu
	WHERE idt_dis = cod_dis AND idt_alu = cod_alu;
    
SELECT nme_dis, nme_alu, vlr_nota_mat, num_falta_mat
	FROM ta_mat JOIN tb_dis ON idt_dis = cod_dis
				JOIN tb_alu ON idt_alu = cod_alu;
                
SELECT * FROM tb_dis;

SELECT * FROM tb_alu;

SELECT * FROM ta_mat;

-- COMO MOSTRAR A TURMA DE CALCULO
SELECT nme_dis, nme_alu, vlr_nota_mat, num_falta_mat
	FROM ta_mat JOIN tb_dis ON idt_dis = cod_dis
				JOIN tb_alu ON idt_alu = cod_alu
                WHERE idt_dis =2;
                
-- E SE QUISERMOS SABER QUEM ESTÁ APROVADO OU REPROVADO?
SELECT nme_dis, nme_alu, vlr_nota_mat, num_falta_mat,
	CASE WHEN vlr_nota_mat<5 THEN 'REPROVADO' ELSE 'APROVADO' END AS ap_rp
	FROM ta_mat JOIN tb_dis ON idt_dis = cod_dis
				JOIN tb_alu ON idt_alu = cod_alu
                ;
-- E SE QUISERMOS SABER A MÉDIA DA TURMA?
SELECT AVG(vlr_nota_mat) AS media_nota, AVG(num_falta_mat) AS media_falta
FROM ta_mat;   

-- E SE QUISERMOS SABER A MÉDIA DA TURMA (por disciplina)? 
SELECT nme_dis, AVG(vlr_nota_mat), AVG(num_falta_mat)
	FROM ta_mat JOIN tb_dis ON idt_dis = cod_dis
				JOIN tb_alu ON idt_alu = cod_alu
                GROUP BY nme_dis;
                
-- PRODUTO CARTESIANO (não fazer!!!!!!!)
SELECT nme_dis, nme_alu, vlr_nota_mat, num_falta_mat FROM ta_mat, tb_dis, tb_alu;
            
    /*
CONSTRAINT É ATRIBUIR OS NÚMEROS A NÚMEROS JÁ CONTIDOS EM OUTRA TABELA
SE TENTARMOS USAR UM NUMERO QUE NAO EXISTE NA OUTRA TABELA IRÁ DAR ERRO.
*/