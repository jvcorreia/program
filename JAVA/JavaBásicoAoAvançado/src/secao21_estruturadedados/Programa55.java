package secao21_estruturadedados;
//Armazenamento sequencial

/*
 * int numeros [] = new int [5];
 * 
 * numeros [0] = 1;
 * numeros [1] = 3;
 * numeros [2] = 5;
 * numeros [3] = 7;
 * numeros [4] = 9;
 * 
 */
public class Programa55 {

	public static void main(String[] args) {
		int numeros [] = new int [5];
		numeros[4] = 9;
		System.out.println(numeros[4]);
		//o que teria no primeiro elemento?
		//Resposta: 0, pois o programa preenche automaticamente os outros elementos com numeros 0
		
		Aluno a1 = new Aluno ("joao");
		Aluno a2 = new Aluno ("maria");
		
		Vetor list = new Vetor();
		System.out.println(list.tamanho());
		list.Adiciona(a1);
		System.out.println(list.tamanho());
		list.Adiciona(a2);
		System.out.println(list.tamanho());
		
		System.out.println(list);
		
		System.out.println(list.contem(a1));
		
		System.out.println(list.pega(0));

	}

}
