package secao21_estruturadedados;

import java.util.LinkedList;
import java.util.Queue;

/*
 * Filas
 * 
 * Todo elemento entra no final da fila
 * 
 * - O primeiro elemento a entrar � tamb�m o primeiro a sair
 */
public class Filas {
	public static void main(String[] args) {
		Queue<String> fila = new LinkedList<String>();
		fila.add("joao");
		fila.add("joabs");
		System.out.println(fila);
		String ret = fila.poll();//Remove
		System.out.println(ret);
		
	}

}
