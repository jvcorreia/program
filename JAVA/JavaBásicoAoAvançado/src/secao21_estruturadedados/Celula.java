package secao21_estruturadedados;
/**
 * Classe que representa uma c�lular(container) onde
 * teremos o objeto (valor) e uma celula que ser� a liga��o para o proximo
 * @author Jo�o Victor
 * 
 *
 */
public class Celula {
	private Object elemento;
	private Celula proximo;
	
	public Celula (Object elemento, Celula proximo) {
		this.elemento = elemento;
		this.proximo = proximo;
	}
	
	public Celula getproximo(Celula proximo) {
		return this.proximo;
	}
	
	public void setproximo(Celula proximo) {
		 this.proximo = proximo;
	}
	
	public Object getElemento() {
		return this.elemento;
	}
	

}
