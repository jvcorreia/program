package secao21_estruturadedados;

public class Aluno {
	private String nome;
	
	public Aluno(String nome) {
		this.nome = nome;
	}
	
	public String GetNome() {
		return this.nome;
	}
	
	@Override
	public boolean equals(Object obj) {
		Aluno outro = (Aluno) obj;
		return outro.GetNome().equals(this.nome);
	}
	
	@Override
	public String toString() {
		
		return this.nome;
	}

}
