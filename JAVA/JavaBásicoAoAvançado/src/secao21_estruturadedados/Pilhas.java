package secao21_estruturadedados;

import java.util.Stack;

//Pilha s�o estruturas de dados onde o elemento que est� vis�vil/dispon�vel � o que est�
//sempre no topo.

//Os elementos, ao serem adicionados, s�o adicionados sempre no topo da pilha.

//Para remover elementos da pilha s� podemos remover o topo

/*
 * [4]-> tem visibilidade
 * [3]
 * [2]
 * 
 * O control + z � um bom exemplo de utiliza��o da pilha, a��es do teclado s�o colocadas em pilha
 */
public class Pilhas {
	public static void main(String[] args) {
		Stack<String> pilha = new Stack<String>();
		System.out.println(pilha);
		pilha.push("joao");//Adiciona
		pilha.push("jonnys");
		System.out.println(pilha);
		pilha.pop();//Remove o elemento do topo
		System.out.println(pilha);
		System.out.println(pilha.peek());//Retorna o elemento do topo
		
	}

}
