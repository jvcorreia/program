package secao21_estruturadedados;

import java.util.Arrays;

public class Vetor {
	private Aluno alunos[] = new Aluno[100];
	private int total = 0;
	
	public void Adiciona(Aluno aluno) {
		//Receber aluno
		this.alunos[total] = aluno;
		total = total + 1;
		
		
	}
	
	public Aluno pega(int posicao) {
		//Recebe uma posicao e devolve o aluno
		return this.alunos[posicao];
		
	}
	
	public void remove(int posicao) {
		//Remove aluno pela posicao
		if (posicao > 100) {
			System.out.println("essa posicao n�o est� na lista");
		}
		this.alunos[posicao] = null;
		
	}
	
	public boolean contem (Aluno aluno) {
		//descobre se o aluno est� ou n�o na lista
		for (int i = 0; i < total; i++) {
			if(aluno.equals(alunos[i])) {
				return true;
			}
			
		}
		return false;
	}
	
	public int tamanho() {
		//devolve a quantidade de alunos no vetor
		return total;
	}
	
	@Override
	public String toString() {
		return Arrays.toString(this.alunos);//Facilita a vizualiza��o do array
	}

}
