package secao21_estruturadedados;

public class ListaLigada {
	
	private Celula primeira = null;
	private Celula ultima = null;
	private int total = 0;
	/**
	 * M�todo que adiciona um objeto no come�o da lista
	 * @param elemento
	 */
	public void adicionaNoComeco (Object elemento) {
		//[42] -> [56] -> null
		Celula nova = new Celula(elemento, this.primeira);
		this.primeira = nova;
		if(this.total == 0) {
			this.ultima = this.primeira;
		}
		this.total = this.total +1;
	}
	
	/**
	 * M�todo que adiciona um objeto no final da lista
	 * @param elemento
	 */
	public void adiciona(Object elemento) {
		if(this.total == 0) {
			this.adicionaNoComeco(elemento);
		}
		else {
			Celula nova = new Celula(elemento, null);
			this.ultima.setproximo(nova);
			this.ultima = nova;
			this.total = this.total + 1;
		}
		
		
	}
	
	/**
	 * M�todo que adiciona um elemento no meio da lista de acordo com a posi��o
	 * @param posicao
	 * @param elemento
	 */
	public void adiciona (int posicao, Object elemento) {
		
	}
	
	public Object pega (int posicao) {
		return null;
	}
	
	public void remove (int posicao) {
		
	}
	
	public int tamanho() {
		return 0;
	}
	
	public boolean contem (Object obj) {
		return false;
	}
	
	private boolean PosicaoOcupada(int posicao) {
		return posicao > 0 && posicao < this.total;
		
	}
	
	
	
	@Override
	public String toString() {
		if(total == 0) {
			return "[]";
		}
		Celula atual = primeira;
		StringBuilder builder = new StringBuilder("[");//Montando string
		for (int i = 0; i < total; i++) {
			builder.append(atual.getElemento());
			builder.append(",");
			
			atual = atual.getproximo(atual);
			
		}
		builder.append("]");
		return builder.toString();
	}

}
