package secao21_estruturadedados;
//Lista ligada / LinkedList

/*
 * Em um vetor, os elementos est�o um do lado do outro, enquanto em uma lista 
 * ligada, eles podem estar em lugares diferenres, por�m um aponta para o outro
 * indicando o pr�ximo elemento
 * 
 *[0] [3] [1] [2] [4]
 */
public class Programa57 {

	public static void main(String[] args) {
		ListaLigada lista = new ListaLigada();
		System.out.println(lista);
		lista.adicionaNoComeco("a");
		System.out.println(lista);
		lista.adicionaNoComeco("joao");
		System.out.println(lista);
		lista.adiciona("juca");
		System.out.println(lista);
		
		
		
	}

}
