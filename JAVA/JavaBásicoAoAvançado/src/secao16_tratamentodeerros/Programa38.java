package secao16_tratamentodeerros;

import java.util.Scanner;

public class Programa38 {

	public static void main(String[] args) throws Exception {
		Scanner teclado = new Scanner(System.in);
		
		System.out.println("informe o n1");
		int n1 = teclado.nextInt();
		
		System.out.println("informe o n2");
		int n2 = teclado.nextInt();
		teclado.close();
		
		try {
			System.out.println(divis�o(n1, n2));
			
		} catch (Exception e) {
			System.out.println("n�o � possivel dividir por 0");
		}
		

	}
	
	//Estou criando uma fun��o que avisa que tem possibilidade de lan�ar uma 
	// exce��o do tipo exception
	public static int divis�o (int n1, int n2) throws Exception {
		return n1/n2;
	}

}
