package secao16_tratamentodeerros;
//Tratando exce��es com try/catch

/*
 * Utilizamos o try para tentar realizar algo, geralmente aquilo que pode acarretar em um problema.
 * 
 * utilizamos o catch para capturar o erro, e com isso oferecer uma mensagem adequada
 * sem que o sistema quebre
 */
public class Programa36 {

	public static void main(String[] args) {
		int numeros [] = new int [5];// 0 a 4
		
		for (int i = 0; i < numeros.length; i++) {
			numeros[i] = i + 3 * 2;
		}
		
		for (int i = 0; i <= numeros.length; i++) { // a segunda condi��o deveria ser:
			try {
				System.out.println(numeros[i]);
			} catch (ArrayIndexOutOfBoundsException e) {
				System.out.println("essa posi��o da lista n�o existe");
			} finally {
				System.out.println("fa�a isso tambem");	// i < numeros.lenght, pois o lenght
			}
		}											// � 5 e o array vai de 0 a 4
													// nesse caso, um erro � gerado
	}												//

}
