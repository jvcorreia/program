package secao16_tratamentodeerros;

import java.util.Scanner;

public class Programa37 {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		
		
		System.out.println("digite o primeiro numero para divis�o");
		int num1 = scan.nextInt();
		
		System.out.println("digite o segundo numero para divis�o");
		int num2 = scan.nextInt();
		
		try {
			System.out.println(num1/num2);
		} catch (ArithmeticException e) {
			System.out.println("divis�o por 0 n�o existe");
		}
		finally {
			System.out.println("continua o processo....");//trabalha em conjunto com o try
		}												 // e o catch
	}

}
