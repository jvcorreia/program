package secao05_estruturasderepeticao;
// do while
import java.util.Scanner;
public class Programa08 {

	public static void main(String[] args) {
		int idade = 1;
		String nome;
		Scanner teclado = new Scanner (System.in);
		
		//primeiro ele executa o bloco, depois faz a checagem
		do {
			  
			  System.out.println("informe seu nome");
			  nome = teclado.nextLine();
			  System.out.println("informe sua idade");
			 // bug idade = teclado.nextInt();
			  idade = Integer.parseInt(teclado.nextLine());
			  
			  System.out.println(nome + " tem " + idade + " anos");
			}
		while (idade>0);
			 teclado.close();

	}

}
