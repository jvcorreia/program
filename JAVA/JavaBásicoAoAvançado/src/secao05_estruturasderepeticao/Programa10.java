package secao05_estruturasderepeticao;
import java.util.Scanner;
public class Programa10 {
	public static void main(String[] args) {
		String nome = "joao";
		
		//para cada um dos caracteres da string, imprima o caractere
		
		for (char letra : nome.toCharArray()) {
			// 1 letra por linha System.out.println(letra);
			System.out.print(letra);//tudo em uma linha
			
		}
	}

}
