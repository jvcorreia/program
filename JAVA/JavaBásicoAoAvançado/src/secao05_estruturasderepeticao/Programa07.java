package secao05_estruturasderepeticao;
// while e do while

//receba dados do usuario enquanto a idade for maior que 0

import java.util.Scanner;

public class Programa07 {
public static void main(String[] args) {
	int idade = 1;
	String nome;
	Scanner teclado = new Scanner (System.in);
	
	//while sempre checa o valor antes de executar o bloco
	while (idade > 0) {
	  
	  System.out.println("informe seu nome");
	  nome = teclado.nextLine();
	  System.out.println("informe sua idade");
	 // bug idade = teclado.nextInt();
	  idade = Integer.parseInt(teclado.nextLine());
	  
	  System.out.println(nome + " tem " + idade + " anos");
	}
	 teclado.close();
}
}
