package secao05_estruturasderepeticao;
//for
import java.util.Scanner;
public class Programa09 {

	public static void main(String[] args) {
		int idade = 1;
		String nome;
		Scanner teclado = new Scanner (System.in);
		
		//variavel de controle; condi��o de parada; forma de incremento
		// variavel i = 0, enquanto o i for menor que 5 o loop i++ vai acontecer
		for ( int i = 0; i < 5; i++)	{  
			  System.out.println("informe seu nome");
			  nome = teclado.nextLine();
			  System.out.println("informe sua idade");
			 // bug idade = teclado.nextInt();
			  idade = Integer.parseInt(teclado.nextLine());
			  if (idade > 0) {
				  System.out.println(nome + " tem " + idade + " anos");
			  }
			  
		}
		
			 teclado.close();

	}

}
