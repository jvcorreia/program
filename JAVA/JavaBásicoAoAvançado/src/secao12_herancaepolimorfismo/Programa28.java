package secao12_herancaepolimorfismo;
//Heran�a
public class Programa28 {
		public static void main(String[] args) {
			//Pessoa nova = new Pessoa(); <-- isso j� n�o pode ser usado
			//Pessoa nova = new Pessoa("jonny correia", 1999, "ifuisfs");
			//System.out.println(nova.getAnoNas());
			
			Aluno jonny = new Aluno("joao", 1000, 1000, "nufshufs");
			
			System.out.println(jonny.getAnoNas());
			
			Professor marcos = new Professor ("marcos", 1976, 2037131, "joaoj");
			
			System.out.println(marcos.GetMatricula());
			
			jonny.setEmail("jv");
			
			System.out.println(jonny.getEmail());
	
		}
}
