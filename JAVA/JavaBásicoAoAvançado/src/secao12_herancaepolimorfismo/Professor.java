package secao12_herancaepolimorfismo;

public class Professor extends Pessoa {
	private int matricula;
	
	public Professor( String nome, int ano_nasc, int matricula, String email) {
		super(nome, ano_nasc, email);
		this.matricula = matricula;
	}
	
	public int GetMatricula() {
		return this.matricula;
	}
	
	public void SetMatricula(int matricula) {
		this.matricula = matricula;
	}

	//implementando m�todo abstrado
	@Override
	public void outra_mensagem(String texto) {
		System.out.println(texto);
		
	}
}
