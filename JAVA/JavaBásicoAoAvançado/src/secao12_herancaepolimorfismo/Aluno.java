package secao12_herancaepolimorfismo;
/*
 * Benef�cios da heran�a:
 * 	- Evita a repeti��o do c�digo
 *  - Facilita a manunten��o do programa
 *  
 *  Todo aluno � uma pessoa
 *  
 *  -Quando uma classe herda de outra classe, ela ganha:
 *  	-Todos os atributos e m�todos da classe herdada
 *  
 *  -Classe espec�fica
 *  -Sub-classe
 *  -Classe filha
 */

public class Aluno extends Pessoa { // <- quando queremos utilizar atributos de uma classe
	private int ra;					// em outra classe, usamos o extends
									// Quer dizer : " Aluno � uma pessoa"
	public Aluno (String nome, int ano_nasc, int ra, String email) {
		super(nome, ano_nasc, email); // Para utilizar os atributos da outra classe
		//^ Pessoa(nome, ano_nasc); <- n�o seria aceito
		this.ra = ra;
	}
	public int GetRa() {
		return this.ra;
	}
	
	public void SetRa(int Ra) {
		this.ra = Ra;
	}
	@Override
	public void outra_mensagem(String texto) {
		System.out.println(texto);
		// TODO Auto-generated method stub
		
	}
	
	
}
