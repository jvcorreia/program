package secao12_herancaepolimorfismo;

/*
 * -Classe base, pois serve de base para outras classes (aluno)
 * -Classe gen�rica
 * 	
 */
public abstract class Pessoa {
		private String nome;
		private int ano_nasc;
		private String email;
		
		//Getters e Setters - RELEMBRANDO - Fun��es para acessar (get) e alterar (set) os elementos
		
		//� importante pois obriga o usu�rio, no momento de criar a pessoa, j� declarar
		// o nome e o ano de nascimento. Pra isso tamb�m poderia ser usado o m�todo
		// SetNome, por�m � dificultado o entendimento.
		public Pessoa(String nome, int ano_nasc, String email) {
			this.nome = nome;
			this.ano_nasc = ano_nasc;
			this.email = email;
		}
		
		public String getNome() {
			return this.nome;
		}
		
		public void SetNome(String nome) {
			this.nome = nome;
			
		}
		
		public String getEmail() {
			return this.email;
		}
		
		public void setEmail(String email) {
			this.email = email;
			
		}
		
		public int getAnoNas() {
			return this.ano_nasc;
		}
		
		public void SetAnoNas (int anoNasc) {
			this.ano_nasc = anoNasc;
		}
		
		//Declara��o de um m�todo abstrato
		public abstract void outra_mensagem(String texto);
}
