// Programa que de 2 n�meros, mostra o maior e a diferen�a entre eles
package exerc�ciosComEstruturasDeDecis�o;
import java.util.Scanner;

public class Exercicio6EstruturaDeDecisao {

	public static void main(String[] args) {
		System.out.println("digite o primeiro numero");
		Scanner num1 = new Scanner (System.in);
		double read1 = num1.nextDouble();
		System.out.println("digite o segundo numero");
		Scanner num2 = new Scanner (System.in);
		double read2 = num2.nextDouble();
		double num3 = read1 - read2;
		double num4 = read2 - read1;
		
		if (read1 > read2) {
			System.out.println(read1 + " � maior que" + read2);
			System.out.println("a diferen�a entre eles � " + num3 );
		}
		else {
			System.out.println(read2 + " � maior que" + read1);
			System.out.println("a diferen�a entre eles � " + num4 );
		}
		

	}

}
