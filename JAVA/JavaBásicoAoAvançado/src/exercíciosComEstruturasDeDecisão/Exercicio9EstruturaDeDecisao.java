//leia o salario de um trabalhador e uma parcela de emprestimo, se a parcela for maior que 20% do salario invalidar emprestimo
package exercíciosComEstruturasDeDecisão;
import java.util.Scanner;

public class Exercicio9EstruturaDeDecisao {

	public static void main(String[] args) {
		System.out.println("insira o salario");
		Scanner num1 = new Scanner (System.in);
		double read1 = num1.nextDouble();
		System.out.println("Insira a prestação emprestimo");
		Scanner num2 = new Scanner (System.in);
		double read2 = num2.nextDouble();
		if (read2 > read1/5) {
			System.out.println("emprestimo nao valido");
		}
		else {
			System.out.println("emprestimo valido");
		}

	}

}
