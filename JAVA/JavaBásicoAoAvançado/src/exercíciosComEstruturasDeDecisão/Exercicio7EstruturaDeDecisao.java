//programa que recebe 2 numeros e mostra qual � maior, se forem iguais ele mostra "iguais"
package exerc�ciosComEstruturasDeDecis�o;
import java.util.Scanner;

public class Exercicio7EstruturaDeDecisao {

	public static void main(String[] args) {
		
		
		System.out.println("digite o primeiro numero");
		Scanner num1 = new Scanner (System.in);
		double read1 = num1.nextDouble();
		System.out.println("digite o segundo numero");
		Scanner num2 = new Scanner (System.in);
		double read2 = num2.nextDouble();
		
		
		if (read1 > read2) {
			System.out.println(read1 + " � maior que" + read2);
			
		}
		else if (read2 > read1) {
			System.out.println(read2 + " � maior que" + read1);
		}
		else {
			System.out.println("eles s�o iguais");
		}
	}

}
