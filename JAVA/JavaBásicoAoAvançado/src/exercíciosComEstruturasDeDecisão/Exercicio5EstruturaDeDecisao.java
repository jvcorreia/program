//Programa que verifica se o número é par ou impar
package exercíciosComEstruturasDeDecisão;
import java.util.Scanner;


public class Exercicio5EstruturaDeDecisao {

	public static void main(String[] args) {
		System.out.println("Digite o número:");
		Scanner num = new Scanner (System.in);
		double read = num.nextDouble();
		
		if (read > 0) {
			System.out.println(Math.sqrt(read));
			System.out.println(read*read);
		}

	}

}
