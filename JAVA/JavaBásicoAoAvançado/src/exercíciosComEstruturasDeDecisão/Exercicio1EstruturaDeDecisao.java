//Programa que recebe dois n�meros e diz qual deles � maior
package exerc�ciosComEstruturasDeDecis�o;
import java.util.Scanner;

public class Exercicio1EstruturaDeDecisao {
       public static void main(String[] args) {
	System.out.println("Digite dois n�meros e iremos dizer qual o maior.");
	System.out.println("digite o primeiro:");
	Scanner num1 = new Scanner (System.in);
	int read1 = num1.nextInt();
	System.out.println("agora digite o segundo");
	Scanner num2 = new Scanner (System.in);
	int read2 = num2.nextInt();
	
	if (read1 > read2) {
		System.out.println(read1 + " � maior que " + read2);
	}
	else if (read2 > read1) {
		System.out.println(read2 + " � maior que " + read1);
	}
	else {
		System.out.println("eles s�o iguais");
	}
}
}
