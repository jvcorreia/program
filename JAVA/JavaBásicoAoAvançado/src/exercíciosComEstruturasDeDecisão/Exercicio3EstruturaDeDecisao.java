//leia um numero, se for positivo imprima a raiz quadrada, se for negativo imprima ele ao quadrado
package exercíciosComEstruturasDeDecisão;
import java.util.Scanner;


public class Exercicio3EstruturaDeDecisao {

	public static void main(String[] args) {
		System.out.println("Digite o número:");
		Scanner num = new Scanner (System.in);
		double read = num.nextDouble();
		
		if (read > 0) {
			System.out.println(Math.sqrt(read));
		}
		else if (read < 0) {
			System.out.println(read*read);
	}

	}
}
