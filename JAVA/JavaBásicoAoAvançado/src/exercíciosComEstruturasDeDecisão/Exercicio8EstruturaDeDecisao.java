//programa que le duas notas, sendo que elas tem que estar entre 0 e 10, e mostra as medias.
package exercíciosComEstruturasDeDecisão;
import java.util.Scanner;

public class Exercicio8EstruturaDeDecisao {

	public static void main(String[] args) {
		System.out.println("Insira a primeira nota");
		Scanner num1 = new Scanner (System.in);
		double read1 = num1.nextDouble();
		System.out.println("Insira a segunda nota");
		Scanner num2 = new Scanner (System.in);
		double read2 = num2.nextDouble();
		double num3 = (read1 + read2)/2;
		if (read1 <= 10 && read1 >= 0 && read2 <=10 && read2 >= 0) {
			System.out.println("a media é " + num3 );
		}
		else {
			System.out.println("Valores não validos");
		}
			
		
	}

}
