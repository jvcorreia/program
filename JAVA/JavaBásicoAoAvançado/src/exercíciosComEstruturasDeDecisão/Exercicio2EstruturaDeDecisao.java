//Leia um n�mero fornecido. Se o n�mero for positivo, imprima a raiz quadrada, se for negativo diga que � invalido
package exerc�ciosComEstruturasDeDecis�o;
import java.util.Scanner;

public class Exercicio2EstruturaDeDecisao {

	public static void main(String[] args) {
		System.out.println("Digite o n�mero:");
		Scanner num = new Scanner (System.in);
		double read = num.nextDouble();
		
		if (read > 0) {
			System.out.println(Math.sqrt(read));
		}
		else if (read < 0) {
			System.out.println("O n�mero " + read + " � invalido.");
			
		}
		

	}

}
