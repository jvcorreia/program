package secao15_atributosemetodosestaticos;
//Atributos est�ticos

/*
 * Atributos est�ticos s�o atributos, onde os valores s�o compartilhados
 * entre as inst�ncias da classe.
 * 
 * Para utilizar um atributo est�tico, colocamos o nome da classe junto ao atributo
 * 
 */
public class Programa34 {
	public static void main(String[] args) {
		Conta c1 = new Conta( "joaoo");
		System.out.println(c1.getNumero());
		
		Conta c2 = new Conta("jurema");
		System.out.println(c2.getNumero());
	}

}
