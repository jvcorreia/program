package secao15_atributosemetodosestaticos;
//M�todos est�ticos

/*
 * Um m�todo est�tico n�o depende de uma inst�ncia da classse para ser utilizado
 * 
 * Pode-se utilizar conforme:
 */
public class Programa35 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Conta c1 = new Conta("jonny");
		System.out.println("a proxima conta ser� " + Conta.proximaConta());
	}

}
