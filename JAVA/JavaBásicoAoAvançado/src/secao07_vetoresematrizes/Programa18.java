package secao07_vetoresematrizes;
//Matrizes

/*
 * Vetores/arrays/matrizes multi-dimenssionais
 * 
 * 
 * Matriz
 * [linhas][colunas]
 * int numeros [3][3]
 * numeros [0][0] = 1;
 * numeros [0][1] = 3;
 * numeros [0][2] = 5;
 * numeros [1][0] = 7;
 * numeros [1][1] = 9;
 * numeros [1][2] = 11;
 * numeros [2][0] = 13;
 * numeros [2][1] = 15;
 * numeros [2][2] = 17;
 */

public class Programa18 {

	public static void main(String[] args) {
		//declaração
		int outros_numeros [][];
		
		//declaração e definição de tamanho
		int numeros [][] = new int [3][3];
		
		int mais_numeros [][] = {{1, 2, 3}, {4, 5, 6}, {7, 8, 9}};//matriz 3x3
		
		//declarar uma matriz informando somente as linhas
		int matriz[][] = new int [2][];
		matriz [0] = new int [5];
		matriz [1] = new int [3];
		
		int nova_matriz[][] = { {1,2}, {4, 5, 6, 7, 8}, {9, 10, 11} }; //3 x (2), (5), (3)
		

	}

}
