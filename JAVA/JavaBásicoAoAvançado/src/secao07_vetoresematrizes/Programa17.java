package secao07_vetoresematrizes;
//Vetores parte 2
public class Programa17 {
	public static void main(String[] args) {
		//declara��o e defini��o de tamanho do vetor
		int numeros [] = new int [10];
		
		for (int i = 0; i < numeros.length; i++) {
			//numeros [0] = i + 3;
			numeros [i] = i + 3;
			//numeros [9] = i + 3;
		}
		
		for (int i : numeros) {
			System.out.println(i);
		}
		System.out.println(numeros[0]);//primeiro elemento
		System.out.println(numeros[9]);//ultimo
		numeros [0] = 7;
		System.out.println(numeros[0]);
	}
//Os vetores possuem tamanho fixo e n�o podem ser aumentados ou diminuidos.
	
}
