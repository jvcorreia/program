package secao07_vetoresematrizes;
//Vetores parte 1

//Arrays (vetores uni-dimensionais)

/* float valores [10]; 0 at� 9
 * int numeros [5]
 * numeros [0] = 1; //primeiro elemento
 * numeros [1] = 3; //segundo
 * numeros [2] = 5; //terceiro
 * numeros [3] = 7; // quarto
 * numeros [4] = 9; //quinto
 */
public class Programa16 {

	public static void main(String[] args) {
		//declarando um vetor
		int vetor [];
		
		//Declarando e especificando o tamanho do vetor
		int numeros [] = new int [5];
		
		//declarando e inicializando
		int outros_numeros [] = {1, 3, 5, 7, 9};
		
		float valores [] = new float [5];
		char caracteres [] = new char [30];
		
		String frutas [] = {"joao", "victor"};
		

	}

}
