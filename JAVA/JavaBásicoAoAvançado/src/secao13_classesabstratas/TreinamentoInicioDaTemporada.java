package secao13_classesabstratas;

public class TreinamentoInicioDaTemporada extends Treinamento {

	@Override
	public void preparoFisico() {
		System.out.println("preparo f�sico de inicio da temporada...");
		
	}

	@Override
	public void jogoTreino() {
		System.out.println("Jogo treino de inicio de temporada");
		
	}

}
