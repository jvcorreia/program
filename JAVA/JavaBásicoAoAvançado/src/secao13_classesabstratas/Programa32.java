package secao13_classesabstratas;
//Template Method

/*
 * - O padr�o Template Method define o esqueleto de um algor�timo dentro de um m�todo,
 * transferindo alguns de seus passos para subclasses. O Template Method permite
 * que as subclasses redefinam certos passos de um algoritimo sem alterar a estrutura
 * do pr�prio algoritimo.
 * 
 * Algoritimos s�o "receitas" passo-a-passo para resolver algum problema.
 * 
 * receber numero;
 * retornar numero*numero;
 * 
 * m�todo_principal(){
 * 	passo();
 *  passo2();
 *  passo3();
 * }
 */
public class Programa32 {
	public static void main(String[] args) {
		TreinamentoInicioDaTemporada tit = new TreinamentoInicioDaTemporada();
		
		tit.treinoDiario();
		
		TreinamentoFimDaTemporada tft = new TreinamentoFimDaTemporada();
		
		tft.treinoDiario();
	}

}
