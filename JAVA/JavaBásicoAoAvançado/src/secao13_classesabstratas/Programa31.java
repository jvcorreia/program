package secao13_classesabstratas;
//Classes abstratas

import secao12_herancaepolimorfismo.Aluno;
import secao12_herancaepolimorfismo.Pessoa;

/*
 * -� um recurso que proporcional um bloqueio da cria��o de objetos
 * -Nesse contexto, podemos instanciar a classe pessoa, mas na pr�tica n�o deveria ser
 *  poss�vel, pois uma pessoa tem que ser aluno ou professor.
 *  -N�o conseguimos instaciar objetos de uma classe abstrata
 *  
 *  Uma classe abstrata pode ter:
 *  	- atributos;
 *  	- m�todos;
 *  	- M�TODOS ABSTRATOS;
 *  
 *  M�TODOS ABSTRATOS: 
 *  - n�o  possuem implementa��o (conte�do entre as chaves), apenas declara��o, e as classes que
 *  herdarem este m�todo dever�o implementar.
 */
public class Programa31 {

	public static void main(String[] args) {
	//-> repare que agora n�o � possivel:	Pessoa mariaa = new Pessoa ("mari da silva", 1999, "maria@maria.com");
	//	System.out.println(mariaa);
		
		Aluno mariaa = new Aluno("mariaa da saaa", 1999,3298329, "maria@maria");
		System.out.println(mariaa.getEmail());
		
		mariaa.outra_mensagem("ola");

	}

}
