package sistemafinanceiro;

public class Gasto {
	float valor;
	String descricao;
	
	public Gasto(float valor, String descricao){
		this.valor = valor;
		this.descricao = descricao;
	}
	
	public float GetGastoValor() {
		return this.valor;
	}
	
	public String GetGastoDescricao() {
		return this.descricao;
	}
	
	void SetGastoValor(float valor) {
		System.out.println("Digite o novo valor:");
		this.valor = valor;
		System.out.println("o novo valor � " + valor);
	}

	void SetGastoDescricao(String descricao) {
		System.out.println("Digite a nova descricao");
		this.descricao = descricao;
		System.out.println("A nova descricao � " +"'" + descricao + "'");
	}
	
	
	
}
