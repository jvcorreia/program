package secao14_interfaces;
//Interfaces - o que �, e quando utilizar

/*
- Interfaces s�o conhecidas como "contratos".

Uma empresa criou um contrato 'com regras' para definir a
cria��o de um produto/servi�o.

Quem implementar este contrato � OBRIGADO  a seguir as regras.

O jo�o decidiu implementar um produto/servi�o baseado nestre contrato.
A maria tamb�m decidiu implementar um produto/servi�o baseado no mesmo contrato.

Contrato para a confec��o de um bolo.
	- bolo precisa ser de chocolate
	- bolo deve ter cobertura
	- bolo deve ser recheado
	

 
 */
public class Programa33 {
	public static void main(String[] args) {
		Ventilador vent = new Ventilador();
		
		vent.ligar();//deve imprimir mensagem
	}

}
