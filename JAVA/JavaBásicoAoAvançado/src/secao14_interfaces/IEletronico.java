package secao14_interfaces;

/*
 Uma interface pode conter:
 -Constantes;
 -M�todos abstratos.
 
 Interface para servir de contrato para produtos eletr�nicos. 
 Todo produto eletr�nico que implementar esta interface OBRIGATORIAMENTE
 dever�o implementar os m�todos abstratos.
 */
public interface IEletronico {
	
	public String marca = "Jonny";
	
	public void ligar();
	
	public void desligar();

}
