package secao04_estruturasdedecisao;
// Instru��o Switch
public class Programa06 {
 public static void main(String[] args) {
	 int numero = 5;
	 
	 switch (numero) {
	case 1:
		System.out.println("o numero � 1 ");
		break;
	case 3:
		System.out.println("o numero � 3");
		break;
	case 5:
		System.out.println("o n�mero � 5");
		break;
	default:
		System.out.println("o n�mero � " + numero);
		break;
	}
	
}
}
