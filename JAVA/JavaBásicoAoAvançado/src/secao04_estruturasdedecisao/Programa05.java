package secao04_estruturasdedecisao;
//Operador tern�rio

//Se voc� precisar criar a fun��o main //Programa java
//Digite main e depois CTRL + barra de espa�o
public class Programa05 {
  
	public static void main(String[] args) {
		int valor = 4, numero;
		
		//Operador tern�rio
		// valor � maior que 0? se sim, n�mero recebe valor, caso contrario recebe 7.
		numero = (valor > 0) ? valor : 7;
		System.out.println(numero);
		
	}
	
}
