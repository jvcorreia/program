package secao04_estruturasdedecisao;
//If, else, else if
public class Programa04 {

	public static void main(String[] args) {
		//Declarando e inicializando a vari�vel.
		int numero = 9;
		
		if(numero > 5) {
			System.out.println("sim, o n�mero " + numero + " � maior que 5.");
		}
		else if(numero == 5){
			System.out.println("sim, o n�mero " + numero + " � igual a 5.");
		}
		//verificando se o n�mero � par
		else if (numero % 2 == 0) {
			System.out.println("sim, o n�mero " + numero + " � par.");
		}
		else {
			System.out.println("n�o, o n�mero " + numero + " n�o � maior que 5");
		}
		

	}

}
