package secao03_introducaoalinguagem;
//O compilador e o bytecode
public class Programa02 {

	public static void main(String[] args) {
     int idade = 28;
     System.out.println ("EU TENHO " + idade + " ANOS.");
	}

}

//O arquivo .class (o execut�vel no sistema) � chamado de bytecode, pois � ele que o sistema interpreta.

//Para criar o bytecode de determinado c�digo fonte: javac nomedoarquivo.java 

//O c�digo fonte � o arquivo .java

//Para executar:  java nomearquivo

