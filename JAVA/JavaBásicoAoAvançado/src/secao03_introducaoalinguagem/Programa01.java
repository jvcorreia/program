package secao03_introducaoalinguagem;
//Vari�veis
public class Programa01 {

	public static void main(String[] args) {
		//Declara��o de vari�vel.
		int valor;
		
		//Declara��o de vari�veis.
		int num1, num2, num3;
		
		//Declara��o e inicializando algumas vari�veis.
		int num4, num5 = 4, num6 = 7, num8;
		
		char caractere;
		float preco;
		
		//Declarando.
		int num9;
		
		//Declarando e inicializando. Neste caso ele j� aloca na mem�ria com o valor declarado.
		int num10 = 99;
		
		//int 10eu = 7; //Errado, n�o � poss�vel iniciar nome de vari�vel com n�mero.
		
		float _valor = 34.2f; //Certo, � poss�vel iniciar nome de vari�vel com o _ //Por padr�o, os valores de ponto flutuante s�o double no Java
		
		double _valor2 = 34.2;

	}

}
