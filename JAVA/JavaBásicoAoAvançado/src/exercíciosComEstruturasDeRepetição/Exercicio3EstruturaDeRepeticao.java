//usando while, fazer programa que conta de 10 até 0 e depois mostra "fim"
package exercíciosComEstruturasDeRepetição;

public class Exercicio3EstruturaDeRepeticao {
	public static void main(String[] args) {
		int tempo = 10;
		while (tempo > -1) {
			System.out.println("faltam " + tempo + " segundos");
			tempo = tempo - 1;
			
		}
		System.out.println("fim");
		
	}

}
