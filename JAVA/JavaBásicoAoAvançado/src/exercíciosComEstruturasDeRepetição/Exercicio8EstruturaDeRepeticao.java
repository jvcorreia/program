//leia 10 numeros e escreva o maior e o menor
package exercíciosComEstruturasDeRepetição;
import java.util.Scanner;

public class Exercicio8EstruturaDeRepeticao {
	public static void main(String[] args) {
		Scanner read = new Scanner (System.in);
		int a = 0, menor = 0, maior = 0;
		for (int i = 0; i < 10; i++) {
			System.out.println("digite um numero");
			a = read.nextInt();
			
			if (i == 0) {
				maior = a;
				menor = a;
				
			}
			
			if (a > maior) {
				maior = a;
			}
			if ( a < menor) {
				menor = a;
			}
			
		}
		System.out.println("o maior é " + maior + " e o menor é " + menor);
		
		
	}
	

}
