//mostre os 5 primeiros multiplos de 3
package exercíciosComEstruturasDeRepetição;

public class Exercicio1EstruturaDeRepeticao {

	public static void main(String[] args) {	
		
		for ( int i = 1; i < 16; i++)	{  
			  if (i % 3 == 0) {
				  System.out.println(i);
			  }
			  
		}
				 

	}

}
