//leia um numero n, e depois imprima os n primeiros numeros impares
package exercíciosComEstruturasDeRepetição;
import java.util.Scanner;

public class Exercicio9EstruturaDeRepeticao {

	public static void main(String[] args) {
		Scanner sc = new Scanner (System.in);
		System.out.println("digite o numero");
		 int x = sc.nextInt();
		
		for (int i = 1; i < x*2; i++) {
			if (i%2 == 1) {
				System.out.println(i);
			}
			
		}
		

	}

}
