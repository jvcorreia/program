//contar de 1000 em 1000 até 100.000
package exercíciosComEstruturasDeRepetição;

public class Exercicio4EstruturaDeRepeticao {
	public static void main(String[] args) {
		int num = 0;
		for ( num = 0; num < 100001; num = num + 1000) {
			System.out.println(num);
		}
	}

}
