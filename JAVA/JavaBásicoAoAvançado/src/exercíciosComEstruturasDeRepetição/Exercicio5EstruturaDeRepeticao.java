//Peça para o usuario digitar 10 valores e some-os
package exercíciosComEstruturasDeRepetição;
import java.util.Scanner;

public class Exercicio5EstruturaDeRepeticao {
	public static void main(String[] args) {
		  Scanner leia = new Scanner(System.in);
	        int valor=0;
	        int total=0;
	        int vez = 0;
	           do{
	            System.out.print("Digite um numero a ser somado  ");
	            valor = leia.nextInt();
	            total += valor;
	            vez++;
	           }while(vez<10);
	        System.out.print("Soma total: " + total);
	    }

}
