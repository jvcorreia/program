package secao19_collections;
//Mapas /Map

import java.util.HashMap;
import java.util.Map;

/*
 * Os mapas s�o representados em Java pela interface Map
 * e mapeia seus elementos utilizando o conceito de chave/valor
 */
public class Programa50 {

	public static void main(String[] args) {
		Map<Integer, String> pessoas = new HashMap<Integer,String>();
		
		pessoas.put(15,"joao");//Adicionando
		pessoas.put(20, "mais velhor");
		
		
		System.out.println(pessoas.keySet());//Cole��o de chave
		
		System.out.println(pessoas.values());//Cole��o de valores
		
		System.out.println(pessoas.entrySet());//Cole��o de associa��o
		
		pessoas.keySet().forEach(idade ->{
			System.out.println(pessoas.get(idade));
		});
			
		
	}

}
