package secao19_collections;
//HashMap

//N�o aceita chaves duplicadas
import java.util.HashMap;
import java.util.Map;

import secao11_modificadoresdeacesso.Cliente;
import secao11_modificadoresdeacesso.Conta;

/*
 * A classe HashMap implementa a interface Map e trabalha com chave/valor
 * 
 */
public class HashMaps {
		public static void main(String[] args) {
			Map<String, Conta> nomes = new HashMap<String, Conta>();//Chave String e valor � Conta
			
			Cliente cli1 = new Cliente("joao","nucleio bandeirate");
			Cliente cli2 = new Cliente("luiz","casa 778787879");
			
			Conta c1 = new Conta(1, 400, 600, cli1);
			Conta c2 = new Conta(2, 300, 500, cli2);
			
			nomes.put("empresa publica", c1);//Adicionando
			nomes.put("emrpesa privada", c2);
			
			System.out.println(nomes.get("empresa publica"));
			
			
			
			
		}

}
