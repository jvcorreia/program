package secao19_collections;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

//Listas

/*Arrays (Vetores/Matrizes)
 * 
 * -Um array tem tamanho fixo. Ou seja, se criarmos um array com 5 elementos
 * ele ter� no m�ximo 5
 * 
 * -Um array tem tipo de dado fixo.
 * 
 * -� dificil encontrar um determinado elemento em um array. Precisamos para isso
 * percorrer todo o array atrav�s dos seus �ndices at� encontrar.
 * 
 * 
 * Colections (cole��es)
 * 
 * -Java possui diversas classes/interfaces que facilitam muito o trabalho quando
 * se trata de cole��es de dados. Essas classes/interfaces s�o chamadas de Collections (cole��es)
 * 
 * 
 * Listas
 * 
 * - Aceitam repeti��o de valores
 * - Possuem tamanho infinito (depende da mem�ria)
 * - Sem tipo de dado definido, ou seja, qualquer tipo de valor pode ser adicionado (Desde que n�o especificado na declara��o)
 * 
 */
public class Listas {

	public static void main(String[] args) {
		List nomes = new ArrayList();
		ArrayList<String> strings = new ArrayList<String>();//Definindo tipo de dado
		nomes.add("joao");
		nomes.add("lucas");
		nomes.add("joao");
		System.out.println(nomes.get(0));//Printar determinado elemento
		System.out.println(nomes);//Printar a lista toda
		System.out.println(nomes.size());//tamanho da lista
		nomes.add(44);
		nomes.add(true);
		Collections.sort(nomes);//ordenar

	}

}
