package secao19_collections;

import java.util.HashSet;
import java.util.Set;

//Conjuntos

/*
 * Os conjuntos s�o implementados com a interface Set e uma das
 * classes que implementam esta interface � a HashSet
 * 
 * A maioria das cole��es possuem os mesmos m�todos, mas os comportamentos s�o um pouco diferentes
 * 
 * Caracter�sticas dos conjuntos: N�o aceitam valores repetidos
 * -A ordem de inser��o n�o � respeitada
 * - N�o aceita ordena��o
 * -N�o possui indice
 */
public class Conjuntos {
	public static void main(String[] args) {
		Set<String> nomes = new HashSet<String>();
		nomes.add("maria");
		nomes.add("joao");
		nomes.add("luqquita");
		nomes.add("joao"); //Conjuntos n�o aceitam repeti��o de valores
		
		System.out.println(nomes.contains("joao"));//vendo se um elemento est� no conjunto //Booleano
		
		System.out.println(nomes.add("joao"));//Se conseguir adicionar retorna true, sen�o retorna false
		
		System.out.println(nomes);
	}

}
