package secao17_executaveisedocumentacao;

import java.util.Scanner;
import secao11_modificadoresdeacesso.Cliente;
import secao11_modificadoresdeacesso.Conta;

/*
 * Gerando execut�veis jar
 * 
 * JAR- Java Archive
 * 
 * Arichive- arquivo compactado Java
 * 
 * zip/rar
 */
public class Principal {
	
	static Cliente cliente = new Cliente ("jonny","2 avenidade");
	static Conta conta = new Conta(100, 200f, 400f, cliente);
	static Scanner teclado = new Scanner (System.in);
	/**
	 * M�todo para depositar  //<------ documenta��o
	 * 
	 * para gerar: v� em project -> generate java documentation
	 * 
	 */
	public static void depositar() {
		System.out.println("----deposito----");
		System.out.println("informe o valor para deposito:");
		float valor = teclado.nextFloat();
		if (valor > 0) {
			conta.depositar(valor);
			System.out.println("deposito efetuado");
		
		}
		else {
			System.out.println("valor tem que ser maior que 0");
		}
	}
	
	public static void sacar() {
		System.out.println("-------saque----");
		System.out.println("informe a quantidade para saque:");
		float valor = teclado.nextFloat();
		if (valor>0) {
			conta.sacar(valor);
			
		}
		else {
			System.out.println("valor tem que ser maior que 0");
		}
	}
	
	public static void saldo() {
		System.out.println("-------saldo------");
		System.out.println(conta.getSaldo());
	}

	public static void main(String[] args) {
		int opcao = 0;
		System.out.println("bem vindo ao banco jonny");
		do {
			System.out.println("selecione uma op��o abaixo");
			System.out.println("1- saque");
			System.out.println("2- deposito");
			System.out.println("3- saldo");
			System.out.println("0- sair");
			opcao = teclado.nextInt();
			
			switch (opcao) {
			case 1:sacar();
				
				break;
				
			case 2: depositar();
				
			break;
			
			case 3: saldo();
			break;
			
			case 0: teclado.close();
			break;
			default:
				break;
			}
			
		} while (opcao>0);
		

	}

}
