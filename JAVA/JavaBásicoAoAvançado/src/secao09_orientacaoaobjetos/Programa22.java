package secao09_orientacaoaobjetos;
//Objetos

/*
 * Objetos s�o produtos/inst�ncias da classe;
 */
public class Programa22 {

	public static void main(String[] args) {
		int numero = 4;
		
		//Declara��o de um objeto
		Produto p0;
		
		//Declara��o e instancia��o/inicia��o do objeto
		Produto p1 = new Produto();//Construtor

		System.out.println(p1);
		p1.nome = "Notebook";
		p1.preco = 2.340f;
		p1.desconto = 15.0f;
		
		System.out.println(p1.nome);
		System.out.println(p1.preco);
		System.out.println(p1.desconto);
		
		Pessoa pessoa1 = new Pessoa ();//Construtor
		pessoa1.nome = "joao";
		pessoa1.emai = "joao@joao.com";
		
		Pessoa pessoa2 = new Pessoa ("angelina", "joao@2.com", 2008);
		
		System.out.println(pessoa2.emai);
		
	}

}
