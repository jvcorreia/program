package secao09_orientacaoaobjetos;
//Classes

//Atributos

//M�todos

//Construtores

/*
 * Sempre, um construtor vazio tem a seguinte forma:
 * 
 * public NomeDaClasse(){
 * }
 */
public class Pessoa {
	String nome;
	int ano_de_nascimento;
	String emai;
	
	//Construtor vazio
	public Pessoa() {
		
	}
	
	//Construtor com par�metros
	public Pessoa (String nome, String email, int ano_de_nascimento) {
		// this == este objeto
		this.nome = nome;
		this.emai = email;
		this.ano_de_nascimento = ano_de_nascimento;
		
	}
	void imprime_info() {
		System.out.println(this.nome + this.emai);
		
	}
}
