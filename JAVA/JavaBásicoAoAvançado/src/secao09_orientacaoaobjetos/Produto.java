package secao09_orientacaoaobjetos;
//Classes

/*
 * O nome das classes inicia com letra mai�scula;
 * O nome n�o deve conter: acentua��o, caracteres especiais, espa�o
 * Nas classes Java, n�o existe a implementa��o da fun��o main()
 */

//Atributos
/*
 * S�o as caracter�sticas da classe/molde/modelo de dados;
 * Podemos entender attributos como vari�veis da classe;
 * Uma outra forma de nomenclatura para os atributos s�o estados;
 * Atributos s�o nomeados em letras min�sculas, sem espa�os, sem caracteres especiais, 
 * acentua��o.
 */

//M�todos
/*
 * Podemos entender os m�todos como a a��o que � realizada por um objeto da classe;
 * Podemos entender tamb�m que os m�todos s�o comportamentos dos objetos da classe;
 * Mesmos requisitos para fun�oes
 */
public class Produto {
	String nome;
	float preco, desconto;
	
	//M�todo para aumentar o pre�o em 10
	void aumentar_preco() {
		this.preco = this.preco + 10;
	}
}
