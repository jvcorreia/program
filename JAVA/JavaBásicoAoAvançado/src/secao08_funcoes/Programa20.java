package secao08_funcoes;
//Criando as pr�prias fun��es

/*
 * Fa�a um programa que receba diversas frutas do usu�rio e no final imprima essas frutas
 * em ordem contr�ria. O programa deve solicitar ao usu�rio quantas frutas ele quer informar.
 * 
 * informe quantas frutas deseja cadastrar:
 * 2
 * 
 * informe o nome de uma fruta:
 * manga
 * 
 * informe o nome de uma fruta:
 * goiaba
 * 
 * goiaba
 * manga
 */
 
import java.util.Scanner;
public class Programa20 {
	//vari�veis globais
	static String frutas [];
	
	static Scanner teclado = new Scanner (System.in);
	
public static void main(String[] args) {
	//vari�vel local
	int qtd;	
	System.out.println("informe a quantidade de frutas que deseja cadastrar:");
	qtd = Integer.parseInt(teclado.nextLine());
	
	
	cadastrar_dados(qtd);
	
	mostrar_dados(qtd);
	
	teclado.close();
}
//uma fun��o deve ter o seguinte:
// a) tipo de retorno (tipo de dado que a fun��o vai retornar);
//b)) nome - corresponde a a��o que a fun��o realiza;
//c) par�metros/argumentos de entrada;
//d) retorno (opcional - depende do tipo de retorno);
//void = vazio
 static void cadastrar_dados(int quantidade) {
	frutas = new String [quantidade];
	for (int i = 0; i < quantidade; i++) {
		System.out.println("informe a " + (i + 1) + " fruta");
		 frutas[i] = teclado.nextLine();
	}
}

static void mostrar_dados (int quantidade) {
	for (int i = (quantidade - 1); i>=0 ; i--) {
		System.out.println(frutas[i]);
		
	}
}

}
