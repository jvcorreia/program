package secao11_modificadoresdeacesso;

public class Programa25 {

	public static void main(String[] args) {
		
		// Criando clientes com a classe feita anterior mente
		Cliente joao = new Cliente("Jo�o da silva", "rua da paz, 45");
		Cliente maria = new Cliente("Maria eduarda", "rua da paz, 43");
		
		// Criando contas com a classe feita anteriormente, e utilizando a classe cliente juntamente
		Conta conta_joao = new Conta(1, 100.0f, 200.0f,joao);
		Conta conta_maria = new Conta(1, 200.0f, 400.0f,maria);
		
		System.out.println("saldo do joao " + conta_joao.getSaldo());
		System.out.println("saldo da maria " + conta_maria.getSaldo());
		
		
		int x = 200;
		conta_joao.sacar(200);
		System.out.println("saldo do joao depois do saque de " + x  + " = " + conta_joao.getSaldo());
		
		joao.dizer_oi();
		
		//conta_joao.limite = 1000000; Seria poss�vel se o limite fosse public, mas por motivos de �bvios ele n�o deve ser acessado pelo cliete
		// para altera��es diretas
	}

}
