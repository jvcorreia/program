package secao11_modificadoresdeacesso;

public class ModificadoresDeAcesso {
// Public: � um modificador de acesso, que torna p�blico o objeto, pode ser utlizado em todo o projeto.
// As classes s�o sempre public
// Construtor � obrigado a ser public tamb�m, pois eles s�o utilizados nos programas
// Os atributos podem ser dos mais variados tipos
	
// Private: privado � propria classe
// Ou seja, s� temos acesso ao atributo ou m�todo dentro da pr�pria classe onde ele foi declarado
	
// Protected (modificador de acesso default): se voc� n�o declarar um atributo, ele vai receber o modificador de acesso protected
// Esse modificador de acesso faz com que o atributo seja vizualizado apenas dentro do pacote onde foi declarado
}
