package secao11_modificadoresdeacesso;
//Encapsulamento
//Capsula

/*
 * Getters e Setters
 * Getter - m�todo publico que serve para consultar dados;
 *        - a nomenclatura desses m�todos � get_nome_do_atributo()
 */

public class Conta {
	int numero;
	private float saldo; //Private, pois o cliente n�o pode fazer altera��es diretamente
	private float limite;
	Cliente cliente;
	
	public Conta (int numero, float saldo, float limite, Cliente cliente) {
		this.numero = numero;
		this.saldo = saldo + limite;
		this.limite = limite;
		this.cliente = cliente;
	}

	//100
	//50
	
	//this.saldo = this.saldo - valor
	
	public void sacar (float valor) {
		if (valor <= this.saldo) {
		this.saldo = this.saldo - valor;
		System.out.println("saque realizado com sucesso");
		}
		else {
			System.out.println("saldo insuficiente");
		}
	}
	
	public void depositar (float valor) {
		synchronized (this) {
			this.saldo = this.saldo + this.limite + valor;
		}
		
		// Forma 2:
		
		//public synchronized void depositar (float valor) {
		//this.saldo= this.saldo + this.limite + valor;
		
	//}
	}
	
	public float getSaldo() {
		return this.saldo;
	}
	
}
