package secao18_javalang;
//Realizando c�lculos com a biblioteca Math

/*
 *A biblioteca Math possui v�rios m�todos e constantes est�ticas
 *para que possamos utilizar nos nossos programas.
 * 
 */
public class BibliotecaMath {

	public static void main(String[] args) {
		System.out.println(Math.E);//Constante
		
		System.out.println(Math.PI);
		
		System.out.println(Math.sin(45));//Seno
		
		System.out.println(Math.cos(45));//Cosseno
		
		System.out.println(Math.tan(45));//Tangente
		
		System.out.println(Math.round(Math.PI));//M�todo para arrendondar
		
		System.out.println(Math.ceil(Math.PI));//Arredonda para cima
		
		System.out.println(Math.floor(3.99));//Arredonda para baixo
		
		System.out.println(Math.pow(3, 3));//Pot�ncia, 3 elevado a 3
		
		System.out.println(Math.sqrt(9));//Raiz quadrada

	}

}
