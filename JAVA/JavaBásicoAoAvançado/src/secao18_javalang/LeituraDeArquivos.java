package secao18_javalang;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Scanner;

//Lendo em arquivos
public class LeituraDeArquivos {

	public static void main(String[] args) {
		try {
			Scanner ler = new Scanner (new FileInputStream("saida.txt"));
			while (ler.hasNextLine()) { //Esse While serve para ler todas as linhas
				String Linha = ler.nextLine();
				System.out.println(Linha);
					
			}
			ler.close();
		} catch (FileNotFoundException e) {
			System.out.println("arquivo n�o encontrado");
		};										//Colocando o Scanner para ler o arquivo, repare que dentro do par�metro
											  // new Scanner n�o se coloca System.in, pois ele n�o ir� ler o input do teclado

	}

}
