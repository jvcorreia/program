package secao18_javalang;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.Scanner;

//Escrevendo em Arquivos
public class EscritaDeArquivos {
		public static void main(String[] args) {
			
			Scanner Teclado = new Scanner (System.in);
			
			
			try {//A escrita de arquivo deve ser colocada no try/catch, pois ele pode gerar um erro do tipo checked
			PrintStream escrever = new PrintStream("saida.txt"); //Escrita de arquivo, essa vari�vel serve para //armazenar o que ser� escrito
			//PrintStream escrever = new PrintStream(new FileOutputStream("saida.txt", true)); para n�o sobrescrever o conte�do e sim adicionar
			//^^--> se j� existir, ele adiciona conte�do, se n�o ele cria.
			System.out.println("Escreva algo");
			String mensagem = Teclado.nextLine();
			escrever.println(mensagem);//Grava dentro do arquivo
			escrever.close();
			}													
			catch(FileNotFoundException e){
				System.out.println("N�o foi poss�vel criar o arquivo");
			}
		
			
		
			Teclado.close();
			
			
	
			}
}
