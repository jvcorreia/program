package secao18_javalang;
//Trabalhando com Strings


/*
 * Em Java, Strings s�ao imut�veis, ou seja, n�o mudam.
 */
public class TrabalhandoComStrings {

	public static void main(String[] args) {
		String curso = "ci�ncia da computa��o";
		System.out.println(curso);
		
		curso.replace("da", "de");
		System.out.println(curso);//return = ci�ncia da computa��o, pois string � imut�vel
		
		String nova = curso.replace("da", "de");
		System.out.println(nova);//Return = ci�ncia de computa��o
		
		//A N�O SER QUE
		
		curso = curso.replace("da", "de");
		
		System.out.println(curso);//Return = ci�ncia de computa��o
		
		curso = curso.toUpperCase();//Deixando todas as letras mai�sculas
		System.out.println(curso);
		
		System.out.println(curso.charAt(2));//Imprimir o caractere na posi��o 2
		

	}

}
