package secao18_javalang;

import java.io.ObjectInputStream.GetField;

import secao11_modificadoresdeacesso.Cliente;
import secao11_modificadoresdeacesso.Conta;

//Object: a m�e de todas as classes
public class Programa39 {

	public static void main(String[] args) {
		Cliente cli1 = new Cliente ("jonny", "top");
		Cliente cli2 = new Cliente("maria", "2 avenida maria");
		
		Conta c1 = new Conta(1000, 200f, 200f,cli1);
		Conta c2 = new Conta(200, 300f, 200f, cli2);
		
		System.out.println(c1.toString());// -> N�O EST� NA CLASSE CONTA, EST� NO OBJECT
										  // -> M�todo pode ser sobrescrito para de usado de 
										  // forma diferente em cada classe.

		Caixa prateleira = new Caixa();
		
		prateleira.adicionar(c1);
		prateleira.adicionar(c2);
		
		
		
		System.out.println(prateleira.pegar(0).getSaldo());
	}

}
