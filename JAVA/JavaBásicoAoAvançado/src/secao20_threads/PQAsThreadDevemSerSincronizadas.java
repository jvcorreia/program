package secao20_threads;

import secao11_modificadoresdeacesso.Cliente;
import secao11_modificadoresdeacesso.Conta;

/*
 * Por padr�o, as threads n�o s�o sincronizadas, pode ocorrer problemas de uma thread acessar um valor de um objeto que ainda n�o foi
 * atualizado ou ainda as thread executarem depois do valor ter sido impresso
 */
public class PQAsThreadDevemSerSincronizadas {

	public static void main(String[] args) {
		Cliente cli1 = new Cliente( "joao", "favela");
		Conta c1 = new Conta(1, 300, 500, cli1);//saldo 800
		System.out.println(c1.getSaldo());
		
		FazDeposito faz = new FazDeposito(c1);
		Thread t1 = new Thread(faz);
		Thread t2 = new Thread(faz);
		
		t1.start();
		t2.start();
		
		System.out.println(c1.getSaldo());

	}

}
