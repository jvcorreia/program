package secao20_threads;

import secao11_modificadoresdeacesso.Cliente;
import secao11_modificadoresdeacesso.Conta;

public class ThreadSincronizada {
	/*
	 * O sincronismo ocorre pois durante a execu��o do m�todo a thread executa um 'lock'(bloqueio) da fun��o para que 
	 * outra thread s� possa execut�-la p�s a finaliza��o da thread inicial
	 */

	public static void main(String[] args) throws InterruptedException {
		Cliente cli1 = new Cliente( "joao", "favela");
		Conta c1 = new Conta(1, 300, 500, cli1);//saldo 800
		System.out.println(c1.getSaldo());
		
		FazDeposito faz = new FazDeposito(c1);
		Thread t1 = new Thread(faz);
		Thread t2 = new Thread(faz);
		
		t1.start();
		t2.start();
		
		t1.join(); //Avisando que a thread t1 deve se juntar a um sincronizador
		
		t2.join(); //Avisando que a thread t1 deve se juntar a um sincronizador
		
		//Olha a Classe conta na se��o 11
		
		System.out.println(c1.getSaldo());

	}

}
