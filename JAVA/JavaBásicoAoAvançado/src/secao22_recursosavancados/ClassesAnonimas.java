package secao22_recursosavancados;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/*
 * Classes an�nimas
 */
public class ClassesAnonimas {
	public static void main(String[] args) {
List<String> palavras = new ArrayList<String>();
		
//QUANDO VAMOS UTILIZAR APENAS ALGUM M�TODO UMA VEZ NO PROGRAMA, N�O PRECISAMOS CRIAR UMA
// CLASSE NOVA S� PARA INSTANCIA-LA, PARA ISSO SERVE A CLASSE ANONIMA
//OBSERVE A DIFEREN�A ENTRE O TRECHO ABAIXO E A CLASSE "COMPARADORPORTAMANHO"
		Comparator<String> comparador = new Comparator<String>(){

			@Override
			public int compare(String o1, String o2) {
				// TODO Auto-generated method stub
				return 0;
			}
			
		};
		palavras.add("correia");
		palavras.add("joao");
		palavras.add("victor");
		palavras.sort(comparador);
	}
}
