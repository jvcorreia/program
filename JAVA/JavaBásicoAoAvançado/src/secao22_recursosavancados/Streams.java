package secao22_recursosavancados;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
/*
 * Streams s�o fluxos de dados/objetos utilizados para que possamos
 * trabalhar com esses dados de forma mais criteriosa.
 * 
 * Quando trabalhamos com stream, os m�todos aplicados n�o afetam a cole��o original
 */

public class Streams {

	public static void main(String[] args) {
		List<Curso> cursos = new ArrayList<Curso>();
		cursos.add(new Curso("java", 500));
		cursos.add(new Curso("database", 700));
		cursos.add(new Curso("python", 640));
		cursos.add(new Curso("ruby", 990));
		cursos.add(new Curso("react", 780));
		
		//Imprimir os cursos com mais de 600 alunos
		//cursos.stream().filter(//Condi��o);

	}

}
