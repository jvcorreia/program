package secao22_recursosavancados;

import java.util.Comparator;

//Estamos criando nosso pr�prio comparador de strings para que seja possivel, ordenar a string
//pelo seu tamanho
/*
 * Possibilidades:
 * 1- A string1 � menor que a string2 -> retornamos -1
 * 2- A string1 � maior que a string2 -> retornamos 1
 * 3- elas s�o iguais em tamanho, retornamos 0
 */
public class ComparadorPorTamanho implements Comparator<String>{

	@Override
	public int compare(String arg0, String arg1) {
		if(arg0.length()>arg1.length()) {
			return 1;
		}
		else if(arg0.length() < arg1.length()) {
			return -1;
		}
		else {
			return 0;
		}
	}

}
