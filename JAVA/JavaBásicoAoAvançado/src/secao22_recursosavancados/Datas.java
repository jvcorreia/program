package secao22_recursosavancados;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Period;

public class Datas {

	public static void main(String[] args) {
		LocalDate hoje = LocalDate.now();
		System.out.println(hoje);//Formato internacional ano mes dia
		
		LocalDate anonovo = LocalDate.of(2020, 1, 1);
		System.out.println(anonovo);
		
		int ano = anonovo.getYear();
		
		Period periodo = Period.between(hoje, anonovo);
		System.out.println(periodo);
		
		LocalDateTime now = LocalDateTime.now();
	}

}
