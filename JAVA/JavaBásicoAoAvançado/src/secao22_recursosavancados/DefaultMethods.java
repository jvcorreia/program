package secao22_recursosavancados;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

//Default Methods

/*
 *S�o m�todos concretos implementados em interfaces. Estes m�todos, como s�o concretos
 *ou seja, j� possuem implementa��o, n�o precisam ser implementados nas classes que implementarem
 *essa interface. 
 */
public class DefaultMethods {

	public static void main(String[] args) {
		List<String> palavras = new ArrayList<String>();
		
		Comparator<String> comparador = new ComparadorPorTamanho();
		palavras.add("correia");
		palavras.add("joao");
		palavras.add("victor");
		
		
		System.out.println(palavras);
		Collections.sort(palavras);//Ordena de forma alfab�tica
		
		for (int i = 0; i < palavras.size(); i++) {
			System.out.println("o tamanho da palavra " + palavras.get(i) + " � " + palavras.get(i).length());
				
		}
		//E se em vez de ordenar por ordem alfab�tica, eu quiser ordenar por tamanhao?
		palavras.sort(comparador);
		System.out.println(palavras);

	}

}
