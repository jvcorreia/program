package secao22_recursosavancados;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class Lambdas {

	public static void main(String[] args) {
List<String> palavras = new ArrayList<String>();
		
		Comparator<String> comparador = new ComparadorPorTamanho();
		palavras.add("correia");
		palavras.add("joao");
		palavras.add("victor");
		
		palavras.sort(comparador);
		
		
		//CONCEITO DE LAMBDA
		//SERVE PARA SIMPLIFICAR AINDA MAIS AS CLASSES ANONIMAS
		//SEMPRE REPRESENTADO PELA SETA
		
		palavras.sort((String arg0, String arg1) -> {
			if(arg0.length()>arg1.length()) {
				return 1;
			}
			else if(arg0.length() < arg1.length()) {
				return -1;
			}
			else {
				return 0;
			}
		});
		
		/*FORMA 2
		palavras.sort((String arg0, String arg1) -> {
			Integer.compare(arg0, arg1);
		});
		*/
		
		
		
		

	}

}
