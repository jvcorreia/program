//Leia um n�mero e imprima a quinta parte desse n�mero.
package exerc�ciosComVari�vel;
import java.util.Scanner;

public class Exercicio5Variavel {

	public static void main(String[] args) {
		System.out.println("Insira o n�mero para ser dado a sua quinta parte:");
		Scanner num1 = new Scanner(System.in);//Criar um objeto de scanner
		double read1 = num1.nextDouble();//Ler o input do usu�rio, convertendo o valor para n�mero double
		System.out.println("A quinta parte de " + read1 + " � " + (read1/5) );

	}

}
