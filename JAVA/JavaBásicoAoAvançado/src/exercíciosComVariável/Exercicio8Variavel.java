//Leia uma temperatura em graus Kelvin e apresente-a convertida em graus Celsius.
package exerc�ciosComVari�vel;
import java.util.Scanner;

public class Exercicio8Variavel {

	public static void main(String[] args) {
		System.out.println("Coloque a temperatura em graus Kelvin para converte-la em graus Celsius");
		Scanner num1 = new Scanner (System.in);
		double read1 = num1.nextDouble();
		double result = read1 - 273.15;
		System.out.println(read1 + " em Kelvin � igual a " + result + "em graus Celsius");

	}

}
