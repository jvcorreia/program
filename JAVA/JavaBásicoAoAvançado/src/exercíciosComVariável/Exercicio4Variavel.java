//Leia um n�mero real e imprima o quadrado desse n�mero.
package exerc�ciosComVari�vel;
import java.util.Scanner;

public class Exercicio4Variavel {

	public static void main(String[] args) {
		System.out.println("Digite o n�mero para ser calculado o seu quadrado:");
		Scanner num1 = new Scanner(System.in);//Criar um objeto de scanner
		double read1 = num1.nextDouble();//Ler o input do usu�rio, convertendo o valor para n�mero double.
		double quadradoread1 = Math.pow(read1, 2);
		System.out.println("O resultado �: "+ quadradoread1);
	}

}
