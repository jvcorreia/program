//Pe�a ao usu�rio que insira 3 n�meros e mostre a soma deles.
package exerc�ciosComVari�vel;
import java.util.Scanner;

public class Exercicio3Variavel {

	public static void main(String[] args) {
		System.out.println("Digite 3 n�meros para serem somados:");
		Scanner num1 = new Scanner(System.in);//Criar um objeto de scanner
		Scanner num2 = new Scanner(System.in);
		Scanner num3 = new Scanner(System.in);
		System.out.println("Digite o primeiro n�mero:");
		int read1 = num1.nextInt();//Ler o input do usu�rio, convertendo o valor para n�mero inteiro.
		System.out.println("Digite o segundo n�mero:");
		int read2 = num2.nextInt();
		System.out.println("Digite o terceiro n�mero:");
		int read3 = num3.nextInt();
		int result = (read1 + read2 + read3);
		System.out.println("A soma dos n�meros �: "+ result);
		
		
	}

}
