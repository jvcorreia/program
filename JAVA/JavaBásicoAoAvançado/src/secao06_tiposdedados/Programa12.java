package secao06_tiposdedados;
//Tipos de daos
//num�ricos (Inteiros e reais)
public class Programa12 {

	public static void main(String[] args) {
		//Tipos prim�rios/primitivos
		//Esses s�o mais r�pidos
		//Por padr�o, os n�meros reais em Java s�o considerados double
		float preco = 23.4f; //23.40
		double preco2 = 23.4; //23.40329328931839335353
		
		///Tipos n�o prim�rios/ n�o primitivos
		//Gerealmente s�o usados para convers�es
		Float preco3 = 44.5f;
		Double preco4 = 44.5;
		
		System.out.println("float/Float " + Float.SIZE);//32 bits			
		System.out.println("double/Double" + Double.SIZE);//64 bits	
		
		System.out.println("float/Float " + Float.MIN_VALUE); //1.4E-45
		System.out.println("float/Float " + Float.MAX_VALUE);//3.4028235E38
		
		System.out.println("double/Double " + Double.MIN_VALUE); //4.9E-324
		System.out.println("double/Double " + Double.MAX_VALUE);//1.7976931348623157E308

	}

}
