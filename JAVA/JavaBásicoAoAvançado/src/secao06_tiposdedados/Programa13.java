package secao06_tiposdedados;
//Tipos de dados alfanum�ricos
//caracteres e strings
public class Programa13 {

	public static void main(String[] args) {
		//Tipos primitivos	
		char letra = 'a'; //aspas simples
		char letra2 = 97;//97 em decimal � o mesmo que a
		System.out.println(letra2);
		
		
		letra2 = (char) (letra2 + 1); // (char) � chamdo de cast
		
		System.out.println(letra);
		System.out.println(letra2);
		
		//Tipos n�o primitivos
		Character letra3= 'A';
		String nome = "joao"; //A string n�o existe no tipo primitivo, somente no tipo n�o primitivo //
		
		System.out.println(letra3);
		System.out.println( "String " + (Character.SIZE * nome.length()) + " bits"); // quantos bits a string est� ocupando
		
			
	}

}
