package secao06_tiposdedados;
//Tipos de daos
//num�ricos (Inteiros e reais)
public class Programa11 {

	public static void main(String[] args) {
			//Tipos prim�rios/primitivos
		long num0 = 99;
		int num = 4; //Inteiro 9999999999999999
		short num2 = 4; //Inteiro (curto/menor/baixo) 999999
		byte num5 = 4;  //Inteiro
		char num8 = 34; //Tabela ascii
		
			//Tipos n�o primarios/n�o primitivos
		Long num7 = (long) 77777777;
		Integer num3 = 98;
		Short num4 = 7;
		Byte num6 = 9;
		Character num9 = 34; //Tabela ascii
		
		System.out.println("long/Long " + Long.SIZE);//64 bits			
		System.out.println("int/Integer " + Integer.SIZE);//32 bits	
		System.out.println("char/Character " + Character.SIZE);//16 bits
		System.out.println("short/Short " + Short.SIZE);//16 bits
		System.out.println("byte/Byte " + Byte.SIZE);//8 bits
		
		System.out.println("valor min long/Long " + Long.MIN_VALUE); //-9223372036854775808
		System.out.println("valor max long/Long " + Long.MAX_VALUE);//9223372036854775807
		
		System.out.println("valor min int/Integer " + Integer.MIN_VALUE); //-2147483648
		System.out.println("valor max int/Integer " + Integer.MAX_VALUE);//2147483647
		
		System.out.println("valor min char/character " + Character.MIN_VALUE);//0
		System.out.println("valor max char/character " + Character.MAX_VALUE);//65535
		
		System.out.println("valor min short/Short " + Short.MIN_VALUE);//-32768
		System.out.println("valor max short/Short " + Short.MAX_VALUE);//32767
		
		System.out.println("valor min byte/Byte " + Byte.MIN_VALUE);//-128
		System.out.println("valor max byte/Byte " + Byte.MAX_VALUE);//127

	}

}
