var Loja = {
	msg: '<img src="img/logo-serpro.png" width="150px" class="pull-right" alt="" /> Ao clicar em  <strong>Comprar Agora</strong> você será direcionado para o site de vendas do SERPRO.',
/*
	url : function(ambiente, qtd, ifa, ca, ra, cp) {
		return retorno = {
			msg : `'<img src="img/logo-serpro.png" width="150px" class="pull-right" alt="" /> Ao clicar em  <strong>Comprar Agora</strong> você será redirecionado para o site da ARSERPRO.</pre>'`,
			url : `${ambiente}.estaleiro.serpro.gov.br/#/e-commerce/${ifa}/inicio?q=${qtd}&ca=${ca}&ra=${ra}&cp=${cp}` 
		}
	},
*/	
	desenvolvimento : function(ifa, ca, ra, cp, tp) {
		var des = 'https://descliente';
		var codigoservico = 10433;
		if((tp == "78") || (tp == "80"))
		{
			return retorno = {
				msg : this.msg+'<br /><br /><b>Obs.: O NeoiD, por ser uma certificação que utiliza tecnologia inovadora e pioneira no Brasil, pode apresentar incompatibilidade com algumas soluções.</b>',
				url : des+'.estaleiro.serpro.gov.br/#!/e-commerce/'+ifa+'/inicio?ca='+ca+'&ra='+ra+'&cp='+cp+'&tp='+tp
			}
		}
		else
		{
			return retorno = {
				msg : this.msg,
				url : des+'.estaleiro.serpro.gov.br/#!/e-commerce/'+ifa+'/inicio?ca='+ca+'&ra='+ra+'&cp='+cp+'&tp='+tp
			}
		}
	},
	
	validacao : function(ifa, ca, ra, cp, tp) {
		var val = 'https://validacao';
		var codigoservico = 10433;
		if((tp == "78") || (tp == "80"))
		{
			return retorno = {
				msg : this.msg+'<br /><br /><b>Obs.: O NeoiD, por ser uma certificação que utiliza tecnologia inovadora e pioneira no Brasil, pode apresentar incompatibilidade com algumas soluções.</b>',
				url : val+'.cliente.serpro.gov.br/#!/e-commerce/'+ifa+'/inicio?ca='+ca+'&ra='+ra+'&cp='+cp+'&tp='+tp
			}
		}
		else
		{
			return retorno = {
				msg : this.msg,
				url : val+'.cliente.serpro.gov.br/#!/e-commerce/'+ifa+'/inicio?ca='+ca+'&ra='+ra+'&cp='+cp+'&tp='+tp
			}
		}
	},
	
	producao : function(ifa, ca, ra, cp, tp) {
		var prod = 'https://cliente';
		var codigoservico = 10433;
		if((tp == "250") || (tp == "252"))
		{
			return retorno = {
				msg : this.msg+'<br /><br /><b>Obs.: O NeoiD, por ser uma certificação que utiliza tecnologia inovadora e pioneira no Brasil, pode apresentar incompatibilidade com algumas soluções.</b>',
				url : prod+'.serpro.gov.br/#!/e-commerce/'+ifa+'/inicio?ca='+ca+'&ra='+ra+'&cp='+cp+'&tp='+tp
			}
		}
		else
		{
			return retorno = {
				msg : this.msg,
				url : prod+'.serpro.gov.br/#!/e-commerce/'+ifa+'/inicio?ca='+ca+'&ra='+ra+'&cp='+cp+'&tp='+tp
			}			
		}
	}
}