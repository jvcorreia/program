var CertificadoProxy = {
		
//	env: 'http://10.12.1.238:8080/',
	env: 'https://certificados.serpro.gov.br',
		
	arcorreios : function(template) {
		var env = 'https://certificados.serpro.gov.br';
		return retorno = {
			msg : '<img src="img/logo-correios.png" width="150px" class="pull-right" alt="" /> Ao clicar em <strong>Comprar Agora</strong> você será redirecionado para o site da AR CORREIOS.</pre>',
			url : env+'/arcorreiosrfb/pages/certificate_request/certificaterequest_direct.jsf?idTemplate='+template
		}
	},
	
	arserpro : function(template, ar) {
		var env = 'https://certificados.serpro.gov.br';
		return retorno = {
			msg : '<img src="img/logo-serpro.png" width="150px" class="pull-right" alt="" /> Ao clicar em  <strong>Comprar Agora</strong> você será redirecionado para o site da AR SERPRO.</pre>',
			url : env+'/'+ar+'/pages/certificate_request/certificaterequest_direct.jsf?idTemplate='+template 
		}
	},
	
	certificado : function() {
		return $.ajax({
	        type : 'GET',
	        dataType : 'json',
	        cache : false, 
	        url: 'js/proxy/data/certificados.json'
	    });
	},
	
	validacao : function() {
		return $.ajax({
	        type : 'GET',
	        dataType : 'json',
	        cache : false, 
	        url: 'js/proxy/data/certificados.validacao.json'
	    });
	},
	
	municipios : function(municipio) {
		return $.ajax({
	        type : 'GET',
	        dataType : 'json',
	        async: false,
	        url: 'js/proxy/data/municipios.json'
	    });
	},
	
	capitais : function(municipio) {
		return $.ajax({
	        type : 'GET',
	        dataType : 'json',
	        async: false,
	        url: 'js/proxy/data/capitais/municipios.json'
	    });
	}
}