var MunicipioProxy = {
		
	list : function() {
		return $.getJSON('js/proxy/data/municipios.json');
	},
		
	temSerpro : function(ibge) {
		var retorno = false;
		var vr = $.getJSON('js/proxy/data/capitais/municipios.json', function(data) {
			$.each(data, function(key, item) {
				if(item.codigoMunicipioIBGE == ibge) {
					retorno = true;
					vr.abort();
				}
			});
		});
		if(retorno) {
			return true;
		}
	}
}