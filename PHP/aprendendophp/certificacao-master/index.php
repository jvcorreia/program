<?php
	// ========================================================================
	// Setando local da data senão o php reclama
	// ========================================================================

	date_default_timezone_set('America/Sao_Paulo');

	// ========================================================================
	// Array com texto de documentos necessários
	// ========================================================================

	include 'documentos.necessarios.php';

	// ========================================================================
	// Array de certificados, preços do sigepe e função exibição
	// ========================================================================

     include 'certificados.php'; // Produção
	 //include 'certificados.validacao.php'; // Validação

	 // Obs: Na linha 512 e 517 também temos coisas para mudar em caso de validação / produção

	// ========================================================================
	// Retorno do texto para quem chamar esta página com parametro ?ifa=<ifa>
	// ========================================================================

	if(isset($_GET["ifa"]))
	{
		header("Access-Control-Allow-Origin: *");
		header("Access-Control-Allow-Headers: *");
		foreach($certificados as $k1 => $v1)
			foreach($v1 as $k2 => $v2)
				foreach($v2 as $k3 => $v3)
					if($v3[9] == $_GET["ifa"])
					{

						echo $documentosNecessarios[$v3[8]];
						die;
					}
		echo "Código IFA não encontrado. Não foi possível retornar os documentos necessários.";
		die;
	}

	// ========================================================================
	// Recuperando preço do Certificado em destaque (hardcoded lá embaixo)
	// ========================================================================
	/*
	$ifaDestaque = "20324";
	$destaquePreco = "Preço não encontrado.";

	foreach($sigecom as $sgc)
		if($ifaDestaque == $sgc->id)
		{
			$destaquePreco = number_format($sgc->ifaValValorUnitario, 2, ',', '.');
			break;
		}
	*/
?>
<!DOCTYPE html>
<html lang="pt">
<head>
	<!-- Global Site Tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-104929951-14"></script>
	<script>
		window.dataLayer = window.dataLayer || [];
		function gtag() { dataLayer.push(arguments); }
		gtag('js', new Date());

		gtag('config', 'UA-104929951-14');
	</script>
<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description"
		content="O certificado digital é um documento eletrônico que funciona como uma identidade digital. Seu uso confere validade jurídica aos atos praticados.">
	<meta name="keywords" content="Certificado digital; certificação digital; certificado mobile; A3; A1;">
	<meta name="author" content="Serpro">
	<title>Certificado Digital AC Serpro - ICP-Brasil</title>
	<link rel="preconnect" href="https://maxcdn.bootstrapcdn.com">
		<link rel="preconnect" href="https://fonts.googleapis.com">
		<link rel="preconnect" href="https://script.hotjar.com">
		<link rel="preconnect" href="https://in.hotjar.com">
		<link rel="preconnect" href="https://vc.hotjar.com">
		<link rel="preconnect" href="https://vars.hotjar.com">
		<link rel="preconnect" href="https://www.google-analytics.com">
		<link rel="preconnect" href="https://use.fontawesome.com">
		<link rel="preconnect" href="https://code.jquery.com">
	<link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
   <!-- Bootstrap Core CSS -->
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
		integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<!-- Font Awesome -->
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css"
		integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
	<!-- Custom CSS -->
	<link href="css/styles.min.css?=20190617" rel="stylesheet">

	<!-- Custom Fonts -->
	<link href="https://fonts.googleapis.com/css?family=Roboto+Slab:400,700&display=swap" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700&display=swap" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
	<!-- Hotjar Tracking Code for https://servicos.serpro.gov.br/loja/certificacao-digital/ -->
		<script>
		(function (h, o, t, j, a, r) {
			h.hj = h.hj || function () { (h.hj.q = h.hj.q || []).push(arguments) };
			h._hjSettings = { hjid: 815745, hjsv: 6 };
			a = o.getElementsByTagName('head')[0];
			r = o.createElement('script'); r.async = 1;
			r.src = t + h._hjSettings.hjid + j + h._hjSettings.hjsv;
			a.appendChild(r);
		})(window, document, 'https://static.hotjar.com/c/hotjar-', '.js?sv=');
	</script>
</head>
<body id="home-cert">
	<nav class="navbar navbar-expand-md navbar-light fixed-top bg-light">
		<div class="container">
			<a class="navbar-brand" tabindex="0" href="#"><img class="img-fluid center-block" src="img/marca-certificacao-digital.png"
					alt="Certificação Digital Serpro" width="120" /></a>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-cert"
				aria-controls="navbar-cert" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>

			<div class="collapse navbar-collapse" id="navbar-cert">
				<ul class="navbar-nav ml-auto">
					<li class="nav-item">
						<a class="nav-link" tabindex="0" href="#banner">Por que contratar?</a>
					</li>
					<li class="nav-item">
					<a class="nav-link" tabindex="0" href="#duvidas">Dúvidas e Atendimento</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" tabindex="0" href="#certificados">Nossos Certificados Digitais</a>
					</li>
				</ul>
			</div>
		</div>
	</nav>
	<main aria-label="Destaque: Certificado Digital para acesso ao WS Emplaca">
		<div class="jumbotron">
			<div class="container">
				<div class="col-md-8">
				<h1 class="h2 mt-2">Certificado Digital para acesso de estampadores e fabricantes ao WS-Emplaca</h1>
				<p>Gerencie os processos de estampagem e fabricação da Placa Mercosul com segurança e
					transparência.</p>
				<p><a class="btn btn-sm btn-cta smooth" href="#certificados">Garanta o seu!</a></p>
			</div>
			</div>
		</div>
	</main>
	<section role="complementary" id="certificados" aria-label="Conheça nossos Certificados Digitais">
		<div class="container">
				<p style="color:black; text-align:center;">Diante da declaração de pandemia do Covid-19 , o SERPRO decidiu suspender temporariamente os atendimentos de validação presencial, momento em que são conferidos os dados informados na solicitação do Certificado Digital. Para garantir a segurança de seus empregados e clientes, a diretoria aguarda novo parecer das autoridades públicas de saúde para remarcar os atendimentos presenciais.</p>
				<h2 class="pb-5 text-center">Conheça os nossos Certificados Digitais</h2>
			<div class="row">
				<div class="col-12">

					<ul class="nav nav-pills nav-fill nav-justified flex-column flex-md-row" id="lista-certificados" role="tablist" aria-label="Abas contendo os Certificados Digitais disponíveis">

						<!-- <li><a data-toggle="pill" href="#menu0">MAIS VENDIDOS</a></li> -->
						<li class="nav-item"><a class="nav-link active" tabindex="0" data-toggle="pill" href="#home" role="tab" aria-selected="true" aria-controls="home"
								onclick="gtag('event', 'click', {'event_category': 'certificado-digital', 'event_label': 'aba-ecpf'});">e-CPF</a>
						</li>
						<li class="nav-item"><a class="nav-link" tabindex="-1" data-toggle="pill" href="#menu2" role="tab" aria-selected="true" aria-controls="menu2"
								onclick="gtag('event', 'click', {'event_category': 'certificado-digital', 'event_label': 'aba-ecnpj'});">e-CNPJ</a>
						</li>
						<li class="nav-item"><a class="nav-link" tabindex="-1" data-toggle="pill" href="#menu3" role="tab" aria-selected="true" aria-controls="menu3"
								onclick="gtag('event', 'click', {'event_category': 'certificado-digital', 'event_label': 'aba-instituicoes-financeiras'});">INSTITUIÇÕES FINANCEIRAS</a></li>
						<li class="nav-item"><a class="nav-link" tabindex="-1" data-toggle="pill" href="#menu4" role="tab" aria-selected="true" aria-controls="menu4"
								onclick="gtag('event', 'click', {'event_category': 'certificado-digital', 'event_label': 'aba-equipamento'});">EQUIPAMENTO</a>
						</li>
						<li class="nav-item"><a class="nav-link" tabindex="-1" data-toggle="pill" href="#menu5" role="tab" aria-selected="true" aria-controls="menu5"
								onclick="gtag('event', 'click', {'event_category': 'certificado-digital', 'event_label': 'aba-carimbo-do-tempo'});">CARIMBO
								DO TEMPO</a></li>
						<li class="nav-item"><a class="nav-link" tabindex="-1" data-toggle="pill" href="#menu6" role="tab" aria-selected="true" aria-controls="menu6"
						onclick="gtag('event', 'click', {'event_category': 'certificado-digital', 'event_label': 'aba-governo'});">GOVERNO</a></li>
						<!--<li class="nav-item"><a class="nav-link" target="_blank" rel="noopener, noreferrer" href="http://www.serpro.gov.br/clientes/certificacao_digital/" onclick="gtag('event', 'click', {'event_category': 'certificado-digital', 'event_label': 'aba-governo'});">GOVERNO
							<i class="fas fa-external-link-alt" aria-hidden="true"></i>
									</a>
						</li>-->
					</ul>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-12 mx-auto">
					<div class="tab-content">
						<div id="home" class="tab-pane fade show active"  role="tabpanel" aria-label="Certificados e-CPF">
						
							<div class="row" >
								<div class="col-lg-6 certificado separate">
								<?= cert('ecpf','A1','dig');?>
								</div>
								<div class="col-lg-6 certificado">
								<?= cert('ecpf','A3','mob');?>
								</div>
							</div>

							<div class="row">
								<div class="col-lg-6 certificado separate">
								<?= cert('ecpf','A3','stok1');?>
								</div>
								<div class="col-lg-6 certificado" style="background-image: url(img/alerta);background-size:20%;background-repeat:no-repeat;background-position: center center; ">
								<p style="color: red; text-align:center" >Venda indisponível em função do Covid-19.</p>
								<div style="opacity: 0.3">
								<?= cert2('ecpf','A3','ctok1');?>
								</div>
								
								</div>
							</div>
							<div class="row">
								<div class="col-lg-6 certificado separate">
								<?= cert('ecpf','A3','stok3');?>
								</div>
								<div class="col-lg-6 certificado" style="background-image: url(img/alerta);background-size:20%;background-repeat:no-repeat;background-position: center center; ">
								<p style="color: red; text-align:center" >Venda indisponível em função do Covid-19.</p>
								<div style="opacity: 0.3">
								<?= cert2('ecpf','A3','ctok3');?>
								</div>
								
								</div>
							</div>
							<div class="row">
								<div class="col-lg-6 certificado separate">
								<?= cert('ecpf','A3','stok5');?>
								</div>
								<div class="col-lg-6 certificado" style="background-image: url(img/alerta);background-size:20%;background-repeat:no-repeat;background-position: center center; ">
								<p style="color: red; text-align:center" >Venda indisponível em função do Covid-19.</p>
								<div style="opacity: 0.3">
								<?= cert2('ecpf','A3','ctok5');?>
								</div>
								</div>
							</div>
						</div>
						<div id="menu2" class="tab-pane fade"  role="tabpanel" aria-label="Certificados e-CNPJ">
							<div class="row">
								<div class="col-lg-6 certificado separate">
								<?= cert('ecnpj','A1','dig');?>
								</div>
								<div class="col-lg-6 certificado">
								<?= cert('ecnpj','A3','mob');?>
								</div>
							</div>
							<div class="row">
								<div class="col-lg-6 certificado separate">
								<?= cert('ecnpj','A3','stok1');?>
								</div>
								<div class="col-lg-6 certificado" style="background-image: url(img/alerta);background-size:20%;background-repeat:no-repeat;background-position: center center; ">
								<p style="color: red; text-align:center" >Venda indisponível em função do Covid-19.</p>
								<div style="opacity: 0.3">
								<?= cert2('ecnpj','A3','ctok1');?>
								</div>
								
								</div>
							</div>
							<div class="row">
								<div class="col-lg-6 certificado separate">
								<?= cert('ecnpj','A3','stok3');?>
								</div>
								<div class="col-lg-6 certificado" style="background-image: url(img/alerta);background-size:20%;background-repeat:no-repeat;background-position: center center; ">
								<p style="color: red; text-align:center" >Venda indisponível em função do Covid-19.</p>
								<div style="opacity: 0.3">
								<?= cert2('ecnpj','A3','ctok3');?>
								</div>
								
								</div>
							</div>
							<div class="row">
								<div class="col-lg-6 certificado separate">
								<?= cert('ecnpj','A3','stok5');?>
								</div>
								<div class="col-lg-6 certificado" style="background-image: url(img/alerta);background-size:20%;background-repeat:no-repeat;background-position: center center; ">
								<p style="color: red; text-align:center" >Venda indisponível em função do Covid-19.</p>
								<div style="opacity: 0.3">
								<?= cert2('ecnpj','A3','ctok5');?>
								</div>
								
								</div>
							</div>
						</div>
						<div id="menu3" class="tab-pane fade"  role="tabpanel" aria-label="Certificados Instituições Financeiras">
							<div class="row">
								<div class="col-lg-6 certificado separate">
								<?= cert('banco','A1','bac');?>
								</div>
								<div class="col-lg-6 certificado">
								<?= cert('banco','A1','c3');?>
								</div>
							</div>
							<div class="row">
								<div class="col-lg-6 certificado separate">
								<?= cert('banco','A1','cad');?>
								</div>
								<div class="col-lg-6 certificado">
								<?= cert('banco','A1','cip');?>
								</div>
							</div>
							<div class="row">
								<div class="col-lg-6 certificado separate">
								<?= cert('banco','A1','compe');?>
								</div>
								<div class="col-lg-6 certificado">
								<?= cert('banco','A1','eco');?>
								</div>
							</div>
							<div class="row">
								<div class="col-lg-6 certificado">
								<?= cert('banco','A1','spb');?>
								</div>
							</div>
						</div>
						<div id="menu4" class="tab-pane fade"  role="tabpanel" aria-label="Certificados Equipamento">
							<div class="row">
								<div class="col-lg-6 certificado separate">
								<?= cert('equip-apl','A1','equip');?>
								</div>
								<div class="col-lg-6 certificado">
								<?= cert('equip-apl','A1','equip-mult');?>
								</div>
							</div>
						</div>
						<div id="menu5" class="tab-pane fade"  role="tabpanel" aria-label="Certificados Carimbo do Tempo">
							<div class="row">
								<div class="col-lg-6 certificado">
								<?= cert('carimbo','tempo','tmp');?>
								</div>
							</div>
						</div>
						<div id="menu6" class="tab-pane fade"  role="tabpanel" aria-label="Certificados Governo">
							<div class="alert alert-secondary col-md-10 mx-auto" role="alert">Estou ciente de que a opção escolhida para contratação deste serviço é exclusivamente para Órgãos e Entidades pertencentes à Administração Pública de direito público, subordinando-se aos termos da lei 8.666/93</div>
							<div class="row">
							<!-- e-CPF -->
							<div class="col-lg-6 certificado separate">
								<?= cert('ecpf','A1','dig');?>
								</div>
								<div class="col-lg-6 certificado">
								<?= cert('ecpf','A3','mob');?>
								</div>
							</div>
							<div class="row">
								<div class="col-lg-6 certificado separate">
								<?= cert('ecpf','A3','stok1');?>
								</div>
								<div class="col-lg-6 certificado" style="background-image: url(img/alerta);background-size:20%;background-repeat:no-repeat;background-position: center center; ">
								<p style="color: red; text-align:center" >Venda indisponível em função do Covid-19.</p>
								<div style="opacity: 0.3">
								<?= cert2('ecpf','A3','ctok1');?>
								</div>
								
								</div>
							</div>
							<div class="row">
								<div class="col-lg-6 certificado separate">
								<?= cert('ecpf','A3','stok3');?>
								</div>
								<div class="col-lg-6 certificado" style="background-image: url(img/alerta);background-size:20%;background-repeat:no-repeat;background-position: center center; ">
								<p style="color: red; text-align:center" >Venda indisponível em função do Covid-19.</p>
								<div style="opacity: 0.3">
								<?= cert2('ecpf','A3','ctok3');?>
								</div>
								
								</div>
							</div>
							<div class="row">
								<div class="col-lg-6 certificado separate">
								<?= cert('ecpf','A3','stok5');?>
								</div>
								<div class="col-lg-6 certificado" style="background-image: url(img/alerta);background-size:20%;background-repeat:no-repeat;background-position: center center; ">
								<p style="color: red; text-align:center" >Venda indisponível em função do Covid-19.</p>
								<div style="opacity: 0.3">
								<?= cert2('ecpf','A3','ctok5');?>
								</div>
								
								</div>
							</div>
							<!-- e-CNPJ -->
							<div class="row">
								<div class="col-lg-6 certificado separate">
								<?= cert('ecnpj','A1','dig');?>
								</div>
								<div class="col-lg-6 certificado">
								<?= cert('ecnpj','A3','mob');?>
								</div>
							</div>
							<div class="row">
								<div class="col-lg-6 certificado separate">
								<?= cert('ecnpj','A3','stok1');?>
								</div>
								<div class="col-lg-6 certificado" style="background-image: url(img/alerta);background-size:20%;background-repeat:no-repeat;background-position: center center; ">
								<p style="color: red; text-align:center" >Venda indisponível em função do Covid-19.</p>
								<div style="opacity: 0.3">
								<?= cert2('ecnpj','A3','ctok1');?>
								
								</div>
								
								</div>
							</div>
							<div class="row">
								<div class="col-lg-6 certificado separate">
								<?= cert('ecnpj','A3','stok3');?>
								</div>
								<div class="col-lg-6 certificado" style="background-image: url(img/alerta);background-size:20%;background-repeat:no-repeat;background-position: center center; ">
								<p style="color: red; text-align:center" >Venda indisponível em função do Covid-19.</p>
								<div style="opacity: 0.3">
								<?= cert2('ecnpj','A3','ctok3');?>
								</div>
								
								</div>
							</div>
							<div class="row">
								<div class="col-lg-6 certificado separate">
								<?= cert('ecnpj','A3','stok5');?>
								</div>
								<div class="col-lg-6 certificado" style="background-image: url(img/alerta);background-size:20%;background-repeat:no-repeat;background-position: center center; ">
								<p style="color: red; text-align:center" >Venda indisponível em função do Covid-19.</p>
								<div style="opacity: 0.3">
								<?= cert2('ecnpj','A3','ctok5');?>
								</div>
								
								</div>
							</div>
							<!-- Equipamento -->
							<div class="row">
								<div class="col-lg-6 certificado separate">
								<?= cert('equip-apl','A1','equip');?>
								</div>
								<div class="col-lg-6 certificado">
								<?= cert('equip-apl','A1','equip-mult');?>
								</div>
							</div>



						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section role="complementary" id="banner" aria-label="Motivos para contratar com o Serpro" class="text-center">
		<h2>Por que contratar um Certificado Digital com o Serpro?</h2>
		<div class="container">
			<div class="col-md-10 mx-auto">
			<div class="row pt-5">
				<div class="col-md-4">
					<div class="box grow m-3">
							<i class="fas fa-flag-checkered fa-2x"  aria-hidden="true"></i>
							<p>Pioneiro em <br />Certificação Digital <span>18 anos </span>de experiência</p>
					</div>
				</div>
				<div class="col-md-4 mt-4 mt-md-0">
						<div class="box grow m-3">
								<i class="fas fa-map-marker-alt fa-2x" aria-hidden="true"></i>
								<p>Mais de <span>500</span> postos de atendimento </p>
						</div>
				</div>
				<div class="col-md-4  mt-4 mt-md-0">
						<div class="box grow m-3">
								<i class="far fa-clock fa-2x" aria-hidden="true"></i>
								<p>Central de <br />atendimento <span>24h</span></p>
						</div>
				</div>
			</div>
		</div>
		</div>
	</section>

	<section role="complementary" id="duvidas" aria-label="Dúvidas e Atendimento">
		<div class="container text-center">
			<div class="row">
				<div class="col-sm-12 mx-auto">
					<h2 class="section-heading">Tire suas Dúvidas sobre Certificação Digital</h2>
					<p class="py-3">Escolha abaixo a opção que melhor se encaixa ao seu perfil e preencha o formulário:</p>
					<div id="lista-temas" class="row">
						<div class="item-tema col-md-4">
							<a href="https://cssinter.serpro.gov.br/SCCDPortalWEB/pages/dynamicPortal.jsf?ITEMNUM=2763"
							tabindex="0" target="_blank" rel="noopener noreferrer">Cidadão</a>
						</div>
						<div class="item-tema col-md-4">
							<a href="https://cssinter.serpro.gov.br/SCCDPortalWEB/pages/dynamicPortal.jsf?ITEMNUM=2764"
							tabindex="0" target="_blank" rel="noopener noreferrer">Governo</a>
							<!--(Clientes Institucionais/Órgãos Públicos-->
						</div>
						<div class="item-tema col-md-4">
							<a href="https://cssinter.serpro.gov.br/SCCDPortalWEB/pages/dynamicPortal.jsf?ITEMNUM=2766"
							tabindex="0" target="_blank" rel="noopener noreferrer">Autoridade Certificadora/Registro</a>
						</div>
						<div class="item-tema col-md-4">
							<a href="https://cssinter.serpro.gov.br/SCCDPortalWEB/pages/dynamicPortal.jsf?ITEMNUM=2773"
							tabindex="0" target="_blank" rel="noopener noreferrer">Sistema Agenda Certificados</a>
						</div>
						<div class="item-tema col-md-4">
							<a href="https://cssinter.serpro.gov.br/SCCDPortalWEB/pages/dynamicPortal.jsf?ITEMNUM=2765"
							tabindex="0" target="_blank" rel="noopener noreferrer">Empregado Serpro</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section role="banner" id="id" aria-label="Selos de certificação" class="pt-0 mt-0">
		<div class="container text-center">
			<div class="row">
				<div class="col col-sm-8 mx-auto">
                                        <a href="https://www.cpacanada.ca/webtrustseal?sealid=10309" target="_blank" alt="Selos de certificação da empresa: Webtrust"><img src="img/webtrustCA2.jpg" class="img-fluid" style="vertical-align: bottom" alt="Selos de certificação da empresa: Webtrust"></a>
					<img src="img/selos-certificados.JPG" class="img-fluid" alt="Selos de certificação da empresa: APCER ISO/IEC 27001 e IONET">
				</div>
			</div>
		</div>
	</section>


	<div id="voltar-ao-topo">
		<div class="container">
			<div class="row">
				<div class="col"><a href="#home-cert" tabindex="0">voltar para o topo <span>↑ </span></a>
				</div>
			</div>
		</div>
	</div>
	<footer>
		<div class="container">
			<div class="row">
				<div class="col-md-4 text-center text-md-left">
					<p class="small">
						<strong class="text-uppercase">Equipe de Atendimento Serpro</strong>
						<a style="color:white;" tabindex="0" target="_blank" rel="noopener noreferrer" class="d-block"
							href="https://cssinter.serpro.gov.br/SCCDPortalWEB/pages/dynamicPortal.jsf?ITEMNUM=2215">
							<i class="fas fa-external-link-alt fa-sm" style="color:inherit;" aria-hidden="true"></i> Disponível para
							tirar dúvidas ou prestar ajuda</a>
						<a style="color:white;" tabindex="0" target="_blank" rel="noopener noreferrer" class="d-block"
							href="http://www.serpro.gov.br/privacidade/"><i class="fas fa-external-link-alt fa-sm"
								style="color:inherit;" aria-hidden="true"></i> Declaração de Conformidade: proteção de dados</a>
					</p>
				</div>
				<div class="col-md-8 text-center text-md-right">
					<img class="img-fluid" src="https://servicos.serpro.gov.br/assets/img/assinatura-governo.png"
						alt="Serpro, Brasil - Governo Federal">
				</div>
			</div>
		</div>
	</footer>
    	<!-- Modal Localizacao -->
	<div class="modal fade bd-example-modal-sm" tabindex="-1" id="localizacao" role="dialog" aria-label="Certificados e-Socialio para o comprador informar sua localização" aria-hidden="true">
			<div id="modal-localizacao" class="modal-dialog modal-dialog-scrollable modal-dialog-centered modal-md">
				<div class="modal-content" style="min-height: 200px;">
						<div class="modal-header">
								<h3 class="modal-title h5">Informe sua localização</h3>
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
										<span aria-hidden="true">&times;</span>
							</div>
							<div id="modalLocal" class="modal-body text-right">

								<div id="scrollable-dropdown-menu">
									<input id="municipio" class="typeahead" type="text" placeholder="Município - UF">
									<p id="informacao">
								</div>
								<a id="comprar" class="btn btn-sm btn-cta text-right" target="_blank">Comprar Agora</a>
							</div>
				</div>
			</div>
		</div>
		<!-- End Modal Localizacao -->

		<!-- Modal Documentos -->
		<div class="modal fade bd-example-modal-sm" tabindex="-1" id="documentos" role="dialog" aria-label="Certificados e-Social documentos necessários" aria-hidden="true">
				<div id="modal-documentos" class="modal-dialog modal-dialog-scrollable modal-dialog-centered modal-md">
					<div class="modal-content" style="min-height: 200px;">
							<div class="modal-header">
									<h3 class="modal-title h5">Documentos necessários</h3>
									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
											<span aria-hidden="true">&times;</span>
								</div>
								<div id="modalDocumentos" class="modal-body text-left">
										<p id="documento">
								</div>
					</div>
				</div>
			</div>
		<!-- End Modal Documentos -->


	<!-- Scripts de layout -->
	<script src="js/lib/jquery-3.4.1.min.js"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
		integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
		crossorigin="anonymous"></script>

	<!-- Scripts de desenvolvimento -->

    <!-- jQuery -->
	<!-- <script src="js/lib/jquery.js"></script> -->

    <!-- Bootstrap Core JavaScript -->
    <script src="js/lib/handlebars-v4.1.2.min.js"></script>
    <script src="js/lib/typeahead.bundle.20190423.min.js"></script>

    <!-- Proxy JavaScript -->
    <script src="js/proxy/loja20180830.js"></script>
    <script src="js/proxy/certificados20180418.js"></script>

    <!-- Controller JavaScript -->
 	<!-- Conteúdo do  src="js/controller/certificados.js" foi trazido para cá -->
    <script>
	$(document).ready(function() {

		var docs = [
			'<?php echo $documentosNecessarios[0]; ?>',
			'<?php echo $documentosNecessarios[1]; ?>',
			'<?php echo $documentosNecessarios[2]; ?>',
			'<?php echo $documentosNecessarios[3]; ?>',
			'<?php echo $documentosNecessarios[4]; ?>',
		]

		$("#informacao").hide();
		$("#comprar").hide();

		var template = '';
		$.ajax({
		  url: "js/proxy/data/municipios.json",
		  cache: true
		}).done(listOk);

		$(".btn-xs").click(function() {
			$("#informacao").hide();
			$("#comprar").hide();
			$("#municipio").val('');
		});

		$(".btn-sm").click(function() {
			$("#informacao").hide();
			$("#comprar").hide();
			$("#municipio").val('');
		});

		$("a").click(function() {
			template = this.name;
		});

		$("a").click(function() {
			$("#documento").html(docs[this.name]);
		});

		$("#comprar").click(function() {
			$(".close").click();
		});

		$('#scrollable-dropdown-menu').on('typeahead:selected', function (e, municipio) {
			$("#comprar").hide();
			/*
			Maceió - AL = 2704302
			Manaus - AM = 1302603
			Vitória - ES = 3205309
			Campo Grande - MS = 5002704
			Cuiabá - MT = 5103403
			João Pessoa - PB = 2507507
			Natal - RN = 2408102
			*/
			if
			(
				(municipio.codigoMunicipioIBGE == '2704302') ||
				(municipio.codigoMunicipioIBGE == '1302603') ||
				(municipio.codigoMunicipioIBGE == '3205309') ||
				(municipio.codigoMunicipioIBGE == '5002704') ||
				(municipio.codigoMunicipioIBGE == '5103403') ||
				(municipio.codigoMunicipioIBGE == '2507507') ||
				(municipio.codigoMunicipioIBGE == '2408102')
			)
			{
				$("#informacao").html(
				'<center>Prezado cliente, o SERPRO <b>não proverá</b> mais atendimento para a Certificação Digital em seus Escritórios nas cidades de Maceió - AL, Manaus - AM, Vitória - ES, Campo Grande - MS, Cuiabá - MT, João Pessoa - PB e Natal - RN a partir de 02 <b>de Abril de 2020.</b><br /><br />'+
				'O atendimento para estas cidades será realizado pelas unidades dos CORREIOS. Assim, sua solicitação deve ser realizada na  <a href="https://www.correios.com.br/atendimento/para-o-cidadao/certificado-digital">https://www.correios.com.br/atendimento/para-o-cidadao/certificado-digital</a><br /><br />'+
				'Grato pela compreensão.</center>');
			}
			else
			{
				if(template == '')
				{
					$("#informacao").html('<pre>Tipo de Certificado não configurado.</pre>');
				}
				else
				{
					if(municipio.serpro)
					{
						serpro(template, '');
					}
					else
					{
						var arrayTemplate = [188, 189, 204, 213, 214]
						var somenteCorreios = arrayTemplate.filter(function(elem, i) {
							return elem == template;
						} );

						if(somenteCorreios.length > 0) {
							var retorno = CertificadoProxy.arcorreios(template);
							$("#informacao").html(retorno.msg);
							$("#comprar").attr('href', retorno.url);
							$("#comprar").show();
						}
						else
						{
							var msg = '<b>Este certificado é emitido apenas nas capitais. Durante o agendamento você poderá escolher a capital mais próxima.</b><br /><br />';
							serpro(template, msg);
						}
					}
					//$("#comprar").show();
				}
			}
			$("#informacao").show();
		});
	});

	function serpro(template, msg) {
		CertificadoProxy.certificado().done(function(certificados) { // Produção
		//CertificadoProxy.validacao().done(function(certificados) { // Validacao
			var certificado = certificados.filter(function(elem, i) {
				return elem.template == template;
			} );
			//var retorno = Loja.validacao(certificado[0].insumo, certificado[0].ac.id, certificado[0].ac.ar[0].id, certificado[0].cp, template); // Validação
			var retorno = Loja.producao(certificado[0].insumo, certificado[0].ac.id, certificado[0].ac.ar[0].id, certificado[0].cp, template); // Produção
			/*
			ID CA	  CA	        ID Template	Nome	                    ID RA	RA	      	Institucional	Consulta  IdTemplate  Old
			7	      ACSERPROACFv5	132	        PJ A1	                    2	    ARSERPRO	false	        false     189         e-CNPJ A1 - 1 ano de validade
			7	      ACSERPROACFv5	171	        PJ A3	                    2	    ARSERPRO	false	        false     213         e-CNPJ A3 - 3 anos de validade
			7	      ACSERPROACFv5	167	        PJ A3 (1 ano)	            2	    ARSERPRO	false	        false     207         e-CNPJ A3 - 1 ano de validade
			7     	  ACSERPROACFv5	166	        PJ A3 (1 ano com Token)	    2	    ARSERPRO	false	        false     208         e-CNPJ A3 com Token - 1 ano de validade
			7	      ACSERPROACFv5	164	        PJ A3 (5 anos)	            2	    ARSERPRO	false	        false     211         e-CNPJ A3 sem Token - 5 anos de validade
			7	      ACSERPROACFv5	175	        PJ A3 (5 anos com Token)	2	    ARSERPRO	false	        false     206         e-CNPJ A3 com Token - 5 anos de validade
			7	      ACSERPROACFv5	165	        PJ A3 (Token)	            2	    ARSERPRO	false	        false     212         e-CNPJ A3 com Token - 3 anos de validade
			*/
			var templateACF = "";
			if(template == '189') { templateACF = '132'; }
			if(template == '213') { templateACF = '171'; }
			if(template == '207') { templateACF = '167'; }
			if(template == '208') { templateACF = '166'; }
			if(template == '211') { templateACF = '164'; }
			if(template == '206') { templateACF = '175'; }
			if(template == '212') { templateACF = '165'; }
			var retornoACF = Loja.producao(certificado[0].insumo, '7', '2', '0', templateACF); // Produção
			//document.write(retornoACF.url);
			var addtext = "";
			if(templateACF != "")
			{
				addtext = '<br /><br />O responsável pelo certificado é o representante legal junto à Receita Federal do Brasil?<br />';
				addtext = addtext + '<input type="radio" id="radio" name="radio" value="s" onclick="testeee(\''+retorno.url+'\')" /> Sim, sou o representante<br />';
				addtext = addtext + '<input type="radio" id="radio" name="radio" value="n" onclick="testeee(\''+retornoACF.url+'\')" /> Não, vou usar procuração';
			}
			else
			{
				$("#comprar").show();
			}
			$("#informacao").html(msg + retorno.msg + addtext);
			$("#comprar").attr('href', retorno.url);
		});
	}

	function testeee(urlpar)
	{
		$("#comprar").attr('href', urlpar);
		$("#comprar").show();
	}

	function listOk(data) {
		var charMap = {
			"à": "a",
			"á": "a",
			"Á": "A",
			"â": "a",
			"ã": "a",
			"é": "e",
			"è": "e",
			"ê": "e",
			"ë": "e",
			"é": "e",
			"ẽ": "e",
			"É": "E",
			"ï": "i",
			"î": "i",
			"í": "i",
			"Í": "I",
			"ô": "o",
			"ö": "o",
			"ó": "o",
			"Ó": "O",
			"õ": "o",
			"Õ": "O",
			"û": "u",
			"ù": "u",
			"ü": "u",
			"ú": "u",
			"Ú": "U",
			"ñ": "n"
		};

		var normalize = function (input) {
			$.each(charMap, function (unnormalizedChar, normalizedChar) {
				var regex = new RegExp(unnormalizedChar, 'gi');
				input = input.toString().replace(regex, normalizedChar);
			});
			return input;
		};

		var tokenizer = function (q) {
			var normalized = normalize(q);
			return Bloodhound.tokenizers.whitespace(normalized);
		};

		municipios = new Bloodhound({
			datumTokenizer: function customTokenizer(datum) {
				var nome = Bloodhound.tokenizers.whitespace(datum.normalizedName);
				var uf = Bloodhound.tokenizers.whitespace(datum.uf ? datum.uf.sigla : '');
				return nome.concat(uf);
			},
			queryTokenizer: tokenizer,
			local: $.each(data, function(i, obj) {
				obj.normalizedName = normalize(obj.nome);
				})
		});

		municipios.initialize();

		$('#scrollable-dropdown-menu .typeahead').typeahead({
			hint: true,
			highlight: true,
			limit: 5,
			minLength: 1

		}, {
			display: function(item) {
				return item.nome+' - '+item.uf.sigla
			},
			source: municipios.ttAdapter(),
			templates: {
				suggestion: Handlebars.compile('<div><strong>{{nome}}</strong> - {{uf.sigla}}</div>')
			}
		});
	}
	</script>

	<script>
		$('a.smooth').click(function(){
			$('html, body').animate({
				scrollTop: $( $(this).attr('href') ).offset().top
			}, 300);
			return false;
		});
	</script>
	</body>
</html>