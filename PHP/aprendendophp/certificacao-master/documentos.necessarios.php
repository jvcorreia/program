<?php
// ============================================================================
// E-CPF
// ============================================================================

$documentosNecessarios[0] =
'A confirmação da identidade da Pessoa Física é realizada por um Agente de Registro mediante a presença física do interessado e através dos documentos de identificação ORIGINAIS.<br />'.
'<br />'.
'Caso não seja possível a identificação através do documento apresentado, um segundo será solicitado. Para agilizar o atendimento solicitamos que compareça à Autoridade de Registro portando 2 (DOIS) documentos de identificação (preferencialmente RG e CNH).<br />'.
'<br />'.
'Documento de Identificação (OBRIGATÓRIO)<br />'.
'<br />'.
'<ul>'.
	'<li>Cédula de Identidade;</li>'.
	'<ul>'.
		'<li>Registro Geral – RG;</li>'.
		'<li>Carteira Nacional de Habilitação – CNH;</li>'.
		'<li>Carteira de Trabalho – CTPS(modelo informatizado);</li>'.
		'<li>Identificação Profissional emitida por conselho de classe ou órgão competente (OAB, CRM, etc);</li>'.
		'<li>Passaporte, se brasileiro.</li>'.
	'</ul>'.
	'<li>Carteira Nacional de Estrangeiro – CNE, se estrangeiro domiciliado no Brasil;</li>'.
	'<li>Passaporte, se estrangeiro não domiciliado no Brasil.</li>'.
'</ul>'.
'<br />'.
'Outros Documentos (OBRIGATÓRIOS)<br />'.
'<br />'.
'<ul>'.
	'<li>Cadastro de Pessoa Física (CPF);</li>'.
	'<li>Levar o comprovante de pagamento em mãos.</li>'.
'</ul>'.
'<br />'.
'Outros Documentos (OPCIONAIS)<br />'.
'<br />'.
'<ul>'.
'<li>Número de Identificação Social - NIS (NIT/PIS/PASEP);</li>'.
'<li>Cadastro Específico do INSS – CEI;</li>'.
'<li>Título de eleitor.</li>'.
'</ul>'.
'<br />'.
'ATENÇÃO: Os documentos OPCIONAIS, se preenchidos na solicitação do certificado, deverão ser apresentados na sua versão ORIGINAL.<br />'.
'<br />'.
'Para mais informações com relação à documentação solicitada, acesse:<br />'.
'<ul>'.
'<li><a href="https://ccd.serpro.gov.br/acserprorfb/docs/dpcacserprorfb.pdf" target="_blank"><u>DPC da AC SERPRO RFB;</u></a></li>'.
'<li><a href="http://www.iti.gov.br/images/repositorio/legislacao/resolucoes/em-vigor/resolucao90.pdf" target="_blank"><u>Resolução 90 da ICP-Brasil.</u></a></li>'.
'</ul>';

// ============================================================================
// E-CNPJ
// ============================================================================

$documentosNecessarios[1] =
'Sendo o titular Pessoa Jurídica, será designada Pessoa Física como responsável pelo certificado. Será designado como Responsável pelo Certificado o Representante Legal da Pessoa Jurídica cadastrado na Receita Federal do Brasil.<br />'.
'<br />'.
'A identificação das Pessoas Física e Jurídica são realizadas por um Agente de Registro mediante a presença física do interessado e através dos documentos de identificação ORIGINAIS.<br />'.
'<br />'.
'Identificação da(s) Pessoa(s) Física(s):<br />'.
'<br />'.
'Documento de Identificação (OBRIGATÓRIO)<br />'.
'<br />'.
'<ul>'.
	'<li>Cédula de Identidade;</li>'.
	'<ul>'.
		'<li>Registro Geral – RG;</li>'.
		'<li>Carteira Nacional de Habilitação – CNH;</li>'.
		'<li>Carteira de Trabalho – CTPS(modelo informatizado);</li>'.
		'<li>Identificação Profissional emitida por conselho de classe ou órgão competente (OAB, CRM, etc);</li>'.
		'<li>Passaporte, se brasileiro.</li>'.
	'</ul>'.
	'<li>Carteira Nacional de Estrangeiro – CNE, se estrangeiro domiciliado no Brasil;</li>'.
	'<li>Passaporte, se estrangeiro não domiciliado no Brasil.</li>'.
'</ul>'.
'<br />'.
'Outros Documentos (OBRIGATÓRIOS)<br />'.
'<br />'.
'<ul>'.
	'<li>Cadastro de Pessoa Física (CPF);</li>'.
	'<li>Levar o comprovante de pagamento em mãos.</li>'.
'</ul>'.
'<br />'.
'Outros Documentos (OPCIONAIS)<br />'.
'<br />'.
'<ul>'.
'<li>Número de Identificação Social - NIS (NIT/PIS/PASEP);</li>'.
'<li>Cadastro Específico do INSS – CEI;</li>'.
'<li>Título de eleitor.</li>'.
'</ul>'.
'<br />'.
'Identificação da Pessoa Jurídica<br />'.
'<br />'.
'<ul>'.
'<li>Ato Constitutivo original devidamente registrado no órgão competente;</li>'.
'<li>Todas as alterações do ato constitutivo, quando aplicável;</li>'.
'<li>Documentos da eleição de seus administradores, quando aplicável;</li>'.
'<li>Prova de inscrição no Cadastro Nacional de Pessoas Jurídicas – CNPJ;</li>'.
'</ul>'.
'<br />'.
'Outros Documentos (OPCIONAIS)<br />'.
'<br />'.
'<ul>'.
'<li>Prova de inscrição no Cadastro Específico do INSS – CEI.</li>'.
'</ul>'.
'<br />'.
'ATENÇÃO: Os documentos OPCIONAIS, se preenchidos na solicitação do certificado, deverão ser apresentados na sua versão ORIGINAL.<br />'.
'<br />'.
'NOTA 1: O Agente de Registro analisará a cláusula de administração do Ato Constitutivo a fim de identificar os administradores da Pessoa Jurídica. Caso haja mais de um administrador, e estes administram a empresa em conjunto, todos deverão comparecer à Autoridade de Registro munidos de seus documentos para identificação presencial.<br />'.
'<br />'.
'PARA CERTIFICADOS DE CONDOMÍNIOS:<br />'.
'<br />'.
'Para fins de emissão do certificado digital de Pessoa Jurídica, relativamente aos condomínios, é imprescindível a comprovação de seu ato constitutivo devidamente registrado no Cartório de Registro de Imóveis.<br />'.
'<br />'.
'Entende-se como ato constitutivo:<br />'.
'<ul>'.
'<li>O testamento;</li>'.
'<li>A escritura pública ou particular de instituição;</li>'.
'<li>Convenção emitida e registrada após a vigência do novo Código Civil (art. 1332 e ss).</li>'.
'</ul>'.
'<br />'.
'Não se entende como ato constitutivo quaisquer outros documentos, tais como o Regimento Interno, declarações emitidas pelos respectivos síndicos ou a ata de assembléia condominial.<br />'.
'<br />'.
'Àqueles condomínios não constituídos nos termos da legislação, admite-se, para fins de comprovação de sua existência:<br />'.
'<br />'.
'<ul>'.
'<li>Certidão do Instrumento de Individualização do Condomínio emitida pelo Cartório de Registro de Imóveis;</li>'.
'<li>Ata da Assembléia Condominial que escolheu o Síndico;</li>'.
'<ul>'.
'<li>Lista dos participantes da eleição, sendo obrigatória a participação de ao menos um proprietário de imóvel localizado no condomínio, com a comprovação de sua propriedade e firma reconhecida na referida Ata.</li>'.
'</ul>'.
'</ul>'.
'<br />'.
'A convenção de condomínio registrada anteriormente à vigência do novo Código Civil e a ata de eleição do síndico integram igualmente a documentação necessária à emissão do certificado.<br />'.
'<br />'.
'Para mais informações com relação à documentação solicitada, acesse:<br />'.
'<ul>'.
'<li><a href="https://ccd.serpro.gov.br/acserprorfb/docs/dpcacserprorfb.pdf" target="_blank"><u>DPC da AC SERPRO RFB;</u></a></li>'.
'<li><a href="http://www.iti.gov.br/images/repositorio/legislacao/resolucoes/em-vigor/resolucao90.pdf" target="_blank"><u>Resolução 90 da ICP-Brasil.</u></a></li>'.
'</ul>';

// ============================================================================
// Instituições Financeiras
// ============================================================================

$documentosNecessarios[2] =
'Sendo o titular Pessoa Jurídica, será designada Pessoa Física como responsável pelo certificado.<br />'.
'<br />'.
'Será designado como Responsável pelo Certificado o Representante Legal da Pessoa Jurídica cadastrado na Receita Federal do Brasil ou outra pessoa mediante outorga de procuração atribuindo poderes para solicitação de certificado para equipamento e assinatura do respectivo termo de titularidade.<br />'.
'<br />'.
'A identificação das Pessoas Física e Jurídica são realizadas por um Agente de Registro mediante a presença física do interessado e através dos documentos de identificação ORIGINAIS.<br />'.
'<br />'.
'Identificação da(s) Pessoa(s) Física(s):<br />'.
'<br />'.
'Documento de Identificação (OBRIGATÓRIO)<br />'.
'<br />'.
'<ul>'.
	'<li>Cédula de Identidade;</li>'.
	'<ul>'.
		'<li>Registro Geral – RG;</li>'.
		'<li>Carteira Nacional de Habilitação – CNH;</li>'.
		'<li>Carteira de Trabalho – CTPS(modelo informatizado);</li>'.
		'<li>Identificação Profissional emitida por conselho de classe ou órgão competente (OAB, CRM, etc);</li>'.
		'<li>Passaporte, se brasileiro.</li>'.
	'</ul>'.
	'<li>Carteira Nacional de Estrangeiro – CNE, se estrangeiro domiciliado no Brasil;</li>'.
	'<li>Passaporte, se estrangeiro não domiciliado no Brasil.</li>'.
'</ul>'.
'<br />'.
'Outros Documentos (OBRIGATÓRIOS)<br />'.
'<br />'.
'<ul>'.
	'<li>Cadastro de Pessoa Física (CPF);</li>'.
	'<li>Duas vias do Termo de Titularidade. Este termo estará disponível ao final do processo de solicitação do certificado digital. A assinatura do termo deverá ser realizada somente na presença de um Agente de Registro.</li>'.
	'<li>Levar o comprovante de pagamento em mãos.</li>'.
'</ul>'.
'<br />'.
'Outros Documentos (OPCIONAIS)<br />'.
'<br />'.
'<ul>'.
'<li>Número de Identificação Social - NIS (NIT/PIS/PASEP);</li>'.
'<li>Cadastro Específico do INSS – CEI;</li>'.
'<li>Título de eleitor.</li>'.
'</ul>'.
'<br />'.
'Identificação da Pessoa Jurídica<br />'.
'<br />'.
'<ul>'.
'<li>Ato Constitutivo original devidamente registrado no órgão competente;</li>'.
'<li>Todas as alterações do ato constitutivo, quando aplicável;</li>'.
'<li>Documentos da eleição de seus administradores, quando aplicável;</li>'.
'<li>Prova de inscrição no Cadastro Nacional de Pessoas Jurídicas – CNPJ;</li>'.
'</ul>'.
'<br />'.
'Outros Documentos (OPCIONAIS)<br />'.
'<br />'.
'<ul>'.
'<li>Prova de inscrição no Cadastro Específico do INSS – CEI.</li>'.
'</ul>'.
'<br />'.
'ATENÇÃO: Os documentos OPCIONAIS, se preenchidos na solicitação do certificado, deverão ser apresentados na sua versão ORIGINAL.<br />'.
'<br />'.
'Identificação de Equipamento<br />'.
'<br />'.
'Para certificados de equipamento que utilizem URL no campo Common Name, é verificado se o solicitante do certificado detém o registro do nome de domínio junto ao órgão competente, ou se possui autorização do titular do domínio para usar aquele nome. Nesse caso deve ser apresentada documentação comprobatória (Termo de Autorização para Uso de Domínio ou similar) devidamente assinado pelo titular do domínio.<br />'.
'<br />'.
'NOTA 1: Para domínios que utilizam o sufixo ".br" a verificação de registro de domínio será realizada através do site <a href="https://registro.br/2/whois" target="_blank"><u>https://registro.br/2/whois.</u></a>';

// ============================================================================
// Equipamento / Aplicação
// ============================================================================

$documentosNecessarios[3] =
'Sendo o titular Pessoa Jurídica, será designada Pessoa Física como responsável pelo certificado.<br />'.
'<br />'.
'Será designado como Responsável pelo Certificado o Representante Legal da Pessoa Jurídica cadastrado na Receita Federal do Brasil ou outra pessoa mediante outorga de procuração atribuindo poderes para solicitação de certificado para equipamento e assinatura do respectivo termo de titularidade.<br />'.
'<br />'.
'A identificação das Pessoas Física e Jurídica são realizadas por um Agente de Registro mediante a presença física do interessado e através dos documentos de identificação ORIGINAIS.<br />'.
'<br />'.
'Identificação da(s) Pessoa(s) Física(s):<br />'.
'<br />'.
'Documento de Identificação (OBRIGATÓRIO)<br />'.
'<br />'.
'<ul>'.
	'<li>Cédula de Identidade;</li>'.
	'<ul>'.
		'<li>Registro Geral – RG;</li>'.
		'<li>Carteira Nacional de Habilitação – CNH;</li>'.
		'<li>Carteira de Trabalho – CTPS(modelo informatizado);</li>'.
		'<li>Identificação Profissional emitida por conselho de classe ou órgão competente (OAB, CRM, etc);</li>'.
		'<li>Passaporte, se brasileiro.</li>'.
	'</ul>'.
	'<li>Carteira Nacional de Estrangeiro – CNE, se estrangeiro domiciliado no Brasil;</li>'.
	'<li>Passaporte, se estrangeiro não domiciliado no Brasil.</li>'.
'</ul>'.
'<br />'.
'Outros Documentos (OBRIGATÓRIOS)<br />'.
'<br />'.
'<ul>'.
	'<li>Cadastro de Pessoa Física (CPF);</li>'.
	'<li>Duas vias do Termo de Titularidade. Este termo estará disponível ao final do processo de solicitação do certificado digital. A assinatura do termo deverá ser realizada somente na presença de um Agente de Registro.</li>'.
	'<li>Levar o comprovante de pagamento em mãos.</li>'.
'</ul>'.
'<br />'.
'Outros Documentos (OPCIONAIS)<br />'.
'<br />'.
'<ul>'.
'<li>Número de Identificação Social - NIS (NIT/PIS/PASEP);</li>'.
'<li>Cadastro Específico do INSS – CEI;</li>'.
'<li>Título de eleitor.</li>'.
'</ul>'.
'<br />'.
'Identificação da Pessoa Jurídica<br />'.
'<br />'.
'<ul>'.
'<li>Ato Constitutivo original devidamente registrado no órgão competente;</li>'.
'<li>Todas as alterações do ato constitutivo, quando aplicável;</li>'.
'<li>Documentos da eleição de seus administradores, quando aplicável;</li>'.
'<li>Prova de inscrição no Cadastro Nacional de Pessoas Jurídicas – CNPJ;</li>'.
'</ul>'.
'<br />'.
'Outros Documentos (OPCIONAIS)<br />'.
'<br />'.
'<ul>'.
'<li>Prova de inscrição no Cadastro Específico do INSS – CEI.</li>'.
'</ul>'.
'<br />'.
'ATENÇÃO: Os documentos OPCIONAIS, se preenchidos na solicitação do certificado, deverão ser apresentados na sua versão ORIGINAL.<br />'.
'<br />'.
'Identificação de Equipamento<br />'.
'<br />'.
'Para certificados de equipamento que utilizem URL no campo Common Name, é verificado se o solicitante do certificado detém o registro do nome de domínio junto ao órgão competente, ou se possui autorização do titular do domínio para usar aquele nome. Nesse caso deve ser apresentada documentação comprobatória (Termo de Autorização para Uso de Domínio ou similar) devidamente assinado pelo titular do domínio.<br />'.
'<br />'.
'NOTA 1: Para domínios que utilizam o sufixo ".br" a verificação de registro de domínio será realizada através do site <a href="https://registro.br/2/whois" target="_blank"><u>https://registro.br/2/whois.</u></a>';

// ============================================================================
// Carimbo de Tempo
// ============================================================================

$documentosNecessarios[4] =
'Sendo o titular Pessoa Jurídica, será designada Pessoa Física como responsável pelo certificado.<br />'.
'<br />'.
'Será designado como Responsável pelo Certificado o Representante Legal da Pessoa Jurídica cadastrado na Receita Federal do Brasil ou outra pessoa mediante outorga de procuração atribuindo poderes para solicitação de certificado para equipamento e assinatura do respectivo termo de titularidade.<br />'.
'<br />'.
'A identificação das Pessoas Física e Jurídica são realizadas por um Agente de Registro mediante a presença física do interessado e através dos documentos de identificação ORIGINAIS.<br />'.
'<br />'.
'Identificação da(s) Pessoa(s) Física(s):<br />'.
'<br />'.
'Documento de Identificação (OBRIGATÓRIO)<br />'.
'<br />'.
'<ul>'.
	'<li>Cédula de Identidade;</li>'.
	'<ul>'.
		'<li>Registro Geral – RG;</li>'.
		'<li>Carteira Nacional de Habilitação – CNH;</li>'.
		'<li>Carteira de Trabalho – CTPS(modelo informatizado);</li>'.
		'<li>Identificação Profissional emitida por conselho de classe ou órgão competente (OAB, CRM, etc);</li>'.
		'<li>Passaporte, se brasileiro.</li>'.
	'</ul>'.
	'<li>Carteira Nacional de Estrangeiro – CNE, se estrangeiro domiciliado no Brasil;</li>'.
	'<li>Passaporte, se estrangeiro não domiciliado no Brasil.</li>'.
'</ul>'.
'<br />'.
'Outros Documentos (OBRIGATÓRIOS)<br />'.
'<br />'.
'<ul>'.
	'<li>Cadastro de Pessoa Física (CPF);</li>'.
	'<li>Duas vias do Termo de Titularidade. Este termo estará disponível ao final do processo de solicitação do certificado digital. A assinatura do termo deverá ser realizada somente na presença de um Agente de Registro.</li>'.
	'<li>Levar o comprovante de pagamento em mãos.</li>'.
'</ul>'.
'<br />'.
'Outros Documentos (OPCIONAIS)<br />'.
'<br />'.
'<ul>'.
'<li>Número de Identificação Social - NIS (NIT/PIS/PASEP);</li>'.
'<li>Cadastro Específico do INSS – CEI;</li>'.
'<li>Título de eleitor.</li>'.
'</ul>'.
'<br />'.
'Identificação da Pessoa Jurídica<br />'.
'<br />'.
'<ul>'.
'<li>Ato Constitutivo original devidamente registrado no órgão competente;</li>'.
'<li>Todas as alterações do ato constitutivo, quando aplicável;</li>'.
'<li>Documentos da eleição de seus administradores, quando aplicável;</li>'.
'<li>Prova de inscrição no Cadastro Nacional de Pessoas Jurídicas – CNPJ;</li>'.
'</ul>'.
'<br />'.
'Outros Documentos (OPCIONAIS)<br />'.
'<br />'.
'<ul>'.
'<li>Prova de inscrição no Cadastro Específico do INSS – CEI.</li>'.
'</ul>'.
'<br />'.
'ATENÇÃO: Os documentos OPCIONAIS, se preenchidos na solicitação do certificado, deverão ser apresentados na sua versão ORIGINAL.<br />'.
'<br />'.
'Identificação de Equipamento<br />'.
'<br />'.
'Para certificados de equipamento que utilizem URL no campo Common Name, é verificado se o solicitante do certificado detém o registro do nome de domínio junto ao órgão competente, ou se possui autorização do titular do domínio para usar aquele nome. Nesse caso deve ser apresentada documentação comprobatória (Termo de Autorização para Uso de Domínio ou similar) devidamente assinado pelo titular do domínio.<br />'.
'<br />'.
'A solicitação deve conter o nome de servidor e o número de série do equipamento. Esses dados devem ser validados comparando-os com aqueles publicados pelo ITI no Diário Oficial da União, quando do deferimento do credenciamento da ACT.<br />'.
'<br />'.
'NOTA 1: Para domínios que utilizam o sufixo ".br" a verificação de registro de domínio será realizada através do site <a href="https://registro.br/2/whois" target="_blank"><u>https://registro.br/2/whois.</u></a>';
?>