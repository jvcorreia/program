<?php
$certificados = array(
  "ecpf"  => array(
        "A3" => array(
            "ctok1" => array(
					'e-CPF A3 com Token',//nome
					'Certificado digital com Token/SmartCard incluso, com 1 ano de validade para Pessoas Físicas que ainda não possuem um dispositivo criptográfico homologado pela ICP-Brasil.',//descrição
					'1 ano',//validade
					'<i class="fa fa-check" aria-hidden="true"></i> &nbsp;token incluso',//device
					'?',//preço
                    '?',//template 20274	191
                    'simbolo-certificado-a3.png',//image
					'true',//mais vendido
					'0', //ponteiro array documentos necessários
                    '?' // 20274 - Proc.Dados - Certificado Digital - Pessoa Física A3 de 1 ano com token [ 254.98 ]
                    ),
            "stok1" => array(
					'e-CPF A3',//nome
					'Certificado digital com 1 ano de validade para Pessoas Físicas que <strong> já possuem um dispositivo criptográfico</strong> homologado pela ICP-Brasil. Token/SmartCard não fornecido.',//descrição
					'1 ano',//validade
					'<i class="fa fa-times" aria-hidden="true"></i> token não incluso',//device
					'?',//preço
                    '?',//template 20118	193
                    'simbolo-certificado-a3.png',//image
					'false',//mais vendido
					'0', //ponteiro array documentos necessários
					'?' // 20118 - Proc.Dados - Certificado Digital - Pessoa Física A3 de 1 ano [ 164 ]
                    ),
			"ctok3" => array('e-CPF A3 com Token',//nome
					'Certificado digital com Token/SmartCard incluso, com 3 anos de validade para Pessoas Físicas que ainda não possuem um dispositivo criptográfico homologado pela ICP-Brasil.',//descrição
					'3 anos',//validade
					'<i class="fa fa-check" aria-hidden="true"></i> &nbsp;token incluso',//device
					'?',//preço
                    '?',//template 20323	199
                    'simbolo-certificado-a3.png',//image
					'true',//mais vendido
					'0', //ponteiro array documentos necessários
					'?' // 787 - Proc.Dados - Certificado Digital - Pessoa Física A3 de 3 anos com Token [ 383 ]
                    ),
            "ctok5" => array('e-CPF A3 com Token',//nome
					'Certificado digital com 5 anos de validade para Pessoas Físicas que ainda não possuem um dispositivo criptográfico homologado pela ICP-Brasil. Token/SmartCard incluso.',//descrição
					'5 anos',//validade
					'<i class="fa fa-check" aria-hidden="true"></i> &nbsp;token incluso',//device
					'?',//preço
                    '?',//template 20325	194
                    'simbolo-certificado-a3.png',//image
					'true',//mais vendido
					'0', //ponteiro array documentos necessários
					'?' // 4379 - Proc.Dados - Certificado Digital - Pessoa Física A3 de 5 anos com Token [ 453.53 ]
                    ),
			"stok3" => array('e-CPF A3',//nome
					'Certificado digital com 3 anos de validade para Pessoas Físicas que <strong> já possuem um dispositivo criptográfico</strong> homologado pela ICP-Brasil. Token/SmartCard não fornecido.',//descrição
					'3 anos',//validade
					'<i class="fa fa-times" aria-hidden="true"></i> token não incluso',//device
					'?',//preço
                    '59',//template 20322	204
                    'simbolo-certificado-a3.png',//image
					'false',//mais vendido
					'0', //ponteiro array documentos necessários
					'4376' // 4376 - Proc.Dados - Certificado Digital - Pessoa Física A3 de 3 anos [ 267 ]
                    ),
            "stok5" => array('e-CPF A3',//nome
					'Certificado digital com 5 anos de validade para Pessoas Físicas que <strong> já possuem um dispositivo criptográfico</strong> homologado pela ICP-Brasil. Token/SmartCard não fornecido.',//descrição
					'5 anos',//validade
					'<i class="fa fa-times" aria-hidden="true"></i> token não incluso',//device
					'?',//preço
                    '?',//template 20324	197
                    'simbolo-certificado-a3.png',//image
					'false',//mais vendido
					'0', //ponteiro array documentos necessários
					'?' // 779 - Proc.Dados - Certificado Digital - Pessoa Física A3 de 5 anos [ 335 ]
                    ),
            "mob" => array('NeoID e-CPF A3',//nome
					'Seu certificado Digital em Nuvem - para Pessoas Físicas, com 3 anos de validade, armazenado na infraestrutura de segurança do Serpro.',//descrição
					'3 anos',//validade
					'<i class="fa fa-mixcloud" aria-hidden="true"></i> &nbsp;em nuvem',//device
					'?',//preço
                    '78',//template
                    'simbolo-certificado-a3.png',//image
					'false',//mais vendido
					'0', //ponteiro array documentos necessários
					'24294' //Proc.Dados - Certificado Digital - Certificado Digital em Nuvem - NEOID - Pessoa Fisica - A3 de 3 anos
                    )
              ),
          "A1" => array(
            "dig" => array('e-CPF A1',//nome
					'Certificado digital com 1 ano de validade para Pessoas Físicas, gerado e armazenado no próprio computador.',//descrição
					'1 ano',//validade
					'<i class="fa fa-laptop" aria-hidden="true"></i> &nbsp;arquivo digital',//device
					'?',//preço
                    '56',//template 20321	188
                    'simbolo-certificado-a1.png',//image
					'true',//mais vendido
					'0', //ponteiro array documentos necessários
					'777v' // 777 - Proc.Dados - Certificado Digital - Pessoa Física A1 de 1 ano [ 164 ]
                    )
              )
          ),
  "ecnpj"  => array(
        "A3" => array(
            "ctok1" => array('e-CNPJ A3 com Token',//nome
					'Certificado digital com Token/SmartCard incluso, com 1 ano de validade para Pessoas <Físicas> Jurídicas que ainda não possuem um dispositivo criptográfico homologado pela ICP-Brasil.',//descrição
					'1 ano',//validade
					'<i class="fa fa-check" aria-hidden="true"></i> &nbsp;token incluso',//device
					'?',//preço
                    '40',//template 20267	208
                    'simbolo-certificado-a3.png',//image
					'true',//mais vendido
					'1', //ponteiro array documentos necessários
					'20267' // 20267 - Proc.Dados - Certificado Digital - Pessoa Jurídica A3 de 1 ano com token [ 328.13 ]
                    ),
            "stok1" => array('e-CNPJ A3',//nome
					'Certificado digital com 1 ano de validade para Pessoas Jurídicas que já possuem um dispositivo criptográfico homologado pela ICP-Brasil. Token/SmartCard não fornecido.',//descrição
					'1 ano',//validade
					'<i class="fa fa-times" aria-hidden="true"></i> &nbsp;token não incluso',//device
					'?',//preço
                    '48',//template 20112	207
                    'simbolo-certificado-a3.png',//image
					'false',//mais vendido
					'1', //ponteiro array documentos necessários
					'20112' // 20112 - Proc.Dados - Certificado Digital - Pessoa Jurídica A3 de 1 ano [ 225 ]
                    ),
			"stok5" => array('e-CNPJ A3 sem Token',//nome
					'Certificado digital com 5 anos de validade para Pessoas <Físicas> Jurídicas que ainda não possuem um dispositivo criptográfico homologado pela ICP-Brasil. Token/SmartCard não fornecido.',//descrição
					'5 anos',//validade
					'<i class="fa fa-times" aria-hidden="true"></i> &nbsp;token não incluso',//device
					'?',//preço
                    '39',//template 20270	211
                    'simbolo-certificado-a3.png',//image
					'false',//mais vendido
					'1', //ponteiro array documentos necessários
					'4383' // 4383 - Proc.Dados - Certificado Digital - Pessoa Jurídica A3 de 5 anos [ 475 ]
					),
            "ctok5" => array('e-CNPJ A3 com Token',//nome
					'Certificado digital com 5 anos de validade para Pessoas <Físicas> Jurídicas que ainda não possuem um dispositivo criptográfico homologado pela ICP-Brasil. Token/SmartCard incluso.',//descrição
					'5 anos',//validade
					'<i class="fa fa-check" aria-hidden="true"></i> &nbsp;token incluso',//device
					'?',//preço
                    '?',//template 20315	206
                    'simbolo-certificado-a3.png',//image
					'true',//mais vendido
					'1', //ponteiro array documentos necessários
					'?' // 4384 - Proc.Dados - Certificado Digital - Pessoa Jurídica A3 de 5 anos com Token [ 599.83 ]
					),
			"ctok3" => array('e-CNPJ A3 com Token',//nome
					'Certificado digital com 3 anos de validade para Pessoas Jurídicas que ainda não possuem um dispositivo criptográfico homologado pela ICP-Brasil.',//descrição
					'3 anos',//validade
					'<i class="fa fa-check" aria-hidden="true"></i> &nbsp;token incluso',//device
					'?',//preço
                    '?',//template 20269	212
                    'simbolo-certificado-a3.png',//image
					'true',//mais vendido
					'1', //ponteiro array documentos necessários
					'?' // 788 - Proc.Dados - Certificado Digital - Pessoa Jurídica A3 de 3 anos com Token [ 448 ]
                    ),
            "stok3" => array('e-CNPJ A3',//nome
					'Certificado digital com 3 anos de validade para Pessoas Jurídicas que já possuem um dispositivo criptográfico homologado pela ICP-Brasil. Token/SmartCard não fornecido.',//descrição
					'3 anos',//validade
					'<i class="fa fa-times" aria-hidden="true"></i> &nbsp;token não incluso',//device
					'?',//preço
                    '41',//template 20268	213
                    'simbolo-certificado-a3.png',//image
					'false',//mais vendido
					'1', //ponteiro array documentos necessários
					'797' // 797 - Proc.Dados - Certificado Digital - Pessoa Jurídica A3 de 3 anos [ 330 ]
                    ),
            "ctok18" => array('e-CNPJ A3 MEs e EPPs com Token',//nome
					'Certificado digital com 18 meses de validade para pequenas e médias empresas que ainda não possuem um dispositivo criptográfico homologado pela ICP-Brasil. Token/SmartCard incluso.',//descrição
					'18 meses',//validade
					'<i class="fa fa-check" aria-hidden="true"></i> &nbsp;token incluso',//device
					'?',//preço
                    '?',//template 20149	210
                    'simbolo-certificado-a3.png',//image
					'true',//mais vendido
					'1', //ponteiro array documentos necessários
					'?' // 20149 - Proc.Dados - Certificado Digital - Pessoa Jurídica A3 de 18 meses com Token (ME/EPP) [ 250.8 ]
                    ),
            "stok18" => array('e-CNPJ A3 MEs e EPPs',//nome
					'Certificado digital com 18 meses de validade para Pequenas e Médias Empresas  que já possuem um dispositivo criptográfico homologado pela ICP-Brasil. Token/SmartCard não fornecido.',//descrição
					'18 meses',//validade
					'<i class="fa fa-times" aria-hidden="true"></i> &nbsp;token não incluso',//device
					'?',//preço
                    '?',//template 20266	214
                    'simbolo-certificado-a3.png',//image
					'false',//mais vendido
					'1', //ponteiro array documentos necessários
					'?' // 20266 - Proc.Dados - Certificado Digital - Pessoa Jurídica A3 de 18 meses (ME/EPP) [ 225 ]
                    ),
            "mob" => array('NeoID e-CNPJ A3',//nome
					'Certificado Digital para Pessoas Jurídicas, com 3 anos de validade, armazenado na infraestrutura de segurança do Serpro.',//descrição
					'3 anos',//validade
					'<i class="fa fa-mixcloud" aria-hidden="true"></i> &nbsp;em nuvem',//device
					'?',//preço
                    '80',//template
                    'simbolo-certificado-a3.png',//image
					'false',//mais vendido
					'1', //ponteiro array documentos necessários
					'24295' //Proc.Dados - Certificado Digital - Certificado Digital em Nuvem - NEOID - Pessoa Juridica - A3 de 3 anos
                    )
              ),
          "A1" => array(
            "dig" => array('e-CNPJ A1',//nome
					'Certificado digital com 1 ano de validade para Pessoas Jurídicas, gerado e armazenado no próprio computador.',//descrição
					'1 ano',//validade
					'<i class="fa fa-laptop" aria-hidden="true"></i> &nbsp;arquivo digital',//device
					'?',//preço
                    '45',//template 20314	189
                    'simbolo-certificado-a1.png',//image
					'true',//mais vendido
					'1', //ponteiro array documentos necessários
					'780v' // 780 - Proc.Dados - Certificado Digital - Pessoa Jurídica A1 de 1 ano [ 225 ]
                    )
              )
          ),
  "equip-apl"  => array(
        "A1" => array(
            "apl" => array('Aplicação A1',//nome
					'Certificado digital com 1 ano de validade destinado à identificação de aplicações Web.',//descrição
					'1 ano',//validade
					'<i class="fa fa-laptop" aria-hidden="true"></i> &nbsp;arquivo digital',//device
					'?',//preço
                    '?',//template
                    'simbolo-certificado-a1.png',//image
					'false',//mais vendido
					'3', //ponteiro array documentos necessários
					'?' // <<<<<<<<< Não preenchido @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ Chamador de atenção @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                    ),
            "equip" => array('Equipamento A1',//nome
					'Certificado digital com 1 ano de validade destinado à identificação de servidores WEB.',//descrição
					'1 ano',//validade
					'<i class="fa fa-laptop" aria-hidden="true"></i> &nbsp;arquivo digital',//device
					'?',//preço
                    '3',//template 20317	229
                    'simbolo-certificado-a1.png',//image
					'false',//mais vendido
					'3', //ponteiro array documentos necessários
					'776' // 776 - Proc.Dados - Certificado Digital - Equipamento A1 de 1 ano [ 1254 ]
                    ),
			"equip-mult" => array('Equipamento Multidomínio A1',//nome
					'Certificado digital com 1 ano de validade destinado à identificação de servidores WEB Multidomínio.',//descrição
					'1 ano',//validade
					'<i class="fa fa-laptop" aria-hidden="true"></i> &nbsp;arquivo digital',//device
					'?',//preço
                    '8',//template 20432	246
                    'simbolo-certificado-a1.png',//image
					'false',//mais vendido
					'3', //ponteiro array documentos necessários
					'20432' // 20432 - Proc.Dados - Certificado Digital - Equipamento Multi-Domínio A1 de 1 Ano [ 3150.68 ]
                    )
              )
          ),
	"carimbo"  => array(
        "tempo" => array(
            "tmp" => array('Certificado de Carimbo do Tempo',//nome
					'Certificado digital que atesta data e hora que um documento foi assinado digitalmente.',//descrição
					'5 anos',//validade
					'<i class="fa fa-laptop" aria-hidden="true"></i> &nbsp;arquivo digital',//device
					'?',//preço
                    '74',//template 20273	244
                    'simbolo-certificado-a1.png',//image
					'false',//mais vendido
					'4', //ponteiro array documentos necessários
					'20273' // 20273 - Proc.Dados - Certificado Digital - Equipamento T3 de 5 anos [ 2821.5 ]
                    )
              )
          ),
  "banco"  => array(
        "A1" => array(
            "eco" => array('ECO',//nome
					'Certificado digital com 1 ano de validade que permite às Instituições Financeiras acessarem o Sistema de Empréstimo Consignado Online.',//descrição
					'1 ano',//validade
					'<i class="fa fa-laptop" aria-hidden="true"></i> &nbsp;arquivo digital',//device
					'?',//preço
                    '38',//template 20310	111
                    'simbolo-certificado-outros.png',//image
					'false',//mais vendido
					'2', //ponteiro array documentos necessários
					'781' // 781 - Proc.Dados - Certificado Digital - Bancário - SPB, COMPE, Cadastro Positivo, ECO e C3 de 1 ano [ 1881 ]
                    ),
          "c3" => array('C3',//nome
					'Certificado digital com 1 ano de validade para uso na Câmara das Cessões de Crédito - C3.',//descrição
					'1 ano',//validade
					'<i class="fa fa-laptop" aria-hidden="true"></i> &nbsp;arquivo digital',//device
					'?',//preço
                    '30',//template 20310	113
                    'simbolo-certificado-outros.png',//image
					'false',//mais vendido
					'2', //ponteiro array documentos necessários
					'781' // 781 - Proc.Dados - Certificado Digital - Bancário - SPB, COMPE, Cadastro Positivo, ECO e C3 de 1 ano [ 1881 ]
                    ),
          "cad" => array('Cadastro Positivo',//nome
					'Certificado digital com validade de 1 ano que permite a troca de arquivos entre as instituições financeiras e o ambiente do Serviço Eletrônico Compartilhado - SEC, para acesso às informações de cadastro positivo.',//descrição
					'1 ano',//validade
					'<i class="fa fa-laptop" aria-hidden="true"></i> &nbsp;arquivo digital',//device
					'?',//preço
                    '34',//template 20310	117
                    'simbolo-certificado-outros.png',//image
					'false',//mais vendido
					'2', //ponteiro array documentos necessários
					'781' // 781 - Proc.Dados - Certificado Digital - Bancário - SPB, COMPE, Cadastro Positivo, ECO e C3 de 1 ano [ 1881 ]
                    ),
          "cip" => array('CIP – SCG',//nome
					'Certificado digital com 1 ano de validade que permite às Instituições Financeiras acessarem o Sistema de Controle de Garantias.',//descrição
					'1 ano',//validade
					'<i class="fa fa-laptop" aria-hidden="true"></i> &nbsp;arquivo digital',//device
					'?',//preço
                    '32',//template 20310	115
                    'simbolo-certificado-outros.png',//image
					'false',//mais vendido
					'2', //ponteiro array documentos necessários
					'781' // 781 - Proc.Dados - Certificado Digital - Bancário - SPB, COMPE, Cadastro Positivo, ECO e C3 de 1 ano [ 1881 ]
                    ),
          "compe" => array('COMPE',//nome
					'Certificado digital com 1 ano de validade que permite às Instituições Financeiras acessarem a Centralizadora da Compensação de Cheques – COMPE.',//descrição
					'1 ano',//validade
					'<i class="fa fa-laptop" aria-hidden="true"></i> &nbsp;arquivo digital',//device
					'?',//preço
                    '33',//template 20310	116
                    'simbolo-certificado-outros.png',//image
					'false',//mais vendido
					'2', //ponteiro array documentos necessários
					'781' // 781 - Proc.Dados - Certificado Digital - Bancário - SPB, COMPE, Cadastro Positivo, ECO e C3 de 1 ano [ 1881 ]
                    ),
            "spb" => array(
					'SPB',//nome
					'Certificado digital com 1 ano de validade exclusivo para o Sistema de Pagamento Brasileiro – SPB.',//descrição
					'1 ano',//validade
					'<i class="fa fa-laptop" aria-hidden="true"></i> &nbsp;arquivo digital',//device
					'?',//preço
                    '35',//template 20310	118
                    'simbolo-certificado-outros.png',//image
					'false',//mais vendido
					'2', //ponteiro array documentos necessários
					'781' // 781 - Proc.Dados - Certificado Digital - Bancário - SPB, COMPE, Cadastro Positivo, ECO e C3 de 1 ano [ 1881 ]
                    ),
            "bac" => array(
					'BACEN SELIC',//nome
					'Certificado digital exclusivo para BACEN-SELIC.',//descrição
					'1 ano',//validade
					'<i class="fa fa-laptop" aria-hidden="true"></i> &nbsp;arquivo digital',//device
					'?',//preço
                    '37',//template 20310	184
                    'simbolo-certificado-outros.png',//image
					'false',//mais vendido
					'2', //ponteiro array documentos necessários
					'781' // 781 - Proc.Dados - Certificado Digital - Bancário - SPB, COMPE, Cadastro Positivo, ECO e C3 de 1 ano [ 1881 ]
                    )					
		)
	)
); //END CERT ARRAY

// Start: sincronização de preços com o sigecom

// Se der rolo de ssl, alterar o php.ini, habilitar extensão: extension=php_openssl.dll

$opts = array
		(
		'http' => array
				(
					'method' => 'POST',
					'header' => 'Authorization:Basic aWVtZ21yczI3ZDdrdmQ3ZzdtNWM3a2k0N2o6bXBuOTVzYm1uYWppOTlhZmhyZjIxMWI3OG8=', // validacao
					//'header' => 'Authorization:Basic ZDlpZGx0NHY0dWFwZHQyM2JuYjkwaXByZmM6NG81c244czVtb2x1cGVsYWkyNGI5OW9uYmg=', // produção
					'content' => 'grant_type=client_credentials&scope=escopo_api_sigecom'
				),
		'ssl' => array
				(
					'verify_peer' => false,
					'verify_peer_name' => false
				)
		);

$context = stream_context_create($opts);

// Validação
$result = @file_get_contents('https://valautentikus.estaleiro.serpro.gov.br/autentikus-authn/api/v1/token', false, $context);

// Produção
//$result = @file_get_contents('https://autentikus.estaleiro.serpro.gov.br/autentikus-authn/api/v1/token', false, $context);
 
$auten = @json_decode($result);

$opts = array
		(

		'http' => array
				(
					'method' => 'GET',
					'header' => 'Authorization:Bearer '.$auten->access_token,
					'content' => ''
				),

		'ssl' => array
				(
					'verify_peer' => false,
					'verify_peer_name' => false
				)
		);
$context = stream_context_create($opts);

// Validação
$response = @file_get_contents('https://valsigecomservicos.estaleiro.serpro.gov.br/sigecom-servicos/servicos/ifas?codservico=10433', false, $context);

// Produção
//$response = @file_get_contents("https://gfcms.corporativo.serpro/sigecom-servicos/servicos/ifas?codservico=10433", false, $context);
     
$sigecom = @json_decode($response);

foreach($certificados as $k1 => $v1)
	foreach($v1 as $k2 => $v2)
		foreach($v2 as $k3 => $v3)
			foreach($sigecom as $sgc)
				if($v3[9] == $sgc->id)
				{
					$certificados[$k1][$k2][$k3][4] = number_format($sgc->ifaValValorUnitario, 2, ',', '.');
					break; // Mais performance
				}

// End: sincronização de preços com o sigecom

function  cert($tipo, $subtipo, $subsubtipo) {
global $certificados;
echo <<<EOD

<div class="col-lg-2">
	<img src="img/{$certificados[$tipo][$subtipo][$subsubtipo][6]}" alt="" />
</div>
<div class="col-lg-5 no-pad">
	<h4>{$certificados[$tipo][$subtipo][$subsubtipo][0]}</h4>
	<p>{$certificados[$tipo][$subtipo][$subsubtipo][1]}</p>
</div>
<div class="col-lg-5 no-pad text-center">
	<ul>
		<li><i class="fa fa-calendar" aria-hidden="true"></i>&nbsp; <strong>{$certificados[$tipo][$subtipo][$subsubtipo][2]}</strong> de validade</li>
		<li>{$certificados[$tipo][$subtipo][$subsubtipo][3]}</li>
		<li class="price">R$ {$certificados[$tipo][$subtipo][$subsubtipo][4]}</li>
	</ul>
	<a class="btn btn-xs btn-cta" data-toggle="modal" data-target="#localizacao" name="{$certificados[$tipo][$subtipo][$subsubtipo][5]}">Comprar Agora</a><br /><br />
	<a class="btn btn-xs btn-ghost" data-toggle="modal" data-target="#documentos" name="{$certificados[$tipo][$subtipo][$subsubtipo][8]}">Documentos necessários</a>
</div>

EOD;
 
}
?>