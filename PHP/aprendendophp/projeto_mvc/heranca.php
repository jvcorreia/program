<?php 
//Herança é um recurso que permite o compartilhamento de atributos e métodos entre classes

class Veiculo{
     private $modelo;
     private $cor;
     private $ano;


     public function andar(){
          echo "VRUM......";
     }

     public function parar(){
          echo "PARANDO.....";
     }

}


class Carro extends Veiculo{
     
}

class Moto extends Veiculo{
    
}


$palio = new Carro();

$palio->andar();
?>