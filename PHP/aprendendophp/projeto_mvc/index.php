<?php 
class Pessoa{
     public $nome;//Atributo
     public $idade;//Atributo

     public function falar(){ //Métodos
          echo $this->nome." de ".$this->idade." anos falou kkk."; //Utilizando o this
     }

}

$joao = new Pessoa(); //Instanciando a classe == objeto


$joao->nome = 'João'; //Dando valor ao atributo
$joao->idade = 15;

$joao->falar();//Chamando método

var_dump($joao);




?>