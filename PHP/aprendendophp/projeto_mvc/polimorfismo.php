<?php

class Animal{
     public function andar(){
          echo "o animal está andando..";
     }
}

$animal = new Cavalo();

$animal->andar();

class Cavalo extends Animal{
     public function andar(){
          echo "pocoto pocoto pocoto";
     }
}
?>