<?php
//Agregação é quando uma classe precisa de outra para executar sua ação.
class Produto{
     public $nome;
     public $valor;

     public function __construct($nome, $valor)
     {
          $this->nome = $nome;
          $this->valor = $valor;
     }
}

class Carrinho{
     public $produtos;

     public function adiciona (Produto $produto){
          $this->produtos [] = $produto;
     }

     public function exibe (){
          foreach($this->produtos as $produto){
               echo $produto->nome."<br>";
               echo $produto->valor;
          } 
     }
}


$produto1 = new Produto("pepsi", 15.88);

$carrinho = new Carrinho();
$carrinho->adiciona($produto1);

$carrinho->exibe();
?>