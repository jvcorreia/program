<?php 

class Login{
     private $email;
     private $senha;
     private $nome;

     public function __construct($email, $senha, $nome)//Construct é um método que é inicializado quando o obj é instaciado
     {
          $this->setEmail($email);
          $this->setSenha($senha);
          $this->setNome($nome);
     }

     public function getNome(){
          return $this->nome;
     }

     public function setNome($e){
          $this->nome = $e;
     }

     public function getEmail(){
          return $this->email;
     }

     public function setEmail($e){
          $email = filter_var($e);
          $this->email = $email;
     }

     public function getSenha(){
          return $this->senha;
     }

     public function setSenha($e){
          $senha = filter_var($e);
          $this->senha = $senha;      
     }

     public function logar(){
          if($this->email == "teste@teste.com" and $this->senha == "123"){
               echo "logado com sucesso!";
          }
          else{
               echo "dados invalidos";
          }
     }

}

$logar = new Login("teste@teste.com", "123", "João Victor");


echo $logar->getEmail()."<br>";

echo $logar->getSenha()."<br>";

echo $logar->getNome()."<br>";

$logar->logar();

?>