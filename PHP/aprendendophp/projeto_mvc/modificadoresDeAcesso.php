<?php 
//public -> o mais permissivo
//private -> acessado somente pela classe que o declarou
//protected -> os herdeiros veem propriedades como se fossem publicas

class Veiculo{
     protected $modelo;
     private $cor;
     private $ano;

     public function __construct($modelo)
     {
          $this->modelo = $modelo;
     }

     protected function getModelo(){
          return $this->modelo;
     }

     public function andar(){
          echo "VRUM......";
     }

     public function parar(){
          echo "PARANDO.....";
     }


}


class Carro extends Veiculo{
     public function acessaGetModelo(){ //Exemplificando como acessar metodo protected em uma classe
          echo $this->getModelo();
     }
}

class Moto extends Veiculo{
    
}


$palio = new Carro("quadrado");

$palio->andar();

echo $palio->acessaGetModelo();
?>