<?php 
abstract class Banco{ //Após declarar como abstract, não é mais possível instanciar uma classe
     protected $saldo = 0;
     protected $limite_saldo;
     protected $juros;

     abstract protected function sacar($valor);

     abstract protected function depositar($valor);

     abstract protected function getSaldo();
}

class Itau extends Banco{
     public function sacar($valor){
          $this->saldo = $this->saldo - $valor;
     }

     public function depositar($valor){
          $this->saldo = $this->saldo + $valor;
     }

     public function getSaldo(){
          return $this->saldo;
     }
}



$itau = new Itau();

echo $itau->getSaldo();

$itau->depositar(15);

echo $itau->getSaldo();

?>