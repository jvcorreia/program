<?php

class Pessoa{
     public $idade;

     public function __clone()
     {
          //Método inicializado quando chamamos o clone
     }
}

$pessoa = new Pessoa();

$pessoa->idade = 25;

$pessoa2 = $pessoa;

$pessoa2->idade = 35;

echo $pessoa->idade; //Pessoa assumiu o valor de pessoa 2, pq para pessoa 2 não foi instaciado uma nova Pessoa

//Para mudar isso, usar:

$pessoa3 = clone $pessoa;
$pessoa3->idade = 25;
echo $pessoa3->idade;


?>