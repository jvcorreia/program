<?php
//Escopo global
$nome = 'joao correia';

function nome(){
    //print $nome;    ---> esse comando geraria erro, pois a variável está no escopo global e a função cria um escopo local
    //Precisamos primeiro declarar a variável aqui
    global $nome;
    print $nome;
}

nome();
echo '<hr>';

function cidade(){
    global $cidade;
    $cidade = 'bsb';
    echo $cidade;
}

cidade();
echo '<hr>';//Vamos conseguir imprimir o valor de cidade, mas somente pq declaramos a variável como global

$a = 2;
$b = 3;
$c = 4;

function soma(){
   echo $GLOBALS['a'] + $GLOBALS['b'] + $GLOBALS['c'];
}

soma();
?>