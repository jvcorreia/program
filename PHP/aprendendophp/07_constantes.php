<?php
//Constantes

define("NOME", "joao"); //Por convenção, os identificadores de constantes são sempre com letra maiuscula
echo NOME;

echo '<br>';

define('DIASDASEMANA',['segunda', 'terça', 'quarta', 'quinta', 'sexta', 'sábado', 'domingo']); 
//Constantes são automaticamente globais
print DIASDASEMANA[2];
?>