<!DOCTYPE html>
<html lang="pt-br">
    <?php  //Para trocar o indice, é só mudar o nome das páginas

        if(isset($_POST['enviaform'])){
            /*echo "enviado <br>";
            echo var_dump($_POST);*/
            $erros = [];

            if(filter_var($_POST['idade'],FILTER_VALIDATE_INT && FILTER_SANITIZE_NUMBER_INT)){
                echo 'certo';
            }
            else{
                $erros [] = 'idade precisa ser um inteiro';
            }

            if (!empty($erros)){
                foreach($erros as $erro){
                    echo "<li> $erro ";
                }
            }
            else{
                echo "parabens, dados corretos";
            }
        }
    
    ?>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <title>Formulário</title>
</head>
<body>

    <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post" >
        <div class="form-group">
            <label for="exampleInputEmail1">E-mail</label>
            <input type="email" class="form-control"  name="email" aria-describedby="emailHelp" placeholder="E-mail" >
        </div>
        <div class="form-group">
            <label for="exampleInputPassword1">Idade</label>
            <input type="text" class="form-control"  name="idade" placeholder="Idade" >
        </div>
        <div class="form-group">
            <label for="exampleInputPassword1">Peso</label>
            <input type="text" class="form-control"  name="peso" placeholder="Peso" >
        </div>
        <div class="form-group">
            <label for="exampleInputPassword1">IP</label>
            <input type="text" class="form-control"  name="ip" placeholder="Ip" >
        </div>
        <div class="form-group">
            <label for="exampleInputPassword1">URL</label>
            <input type="text" class="form-control"  name="url" placeholder="URL" >
        </div>
        <button type="submit" class="btn btn-primary" name="enviaform">Enviar com o método post</button>
    </form>


        
    <footer>
        <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    </footer>
</body>
</html>