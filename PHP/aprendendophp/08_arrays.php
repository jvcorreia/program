<?php
//Exemplo básico
$carros = ['bmw', 'ferrari', 'hilux'];

$times = array();

print_r($carros);
print '<br>';

echo is_array($carros);

print '<br>';

//Podemos especificar o índice
$homens = [1=> 'xapiuski', 'luciano', 'jose'];
print $homens[1];
print '<br>';
print $homens[2];
print '<br>';
print $homens[3];
print '<br>';

unset($homens[2]); //Remover elemento de uma lista
print_r($homens);
print '<br>';

//Adicionar elemento em uma lista
array_push($homens,'zuck'); // ou
$homens [] = 'homi';
print_r($homens);
print '<br>';

echo count($homens);//Número de elementos
print '<br>';

//Foreach
foreach ($homens as $key ) {
    echo $key.'<br>';
}

$listadelista = [[1, 2, 3, 4], [true, false, false, true]];

//Array associativo

$dados = ['nome' => 'joao', 'idade' => 17, 'homem' => true];

print $dados['nome'];






?>