<?php
/*
As superglobais são variáveis que estarão disponíveis em todos os escopos, dessa forma
 não há a necessidade de fazer o global $variavel antes de chamar ela.

As variáveis superglobais são:

$GLOBALS
$_SERVER
$_GET
$_POST
$_FILES
$_COOKIE
$_SESSION
$_REQUEST
$_ENV

*/


//$GLOBALS

function test() {
    $x = "variavel local";

    echo '$x no escopo global: ' . $GLOBALS["x"] . "<br>";
    echo '$x dentro desse escopo: ' . $x . "<br>";
}

$x = "variavel sem escopo local";
test();

//$_SERVER
/*
$_SERVER é um array contendo informação como cabeçalhos, paths, e localizações do script.
As entradas neste array são criadas pelo servidor web. Não há garantia que cada servidor
 web proverá algum destes; servidores podem omitir alguns, ou fornecer outros não listados aqui. 
*/

echo $_SERVER['PHP_SELF'].'<br>';//-> Retorna endereço do arquivo

echo $_SERVER['SERVER_NAME'].'<br>';//-> Retorna nome do host(servidor)

?>