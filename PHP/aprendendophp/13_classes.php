<?php
/**
 * A pseudo-variável $this está
 *  disponível quando um método é chamado a partir de um contexto de objeto.
 *  $this é uma referência ao objeto chamado
 * 
 * 
 */
class SimpleClass
{
    // declaração de propriedade
    public $var = 'um valor padrão';

    // declaração de método
    public function displayVar() {
        echo $this->var;
    }
}

$teste = new SimpleClass();

echo $teste->displayVar();


class A
{
    function foo()
    {
        if (isset($this)) {
            echo '$this está definida (';
            echo get_class($this);
            echo ")\n";
        } else {
            echo "\$this não está definida.\n";
        }
    }
}

$a = new A();
$a->foo();


class OBJ //Chamando propriedade x Chamando OBJ.
{
    public $bar = 'propriedade';

    public function bar() {
        return 'métod';
    }
}

$obj = new OBJ();
echo $obj->bar, PHP_EOL, $obj->bar(), PHP_EOL;
?>