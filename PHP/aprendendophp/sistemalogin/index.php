
<?php 
require_once 'conexaodb.php'; // Chmando a página do banco

//Sessão
session_start();



//Botão form
if(isset($_POST['logar'])){
    $erros = [];
    $login = mysqli_escape_string($connect, $_POST["login"]);//Função do mysql para filtragem dos dados digitados pelo user
    $senha = mysqli_escape_string($connect, $_POST['senha']);//Função do mysql para filtragem dos dados digitados pelo user

    if(empty($login) or empty($senha)){ //Checando se os campos estão vazios
        $erros [] = 'Existem campos em branco';
    }
    else{
        $sql = "select login from usuario where login = '$login' "; //Consulta SQL
        $resultado = mysqli_query($connect, $sql); //Capturando o resultado
        if(mysqli_num_rows($resultado) > 0){//Vendo se existe um resultado
            $senha = md5($senha);
            $sql = "select * from usuario where login = '$login' and senha = '$senha' ";
            $resultado = mysqli_query($connect, $sql);

            if(mysqli_num_rows($resultado) > 0){//Vendo se existe um resultado
                $dados = mysqli_fetch_array($resultado);
                $_SESSION['logado'] = true;
                $_SESSION['id_usuario'] = $dados['nome'];
                header('Location: home.php');
            }
            else{
                $erros [] = 'Usuário ou senha errados';
            }
        }
        else{
          $erros [] = 'Usuário ou senha errados';
        }
        {

        }
    }
}

if (isset($_POST['cadastrar'])){
  header('Location: cadastra.php');
}
?>



<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>Página de login do João</title>


    <!-- CSS do Bootstrap -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

    <!-- CSS customizado -->
    <link href="signin.css" rel="stylesheet">
  </head>

  <body class="text-center">
    <form class="form-signin" method="post">
      <img class="mb-4" src="https://media-exp1.licdn.com/dms/image/C510BAQFJ-hfjUY-a1A/company-logo_200_200/0?e=2159024400&v=beta&t=VbHvff0e19sP_xYbpvjYbPeVXb1ovQ6A3fv02Hs8YQU" alt="" width="72" height="72">
      <h1 class="h3 mb-3 font-weight-normal">Página de login do João</h1>
      <p>Faça seu login agora mesmo</p>
      <label for="inputEmail" class="sr-only">Usuário</label>
      <input type="text" id="inputEmail" class="form-control" placeholder="user123" autofocus name="login">
      <label for="inputPassword" class="sr-only" >Senha</label>
      <input type="text" id="inputPassword" class="form-control" placeholder="senha123" name="senha">
      <p style="color:darkgoldenrod;"> <?php 
        if(!empty($erros)){
            foreach ($erros as $erro) {
                echo $erro;
            }
        }
        ?></p>
      <div class="checkbox mb-3">
        <label>
          <input type="checkbox" value="remember-me"> Me lembre
        </label>
      </div>
      <button class="btn btn-lg btn-primary btn-block" type="submit" name="logar">Logar</button>

      <button class="btn btn-lg btn-secondary btn-block" type="submit" name="cadastrar">Cadastrar</button>
    </form>

    <footer>
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    </footer>
  </body>
</html>
