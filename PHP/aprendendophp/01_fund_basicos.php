<html>
    <head>
        <title>
            página 
        </title>

        <body>
            <?php  //abertura de tag php
                echo 'meu nome é joão <br>';  //Comandos para mostrar algo na tela
                print 'tenho 20 anos <br>';
                //Se não tiver mais nada após o código php, não é necessário fechar a tag
            ?>

            <?= 'o comando echo também pode ser colocado dessa forma' ?>

            <?php echo 'testando'?> <!--Caso coloque dessa forma, não é necessaário colocar ; pois o fechamento da
                                    tag serve como um ; --> 
        </body>
    </head>
</html>