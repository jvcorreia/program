<?php

$nome = 'joao victor';

if (is_numeric($nome)){ //Verificando se a variável é uma string
    echo 'sim!';
}
else{
    echo 'não';
}

//Função
$carros = array('uno', 'ferrari', 'cruze', 12.23, true, 15);

print '<br><hr>';
print is_array($carros);
print '<br><hr>';
var_dump($carros);

//Objeto
class Cliente {
    public $nome;
    public function atribuirNome($nome){
        $this->$nome = $nome;
    }
}

$cli1 = new Cliente();
$cli1->atribuirNome('joao');

print $cli1->nome;

gettype($nome); // Retorna o tipo da variável.
var_dump($nome); // Retorna o tipo e o valor.
is_int($nome); // Verifica se a variável em questão é do tipo integer.
is_bool($nome); // Verifica se a variável em questão é do tipo boolean.
is_numeric($nome); // Verifica se a variável em questão é uma string numérica, ex "100".
is_string($nome); // Verifica se a variável em questão é do tipo string.
is_float($nome); // Verifica se a variável em questão é do tipo flutuante.
is_array($nome); // Verifica se a variável em questão é do tipo Array.
is_object($nome); // Verifica se a variável em questão é do tipo objeto.
?>