<?php
$nome = "joão";
$concatena = '$nome';//Ele vai considerar tudo como texto, não passando as tags corretamente
$correto = "$nome";

print $concatena; //Não vai funcionar
print '<br>'; 
print $correto;
print '<br>';
echo 'meu nome é '.$nome. ' e tenho 20 anos'; //Desse modo também funciona

?>